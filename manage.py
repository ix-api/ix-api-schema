"""
This file provides various entry points to the
IX-API-Schema:

 - `generate` `json` or `yaml`
 - `serve` the schema using redoc
 - `export` `json` or `yaml` to a file
"""

import sys
import os
import traceback
from os import path

from prompt_toolkit.formatted_text import HTML
from prompt_toolkit import prompt, print_formatted_text

import ixapi_schema
from ixapi_schema import v1, v2
from ixapi_schema.v1.entities import problems as problems_v1
from ixapi_schema.v2.entities import problems as problems_v2
from ixapi_schema.openapi import schema
from ixapi_schema.server import docs

API_VERSIONS = {
    "v1": v1,
    "v2": v2,
}

PROBLEMS = {
    "v1": problems_v1,
    "v2": problems_v2,
}


def usage():
    """Show some help"""
    print_formatted_text(
        HTML("<b>------------------------------------------------</b>"))
    print_formatted_text(HTML(
        "<b color='blue'>IX-API: Schema</b>{}v.{}".format(
                ' ' * (32 - len(ixapi_schema.__version__)),
                ixapi_schema.__version__)))
    print_formatted_text(
        HTML("<b>------------------------------------------------</b>"))
    print("Usage: {} command [params, ...]".format(sys.argv[0]))
    print_formatted_text(HTML("\n<b>Commands:</b>"))
    print("    generate_schema [version] [yaml/json] [pretty]")
    print("    export_problem_details [version] <output path>")
    print("    serve [{}]".format(docs.LISTEN_DEFAULT))
    print("")


def handle_generate_schema(version="v1", fmt="yaml", pretty=False):
    """
    Generate API spec.
    Render either yaml or json. Default: json.
    """
    if fmt not in ["yaml", "yml", "json"]:
        print_formatted_text(HTML("<red>Unsupported format.</red>"))
        return

    api_base = API_VERSIONS.get(version)
    if not api_base:
        print_formatted_text(HTML("<red>Invalid version.</red>"))
        return

    spec = schema.generate(api_base)
    if fmt in ["yaml", "yml"]:
        import yaml
        print(yaml.dump(spec))
        return
    if fmt == "json":
        import json
        if pretty:
            print(json.dumps(spec, indent=2))
        else:
            print(json.dumps(spec))


def handle_generate_go_client_files(output_path="/tmp/ixapi-client-go"):
    """Export schema entities and endpoints"""
    os.makedirs(output_path, exist_ok=True)

    api_base = API_VERSIONS["v2"]
    spec = schema.generate(api_base)

    components = spec["components"]["schemas"]
    paths = spec["paths"]

    entities_filename = path.join(output_path, "entities.go")
    resources_filename = path.join(output_path, "resources.go")

    from ixapi_schema.codegen.go.client import (
        struct_renderer,
        resource_renderer,
    )
    struct_renderer.write_structs_to_file(components, entities_filename)

    try:
        resource_renderer.write_resources_to_file(components, paths, resources_filename)
    except Exception as e:
        print(e)
        print(traceback.format_exc())


def handle_generate_go_terraform_files(pkg_path="/tmp/ixapi-tfp"):
    """Export schema entities and endpoints"""
    os.makedirs(pkg_path, exist_ok=True)

    api_base = API_VERSIONS["v2"]
    spec = schema.generate(api_base)

    from ixapi_schema.codegen.go.terraform_sdk import (
        models,
    )

    try:
        models.write_files(spec, pkg_path)
        # datasources.write_files(spec, pkg_path)
        # resources.write_files(spec, pkg_path)
    except Exception as e:
        print(e)
        print(traceback.format_exc())


def handle_export_problem_details(version=None, output_path=None):
    """Export problem detail HTML pages."""
    if not output_path:
        output_path = version
        version = "v1" # Default
    if not output_path:
        print_formatted_text(HTML(
            "<red>Error: An output path is required.</red>"))
        return

    from ixapi_schema.problem_details import generator
    print("Exporting version {} to: {}".format(version, output_path))

    try:
        generator.export(PROBLEMS[version], output_path)
    except Exception as e:
        print_formatted_text(HTML(
            "<red>Error: {}</red>".format(e)))
        return


def handle_serve(listen=docs.LISTEN_DEFAULT):
    """Run a server on the listen port."""
    docs.serve(listen)


def dispatch_command(command, params):
    """Invoke specific handlers each commands"""
    handler_name = "handle_{}".format(command)
    handler = globals().get(handler_name)
    if not handler:
        usage()
        print_formatted_text(HTML("\n<red>Error: Invalid command.</red>\n"))
        return
    try:
        handler(*params)
    except TypeError:
        usage()
        print_formatted_text(HTML(
            "\n<red>Error: Insufficient or invalid parameters.</red>\n"))


def main(args):
    """IX-API schema entry point"""
    if len(args) < 2:
        usage()
        return

    command, params = args[1], args[2:]
    dispatch_command(command, params)


if __name__ == "__main__":
    main(sys.argv)

