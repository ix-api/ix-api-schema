
# Changelog

## 2.7.0 (2024-11-18)

 * Added flag `consumer_side_ready` to `exchange_lan` network service configs. (Issue #222)
 * Added macs property to `p2p_vc`, `p2mp_vc` and `mp2mp_vc`
   network service configs. (Issue #220)
 * Added RTT statistics. (Issue #190)
 * Fixed documentation for `connection.capacity_allocated`. (Issue #193)


## 2.6.0 (2024-03-04)

 * Added routing functions and layer 3 peer configuration.
 * Added optional pagination for `_list` operations.
 * Added optional `product_offering` to `cloud_vc` network service config. (Issue #210)
 * Added optional `metro_area` and `metro_area_network` to connection. (Issue #204)
 * Added `fields` property to timeseries. (Issue #205)
 * Added peer to peer (p2p) statistics. (Issue #214)
 * Added opt-in sharing of sensitive statistics. (Issue #217)
 * Added structured contract renewal- and notice-periods
   to product offerings. (Issue #203)
 * Using ISO8601 period notation is now recommended. (Issue #216)
 * Fixed missing `ValidationError` responses. (Issue #209)
 * Fixed statistics aggregate example. (Issue #215)
 * Fixed inconsistent filter name. (Issue #219)
 * Fixed missing filters for `managing_account` and `consuming_account` on
   connections, member joining rules and ports list endpoints. (Issue #213)

## 2.5.0 (2023-06-13)

 * `PUT` operations will be deprecated in future releases. (Issue #198)
 * `account_read` was added and will return the authenticated account. (Issue #201)
 * `AvailabilityZones` were added (Issue #156)
 * `mac_acl_protected` property was added to `mp2mp_vc` network service. (Issue #169)
   This also applies to schema versions `2.1.2`, `2.2.2`, `2.3.1` and `2.4.3`.
 * Added IP fields on virtual circuit network-services-configs. (Issue #196)
   This also applies to schema versions `2.1.2`, `2.2.2`, `2.3.1` and `2.4.3`.
 * Polymorphism for codegeneration was improved. (Issue #160)

## 2.4.2 (2022-12-02)
 
 * Improved documentation for `cloud_key` in network services. (Issue #181)
 * Fixed return type of operation: `facilities_read` (Issue #191)
 * Removed unused request parameter `assigned_at` from `macs_list` (Issue #188)
 * Document macs not_valid_before and not_valid_after properties (Issue #189)
 * Operation `roles_read` does no longer accept query parameters (Issue #186)
 * Added service provider pop name to CloudVC product-offering (Issue #187)

## 2.4.1 (2022-10-18)
 
 * Fixes ProductOffering required fields in
   `product_offerings_list` operation.

## 2.4.0 (2022-08-31)
 
 * Added statistics endpoints for ports, connections and
   network services.
 * Introduction of a changelog, to help tracking changes
   between versions.

## 2.3.1 (2023-06-13)

 * Updated network services of type `mp2mp_vc` to add 
   properties `mac_acl_protection`, `subnet_v4` and `subnet_v6`.
 * Updated network service configs of type `mp2mp_vc` 
   to add properties `asns` and `ips`.
 * Update documentation of `ans` property on network service config.

## 2.3.0 (2022-08-09)

 * Added extensions endpoints.
 * Updated facilities to add properties `latitude` and `longitude`.
 * Updated product offerings to add properties `exchange_logo`, `product_logo`
   and `service_provider_logo`.
 * Updated network service configs of type `mp2mp_vc` to add properties `asns`
   and `ips`.

## 2.2.2 (2023-06-13)

 * Updated network services of type `mp2mp_vc` to add 
   properties `mac_acl_protection`, `subnet_v4` and `subnet_v6`.
 * Updated network service configs of type `mp2mp_vc` 
   to add properties `asns` and `ips`.
 * Update documentation of `ans` property on network service config.

## 2.2.1 (2022-04-19)

 * Updated port reservation to add missing `id`. (Issue #163)

## 2.2.0 (2022-04-13)
 
 * New entity port reservations:
   * New endpoints added GET, POST `/port-reservations`
   * New endpoints added GET, PUT, PATCH, DELETE `/port-reservations/{id}`
   * New endpoints added GET `/port-reservations/{id}/cancellation-policy`
   * New endpoints added GET, POST `/port-reservations/{id}/loa`
 * Updated connections:
   * New filters `contacts`, `role_assignments`.
   * New endpoints:
     * POST `/connections`.
     * PUT, PATCH, DELETE `/connections/{id}`.
     * GET, POST `/connections/{id}/loa`.
     * GET `/connections/{id}/cancellation-policy`.
   * New properties:
     * `port_reservations`
     * `pop`
     * `speed`
     * `capacity_allocated`
     * `capacity_allocation_limit`
     * `port_quantity`
     * `subscriber_side_demarcs`
     * `current_billing_start_date`
 * Updated network features to make properties optional 
   `ip_v4`, `ip_v6`.
 * Updated network feature configs:
   * Add new filters `contacts`, `role_assignments`.
   * Add new property `flags`.
 * Updated network services:
   * `p2p_vc`, `p2mp_vc`, `mp2mp_vc` property added `display_name`
   * `p2p_vc`, `p2mp_vc`, `mp2mp_vc`, `cloud_vc` 
      property added `current_billing_start_date`
 * Updated network service configs:
   * Add new filters `contacts`, `role_assignments`.
   * `p2p_vc`, `p2mp_vc`, `mp2mp_vc`, `cloud_vc` 
     property added `current_billing_start_date`
 * Updated product offerings:
   * New type `connection`.
   * New properties:
     * `physical_port_speed`
     * `service_provider`
     * `downgrade_allowed`
     * `upgrade_allowed`
     * `orderable_not_before`
     * `orderable_not_after`
     * `notice_period`
   * Removed property `provider_vlans`.
   * New filters `resource_type`, `handover_pop`.
 * Updated problems:
   * New problem `ConstraintViolation`.
   * Updated `ValidationError` to add property `properties`
     for additional debugging information.
