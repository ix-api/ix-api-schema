
"""
API v2 introspection endpoints
"""

from ixapi_schema.v2 import filters
from ixapi_schema.v2.entities import introspection, problems

PATHS = {
    "/health": {
        "description": """
            This endpoint provides a health status response implementing
            https://tools.ietf.org/id/draft-inadarei-api-health-check-04.html

            The schema describes the toplevel fields - however the
            implementation of health checks is up to the IX-API implementor.
        """,
        "GET": {
            "description": """
                Get the IX-API service health status.
            """,
            "operation": "api_health_read",
            "responses": {
                200: introspection.ApiHealth(),
            },
        },
    },
    "/implementation": {
        "description": """
            This endpoint provides information about the IX-API
            implementation of the exchange.

            You can find the implemented schema version and
            supported types and operations here.
        """,
        "GET": {
            "description": """
                Get the API implementation details.
            """,
            "operation": "api_implementation_read",
            "responses": {
                200: introspection.ApiImplementation(),
            },
        },
    },
    "/extensions": {
        "description": """
            The extensions endpoint provides information about
            additional implementation specific extensions that are not
            officially part of the IX-API standard.
        """,
        "GET": {
            "description": """
            List provider extensions to the IX-API.
            """,
            "operation": "api_extensions_list",
            "filters": filters.FilterSet({
                k: f for k, f in filters.COLLECTION.items()
                if not k == "id"
            }),
            "responses": {
                200: introspection.ApiExtension(many=True),
                206: introspection.ApiExtension(many=True),
            },
            "problems": [ problems.ValidationErrorProblem() ],
        },
    },
}
