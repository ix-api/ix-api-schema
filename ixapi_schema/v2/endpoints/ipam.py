

"""
IPAM
====

IP- and MAC-address related endpoints.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2 import filters
from ixapi_schema.v2.entities import ipam, problems


PATHS = {
    #
    # IP Addresses
    #
    "/ips": {
        "description": """
            An `IP` is a IPv4 or 6 addresses, with a given validity period.
            Some services require IP addresses to work.

            When you are joining an `exchange_lan` network service
            for example, addresses on the peering lan will be assigned
            to you.
        """,
        "GET": {
            "description": """
                List all ip addresses (and prefixes).
            """,
            "responses": {
                200: ipam.IpAddress(many=True),
                206: ipam.IpAddress(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.OWNABLE,
                {
                    "network_service": components.CharFilter(),
                    "network_service_config": components.CharFilter(),
                    "network_feature": components.CharFilter(),
                    "network_feature_config": components.CharFilter(),
                    "version": components.NumberFilter(),
                    "fqdn": components.CharFilter(),
                    "prefix_length": components.NumberFilter(),
                    "valid_not_before": components.CharFilter(),
                    "valid_not_after": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """
                Add an ip host address or network prefix.
            """,
            "request": ipam.IpAddressRequest(),
            "responses": {
                201: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/ips/<id:str>": {
        "GET": {
            "description": """
                Get a single ip addresses by it's id.
            """,
            "responses": {
                200: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update an ip address object.

                You can only update
                IP addresses within your current scope. Not all
                addresses you can read you can update.

                If the ip address was allocated for you, you might
                not be able to change anything but the `fqdn`.
            """,
            "request": ipam.IpAddressUpdate(),
            "responses": {
                200: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """
                Update an ip address.

                You can only update
                IP addresses within your current scope. Not all
                addresses you can read you can update.

                If the ip address was allocated for you, you might
                not be able to change anything but the `fqdn`.
            """,
            "request": ipam.IpAddressPatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                200: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
    },
    #
    # Mac Addresses
    #
    "/macs": {
        "description": """
            A `MAC` is a MAC addresses with a given validity period.

            Some services require MAC addresses to work.
            Please note that only *unicast* MAC addresses are allowed.
            You are not alllowed to use multicast or broadcast addresses.

            The address itself can not be updated after creation.
            However: It can be expired by changing the `valid_not_before`
            and `valid_not_after` properties.

            Setting the above two parameters supports maintenance windows
            in which the MAC addresses overlap for certain period of time.
            The implementation of the maintenance windows is dependent on
            the specific exchange. While some IXPs allow them, the business
            of other IXPs may reject them.

            If both values are set `valid_not_after` may not be before
            `valid_not_before`.

        """,
        "GET": {
            "description": """
                List all mac addresses managed by the authorized customer.
            """,
            "responses": {
                200: ipam.MacAddress(many=True),
                206: ipam.MacAddress(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.OWNABLE,
                {
                    "network_service_config": components.CharFilter(),
                    "address": components.CharFilter(),
                    "valid_not_before": components.CharFilter(),
                    "valid_not_after": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """
                Register a mac address.
            """,
            "request": ipam.MacAddressRequest(),
            "responses": {
                201: ipam.MacAddress(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/macs/<id:str>": {
        "GET": {
            "description": """
                Get a single mac address by it's id.
            """,
            "responses": {
                200: ipam.MacAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "DELETE": {
            "description": """
                Remove a mac address.
            """,
            "responses": {
                200: ipam.MacAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
            ],
        },
    },
}
