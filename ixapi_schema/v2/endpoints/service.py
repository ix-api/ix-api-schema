
"""
Service Endpoints
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2 import filters
from ixapi_schema.v2.entities import (
    service,
    problems,
    cancellation,
    statistics,
)
from ixapi_schema.v2.constants.service import (
    NETWORK_SERVICE_TYPES,
    NETWORK_FEATURE_TYPES,
)
from ixapi_schema.v2.filters.statistics import (
    TIMESERIES_FILTER_SET,
    RTT_FILTER_SET,
)
from ixapi_schema.v2.entities.statistics import (
    RTT_EVENTS_SSE_BODY,
    RTT_EVENTS_EXAMPLE_SHORT,
)


PATHS = {
    #
    # Network Services
    #
    "/network-services": {
        "description": """
            A `NetworkService` represents an instance of a `ProductOffering`.

            The exchange lan is a special case in which the `NetworkService` is
            managed by the exchange.

            All other `NetworkService`s are created and managed by an `Account`.

            *Sensitive Properties*: Please note, that a `NetworkService` may be a shared
            resource and fields marked as *sensitive* should be redacted.
        """,
        "GET": {
            "description": """
                List available `NetworkService`s.
            """,
            "responses": {
                200: service.NetworkService(many=True),
                206: service.NetworkService(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        NETWORK_SERVICE_TYPES),
                    "pop": components.CharFilter(),
                    "product_offering": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """
                Create a new network service
            """,
            "request": service.NetworkServiceRequest(),
            "responses": {
                201: service.NetworkService(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        }
    },
    "/network-services/<id:str>": {
        "GET": {
            "description": """
                Get a specific `network-service` by id.
            """,
            "responses": {
                200: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update a network service.
            """,
            "request": service.NetworkServiceUpdate(),
            "responses": {
                202: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """Update a network service""",
            "request": service.NetworkServicePatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                202: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
            Request decomissioning of the network service.

            The network service will enter the state of
            `decommission_requested`. The request will
            cascade to related network service and feature
            configs.

            An *optional request body* can be provided to request
            a specific service termination date.

            If no date is given in the request body, it is assumed to
            be the earliest possible date.

            Possible values for `decommission_at` can be queried
            through the `network_service_cancellation_policy_read`
            operation.

            The response will contain the dates on which the
            changes will be effected.
            """,
            "request": cancellation.CancellationRequest(),
            "responses": {
                202: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.CancellationPolicyErrorProblem(),
                problems.UnableToFulfillProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
    "/network-services/<id:str>/statistics": {
        "GET": {
            "operation": "network_services_statistics_read",
            "description": """Read a `NetworkService`s aggregated statistics.

            """ + statistics.DESCRIPTION_STATS_AGGREGATED,
            "filters": components.FilterSet({
                "start": components.DateTimeFilter(
                    label="""
                        Beginning of the traffic aggregation.
                    """
                ),
                "end": components.DateTimeFilter(
                    label="""
                        End of the traffic aggregation.
                        Default is `now`.
                    """
                ),
            }),
            "responses": {
                200: statistics.Aggregate(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-services/<id:str>/statistics/<aggregate:str>/timeseries": {
        "GET": {
            "operation": "network_services_statistics_timeseries_read",
            "description": """
                Read a `NetworkService`s aggregated timeseries.
                The resolution is defined by the aggregate.
            """,
            "filters": TIMESERIES_FILTER_SET,
            "responses": {
                200: statistics.AggregateTimeseries(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-services/<id:str>/rtt-statistics": {
        "GET": {
            "operation": "network_services_statistics_rtt_read",
            "description": """
                Get **rtt statistics** from neighbors consuming this network
                service. You can filter by `asn` and peer `ip`.

                If the `NetworkService` does not support RTT statistics,
                a `404` error response will be returned.

                ### Receiving Updates
                To poll for updates, you can provide a `serial` to the
                `after` parameter in order to only receive
                results with a serial greater than the one
                provided. All of the above filters can be applied
                here and allow for receiving updates for a specific peer.

                If no `serial` is provided the latest RTT statistics are returned.

                ### Event Streaming
                This endpoint supports streaming updates using
                [**Server Sent Events**](https://html.spec.whatwg.org/multipage/server-sent-events.html#server-sent-events).

                To subscribe to events, negotiate the response content
                using the HTTP header: `Accept: text/event-stream`.

                Filteres are applied as above.
            """,
            "filters": RTT_FILTER_SET,
            "produces": ["text/event-stream","application/json"],
            "responses": {
                200: {
                    "application/json": statistics.PeerRTT(many=True),
                    "text/event-stream": {
                        "schema": {
                            "type": "string",
                            "description": RTT_EVENTS_SSE_BODY,
                        },
                        "examples": {
                            "streamExample": {
                                "value": RTT_EVENTS_EXAMPLE_SHORT,
                            },
                        },
                    },
                },
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-services/<id:str>/change-request": {
        "POST": {
            "operation": "network_service_change_request_create",
            "description": """
                Request a change to the network service.

                The B-side participant in a peer-to-peer network service
                (`p2p_vc`) can issue a change request, expressing a
                desired change in the capacity.

                The change is accepted when the A-side has configured
                the network service and config with the new bandwidth.
                This is done using the `network_service_update`,
                `network_service_partial_update`,
                `network_service_config_update` or
                `network_service_config_partial_update` operations by
                the A-side.

                These changes can sometimes require a change of the
                product offering. The product offering may only
                differ in bandwidth.

                The network service will change its state from `production`
                into `production_change_pending`.

                A change can by rejected (by the A-side) or retracted
                (by the B-side) using the
                `network_service_change_request_destroy` operation.

                Only one change request may be issued at a time.

                A change request by the A-side is not a valid request
                and will be rejected.
            """,
            "request": service.NetworkServiceChangeRequest(),
            "responses": {202: service.NetworkServiceChangeRequest()},
            "problems": problems.PROTECTED_COLLECTION,
        },
        "GET": {
            "operation": "network_service_change_request_read",
            "description": "Get the change request.",
            "responses": {202: service.NetworkServiceChangeRequest()},
            "problems": problems.PROTECTED_RESOURCE,
        },
        "DELETE": {
            "operation": "network_service_change_request_destroy",
            "description": "Retract or reject a change to the network service.",
            "responses": {202: service.NetworkServiceChangeRequest()},
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
            ],
        },
    },
    "/network-services/<id:str>/cancellation-policy": {
        "GET": {
            "operation": "network_service_cancellation_policy_read",
            "description": """
                The cancellation-policy can be queried to answer
                the questions:

                If I cancel my service, *when will it be technically
                decommissioned*?
                If I cancel my service, *until what date will I be charged*?

                When the query parameter `decommision_at` is not provided
                it will provide the first possible cancellation date
                and charge period if cancelled at above date.

                The granularity of the date field is a day, the start and end
                of which are to be interpreted by the IXP (some may use UTC,
                some may use their local time zone).
            """,
            "responses": {200: cancellation.CancellationPolicy()},
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
            "filters": components.FilterSet({
                "decommission_at": components.CharFilter(label="""
                    By providing a date in the format `YYYY-MM-DD` you can
                    query the policy what would happen if you request a
                    decommissioning on this date.
                """)
            }),
        },
    },

    #
    # Network Features
    #
    "/network-features": {
        "description": """
            A `NetworkFeature` represents additional funcationality of a single
            `NetworkService`.

            For certain `NetworkFeature`s, which are marked as required,
            one `NetworkFeatureConfig needs to be created in order to move
            the  `NetworkServiceConfig` into `production`.
        """,
        "GET": {
            "description": """
                List available network features.
            """,
            "responses": {
                200: service.NetworkFeature(many=True),
                206: service.NetworkFeature(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection({
                "type": components.ChoiceFilter(NETWORK_FEATURE_TYPES),
                "required": components.CharFilter(),
                "network_service": components.CharFilter(),
                "name": components.CharFilter(),
            }),
        },
    },
    "/network-features/<id:str>": {
        "GET": {
            "description": """
                Get a single network feature by it's id.
            """,
            "responses": {
                200: service.NetworkFeature(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    "/member-joining-rules": {
        "description": """
            A `MemberJoiningRule` defines a rule to allow or deny access for
            an `Account` to a access a `NetworkService`.

            Furthermore, some `NetworkService`s may only be visible if the
            querying account is listed in an allow rule.
        """,
        "GET": {
            "description": """Get a list of joining rules""",
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE,
                {
                    "network_service": components.CharFilter(),
                },
            ),
            "responses": {
                200: service.MemberJoiningRule(many=True),
                206: service.MemberJoiningRule(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
        "POST": {
            "description": """Create a member joining rule""",
            "request": service.MemberJoiningRuleRequest(),
            "responses": {
                201: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        }
    },
    "/member-joining-rules/<id:str>": {
        "GET": {
            "description": """
                Get a single rule
            """,
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PATCH": {
            "description": """Update a joining rule""",
            "request": service.MemberJoiningRulePatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update a joining rule.
            """,
            "request": service.MemberJoiningRuleUpdate(),
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """Delete a joining rule""",
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
            ],
        }
    },
    "/routing-functions": {
        "description": """
            Routing functions instances add routing functionality
            implemented as VPRNs at the IXP.

            A routing function instance joins two or more independent services
            into a single routing domain, providing connectivity between all of
            the services attached.

            To create routing function, need to select a product offering,
            which will determine the service metro area network where
            the function will be deployed.
        """,
        "GET": {
            "description": """Get a list of routing functions""",
            "filters": filters.Collection(
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "asn": components.CharFilter(),
                },
            ),
            "responses": {
                200: service.RoutingFunction(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
        "POST": {
            "description": """Create a routing function""",
            "request": service.RoutingFunctionRequest(),
            "responses": {
                201: service.RoutingFunction(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/routing-functions/<id:str>": {
        "GET": {
            "description": """
                Get a single routing function instance.
            """,
            "responses": {
                200: service.RoutingFunction(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PATCH": {
            "description": """Update a routing function""",
            "request": service.RoutingFunctionPatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                200: service.RoutingFunction(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                Request decomissioning the routing function.

                The cancellation policy of the routing function applies
                here and is independent from the
                policy of the network-service and network-service-config
                using the routing function.

                The routing function will assume the state
                `decommission_requested`.

                The decommissioning request will *not* cascade
                to network services and configs.
            """,
            "request": cancellation.CancellationRequest(),
            "responses": {
                202: service.RoutingFunction(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.CancellationPolicyErrorProblem(),
                problems.UnableToFulfillProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
    "/routing-functions/<id:str>/cancellation-policy": {
        "GET": {
            "operation": "routing_functions_cancellation_policy_read",
            "description": """
                The cancellation-policy can be queried to answer
                the questions:

                If I cancel my subscription, *when will it be technically
                decommissioned*?
                If I cancel my subscription, *until what date will I be charged*?

                When the query parameter `decommision_at` is not provided
                it will provide the first possible cancellation date
                and charge period if cancelled at above date.

                The granularity of the date field is a day, the start and end
                of which are to be interpreted by the IXP (some may use UTC,
                some may use their local time zone).
            """,
            "responses": {200: cancellation.CancellationPolicy()},
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
            "filters": components.FilterSet({
                "decommission_at": components.CharFilter(label="""
                    By providing a date in the format `YYYY-MM-DD` you can
                    query the policy what would happen if you request a
                    decommissioning on this date.
                """)
            }),
        },
    },
}
