
"""
CRM
---

Manage accounts, roles and contacts.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import crm, problems
from ixapi_schema.v2 import filters


PATHS = {
    #
    # Accounts
    #
    "/account": {
        "tag": "accounts",
        "GET": {
            "operation": "account_read",
            "description": "Get the currently authenticated `Account`.",
            "responses": {
                200: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    "/accounts": {
        "description": """
            An `Account` represents an individual customer account, organization
            or partner involved with the IXP. They are used to consume
            services from an IXP.

            Accounts can have a hierarchy, i.e. an
            account can have sub-accounts. The max-depth of
            the account-hierarchy may vary by implementer, but it's safe to
            assume an account can have sub-accounts.

            Each `Account` has a state. Only `Account`s in state `production`
            or `production_change_pending` are ready to consume services.

            There are `Contact`s associated with each account. Contacts can
            be assigned for `Role`s via `RoleAssignment`s. Depending on the
            IXP and the services the account wants to use, contacts with
            specific roles may be required.
            A contact with role `legal` is mandatory for an account to become
            operational.

            Only accounts with `billing_information` present can be used
            as a `billing_account`.

            *Sensitive Properties*: Please note, that an `Account` is a shared
            resource and fields marked as *sensitive* should be redacted.
        """,
        "GET": {
            "description": """
                Retrieve a list of `Account`s.

                This includes all accounts the currently authorized account
                is managing and the current account itself.

                Also `discoverable` accounts will be included, however
                sensitive properties, like `address` or `external_ref` will
                either not be present or redacted.
            """,
            "responses": {
                200: crm.Account(many=True),
                206: crm.Account(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.STATEFUL,
                {
                    "managing_account": components.CharFilter(),
                    "billable": components.NumberFilter(),
                    "external_ref": components.CharFilter(),
                    "name": components.CharFilter(),
                }
            ),
        },
        "POST": {
            "description": """
                Create a new account.
            """,
            "request": crm.AccountRequest(),
            "responses": {
                201: crm.Account(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/accounts/<id:str>": {
        "GET": {
            "description": "Get a single account.",
            "responses": {
                200: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update the entire account.
            """,
            "request": crm.AccountUpdate(),
            "responses": {
                202: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": "Update an account.",
            "request": crm.AccountPatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                202: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                Accounts can be deleted, when all services and configs
                are decommissioned or the account is not longer referenced
                e.g. as a `managing_account` or `billing_account`.

                Deleting an account will cascade to `contacts` and
                `role-assignments`.

                The request will immediately fail, if the above preconditions
                are not met.
            """,
            "responses": {
                200: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
            ],
        },
    },
    #
    # Roles
    #
    "/roles": {
        "description": """
            A `Role` enables a `Contact` to act for a specific purpose.

            ### Well-defined roles

            - `legal` signing terms and conditions for the account as a whole, authorized person allowed to sign on behalf of the company
            - `implementation` technical contact for deployment and de-commissioning
            - `noc` technical contact for troubleshooting the services
            - `peering` contact authorized to accept peering requests
            - `billing` contact that receives invoices

            An implementer may choose to support them or not or may add
            additional ones.
        """,
        "GET": {
            "description": """
                List all roles available.
            """,
            "responses": {
                200: crm.Role(many=True),
                206: crm.Role(many=True),
            },
            "problems":  problems.PROTECTED_COLLECTION,
            "filters": filters.Collection({
                "name": components.CharFilter(),
                "contact": components.CharFilter(),
            }),
        }
    },
    "/roles/<id:str>": {
        "GET": {
            "description": """
                Get a single `Role`.
            """,
            "responses": {
                200: crm.Role(),
            },
            "problems":  [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        }
    },
    #
    # Contacts
    #
    "/contacts": {
        "description": """
            A `Contact` is a role undertaking a specific responsibility
            within an account, typically a department or agent of the
            customer company.

            `Contact`s are assigned multiple roles by `RoleAssignment`s.

            A contact is bound to the account by the `consuming_account` property.
        """,
        "GET": {
            "description": """
                List available contacts managed by the authorized account.
            """,
            "responses": {
                200: crm.Contact(many=True),
                206: crm.Contact(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.OWNABLE,
                {"external_ref": components.CharFilter()},
            ),
        },
        "POST": {
            "description": """
                Create a new contact.
            """,
            "request": crm.ContactRequest(),
            "responses": {
                201: crm.Contact(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/contacts/<id:str>": {
        "GET": {
            "description": """Get a contact by it's id""",
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update a contact.
            """,
            "request": crm.ContactUpdate(),
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """Update a contact.""",
            "request": crm.ContactPatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
            Remove a contact.

            Please note, that a contact can only be removed if
            it is not longer in use in a network service or config
            through a role assignment.
            """,
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
                problems.ConstraintViolationProblem(),
            ],
        },
    },
    #
    # Role Assignments
    #
    "/role-assignments": {
        "description": """
            A `Contact` can be assigned to many `Roles`.
        """,
        "GET": {
            "description": """
                List all role assignments for a contact.
            """,
            "responses": {
                200: crm.RoleAssignment(many=True),
                206: crm.RoleAssignment(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection({
                    "contact": components.CharFilter(),
                    "role": components.CharFilter(),
            }),
        },
        "POST": {
            "description": """
                Assign a `Role` to a `Contact`.

                The contact needs to have all fields filled, which the
                role requires. If this is not the case a `400`
                `UnableToFulfill` will be returned.
            """,
            "request": crm.RoleAssignmentRequest(),
            "responses": {
                201: crm.RoleAssignment(),
            },
            "problems": problems.PROTECTED_COLLECTION + [
                problems.UnableToFulfillProblem(),
            ],
        }
    },
    "/role-assignments/<id:str>": {
        "endpoint": "role-assignments",
        "GET": {
            "description": """
                Get a role assignment for a contact.
            """,
            "responses": {
                200: crm.RoleAssignment(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "DELETE": {
            "description": """
                Remove a role assignment from a contact.

                If the contact is still in use with a given role required,
                this will yield an `UnableToFulfill` error.
            """,
            "responses": {
                200: crm.RoleAssignment(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
                problems.ConstraintViolationProblem(),
            ],
        }
    },
}
