
"""
Catalog
-------

The catalog bundles endpoints to query the
ix platform for facilities, pops, devices and so on.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2 import filters
from ixapi_schema.v2.entities import catalog, problems
from ixapi_schema.v2.constants.catalog import (
    PRODUCT_TYPES,
    RESOURCE_TYPES,
    DELIVERY_METHODS,
)


PATHS = {
    #
    # Facilities
    #
    "/facilities": {
        "description": """
            A `Facility` is a data centre, with a determined physical address,
            from which a defined set of PoPs can be accessed
        """,
        "GET": {
            "description": """
                Get a (filtered) list of `facilities`.
            """,
            "responses": {
                200: catalog.Facility(many=True),
                206: catalog.Facility(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.Ordered({"capability_speed": components.NumberFilter()}),
                {
                    "capability_media_type": components.CharFilter(),
                    "organisation_name": components.CharFilter(),
                    "metro_area": components.CharFilter(),
                    "metro_area_network": components.CharFilter(),
                    "address_country": components.CharFilter(),
                    "address_locality": components.CharFilter(),
                    "postal_code": components.CharFilter(),
                },
            ),
        },
    },
    "/facilities/<id:str>": {
        "GET": {
            "description": """
                Retrieve a facility by id
            """,
            "responses": {
                200: catalog.Facility(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # Devices
    #
    "/devices": {
        "description": """
            A `Device` is a network hardware device, typically a switch, which
            is located at a specified facility and inside a `PointOfPresence`.
            PoPs.

            They may be physically located at their related PoPs.
        """,
        "GET": {
            "description": "List available devices",
            "responses": {
                200: catalog.Device(many=True),
                206: catalog.Device(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.Ordered({"capability_speed": components.NumberFilter()}),
                {
                    "name": components.CharFilter(),
                    "capability_media_type": components.CharFilter(),
                    "facility": components.CharFilter(),
                    "pop": components.CharFilter(),
                    "metro_area_network": components.CharFilter(),
                },
            ),
        },
    },
    "/devices/<id:str>": {
        "GET": {
            "description": "Get a specific device identified by id",
            "responses": {
                200: catalog.Device(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # Points Of Presence
    #
    "/pops": {
        "description": """
            A `PointOfPresence` is a technical installation within a Facility
            which is connected to a single `MetroAreaNetwork`. A single
            `MetroAreaNetwork`, however, may have multiple `PointOfPresence`s.
        """,
        "GET": {
            "description": "List all PoPs",
            "responses": {
                200: catalog.PointOfPresence(many=True),
                206: catalog.PointOfPresence(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection({
                    "facility": components.CharFilter(),
                    "metro_area_network": components.CharFilter(),
                    "capability_media_type": components.CharFilter(),
                },
                filters.Ordered({"capability_speed": components.NumberFilter()}),
            ),
        },
    },
    "/pops/<id:str>": {
        "GET": {
            "description": "Get a single point of presence",
            "responses": {
                200: catalog.PointOfPresence(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        }
    },
    #
    # Metro Area Networks
    #
    "/metro-area-networks": {
        "description": """
            Services are provided directly on or can be consumed
            from inside a `MetroAreaNetwork`.

            Accounts can indicate their presence in a `MetroAreaNetwork`
            to others by adding a list of ids.
        """,
        "GET": {
            "description": "List all MetroAreaNetworks",
            "responses": {
                200: catalog.MetroAreaNetwork(many=True),
                206: catalog.MetroAreaNetwork(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection({
                "name": components.CharFilter(),
                "metro_area": components.CharFilter(),
                "service_provider": components.CharFilter(),
            }),
        },
    },
    "/metro-area-networks/<id:str>": {
        "GET": {
            "description": "Retrieve a MetroAreaNetwork",
            "responses": {
                200: catalog.MetroAreaNetwork(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    "/metro-areas": {
        "description": """
            A MetroArea exists if a `MetroAreaNetwork` or `Facility` is
            present in it.
        """,
        "GET": {
            "description": "List all MetroAreas",
            "responses": {
                200: catalog.MetroArea(many=True),
                206: catalog.MetroArea(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(),
        },
    },
    "/metro-areas/<id:str>": {
        "GET": {
            "description": "Get a single MetroArea",
            "responses": {
                200: catalog.MetroArea(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # ProductsOfferings
    #
    "/product-offerings": {
        "description": """
            A `ProductOffering` is an offer made by an exchange
            to be consumed by a client.
        """,
        "GET": {
            "description": """
                List all (filtered) products-offerings available on the platform
            """,
            "responses": {
                200: catalog.ProductOfferingPatch(many=True, partial=True),
                206: catalog.ProductOfferingPatch(many=True, partial=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection({
                "type": components.ChoiceFilter(PRODUCT_TYPES),
                "resource_type": components.ChoiceFilter(RESOURCE_TYPES),
                "name": components.CharFilter(),
                "handover_metro_area": components.CharFilter(),
                "handover_metro_area_network": components.CharFilter(),
                "handover_pop": components.CharFilter(),
                "service_metro_area": components.CharFilter(),
                "service_metro_area_network": components.CharFilter(),
                "service_provider": components.CharFilter(),
                "downgrade_allowed": components.ChoiceFilter(["true", "false"]),
                "upgrade_allowed": components.ChoiceFilter(["true", "false"]),
                "bandwidth": components.NumberFilter(
                    label="""
                        Find product offerings where bandwidth is
                        within the range of `bandwidth_min` and `bandwidth_max`.
                    """),
                "physical_port_speed": components.NumberFilter(),
                "service_provider_region": components.CharFilter(),
                "service_provider_pop": components.CharFilter(),
                "delivery_method": components.ChoiceFilter(DELIVERY_METHODS),
                "cloud_key": components.CharFilter(
                    label="""
                        For product offerings of type `cloud_vc`, if the
                        `service_provider_workflow` is `provider_first` the
                        `cloud_key` will be used for filtering the relevant
                        offerings.
                    """),
                "contract_initial_period": components.CharFilter(),
                "contract_initial_notice_period": components.CharFilter(),
                "contract_renewal_period": components.CharFilter(),
                "contract_renewal_notice_period": components.CharFilter(),
                "fields": components.CharFilter(
                    label="""
                        Returned objects only have properties which are
                        present in the list of fields.
                        The required `type` property is *implicitly* included.
                        The results are *deduplicated*.

                        Example: handover_metro_area,service_provider
                    """)
            }),
        },
    },
    "/product-offerings/<id:str>": {
        "GET": {
            "description": """
                Get a single products-offering by id.
            """,
            "responses": {
                200: catalog.ProductOffering(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # Availability Zones
    #
    "/availability-zones":{
        "description":"""
            An `AvailabilityZone` is a grouping of network resources that have
            the same maintenance scheduling which does not overlap with the
            maintenance schedule for any other `AvailabilityZone`.
        """,
        "GET":{
            "description":"""
                List all availability zones available on the platform.
            """,
            "responses":{
                200: catalog.AvailabilityZone(many = True, partial = True),
                206: catalog.AvailabilityZone(many = True, partial = True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters" : filters.Collection({
                "name": components.CharFilter(),
            }),
        },
    },
    "/availability-zones/<id:str>":{
        "GET": {
            "description": """
                Get a single availability zone by id.
            """,
            "responses": {
                200: catalog.AvailabilityZone(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    }
}
