
"""
Access Endpoints
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2 import filters
from ixapi_schema.v2.entities import (
    config,
    problems,
    cancellation,
    statistics,
)
from ixapi_schema.v2.filters.statistics import (
    TIMESERIES_FILTER_SET,
)



PATHS = {
    #
    # Device Ports
    #
    "/ports": {
        "description": """
            A `Port` is the point at which subscriber and
            IXP networks meet. A port is always associated with
            a `device` and `pop`, has a `speed` and a `media_type`.
        """,
        "GET": {
            "description": """List all ports.""",
            "responses": {
                200: config.Port(many=True),
                206: config.Port(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "media_type": components.CharFilter(),
                    "pop": components.CharFilter(),
                    "name": components.CharFilter(),
                    "device": components.CharFilter(),
                    "speed": components.CharFilter(),
                    "connection": components.CharFilter(),
                },
            ),
        },
    },
    "/ports/<id:str>": {
        "GET": {
            "description": """Retrieve a port.""",
            "responses": {
                200: config.Port(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    "/ports/<id:str>/statistics": {
        "GET": {
            "operation": "ports_statistics_read",
            "description": """Read a `Port`s aggregated statistics.

            """ + statistics.DESCRIPTION_STATS_AGGREGATED,
            "filters": components.FilterSet({
                "start": components.DateTimeFilter(
                    label="""
                        Beginning of the traffic aggregation.
                    """
                ),
                "end": components.DateTimeFilter(
                    label="""
                        End of the traffic aggregation.
                        Default is `now`.
                    """
                ),
            }),
            "responses": {
                200: statistics.PortStatistics(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/ports/<id:str>/statistics/<aggregate:str>/timeseries": {
        "GET": {
            "operation": "ports_statistics_timeseries_read",
            "description": """
                Read a `Port`s aggregated timeseries.
                The resolution is defined by the aggregate.
            """,
            "filters": TIMESERIES_FILTER_SET,
            "responses": {
                200: statistics.AggregateTimeseries(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },

    #
    # Port Reservations
    #
    "/port-reservations": {
        "description": """
            A `PortReservation` expresses the intent to include
            a `Port` in a connection.

            Decommissining a `port-reservation` will lead to the
            removal of the port from the lag.

            Please note that individual cancellation policies
            might apply.
        """,
        "GET": {
            "description": """List all port reservations.""",
            "responses": {
                200: config.PortReservation(many=True),
                206: config.PortReservation(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.STATEFUL,
                {
                    "connection": components.CharFilter(),
                    "port": components.CharFilter(),
                    "external_ref": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """
                Create a new `PortReservation`.

                Two workflows for allocating ports is supported and
                dependent on the `cross_connect_initiator` property
                of the corresponding `product-offering`:

                Individual LOAs can be uploaded and downloaded for
                each PortAllocation using the endpoint
                `/port-reservations/{id}/loa`.

                Please refer to the internet exchange's api usage
                guide for implementation specific details.
            """,
            "responses": {
                201: config.PortReservation(),
            },
            "request": config.PortReservationRequest(),
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/port-reservations/<id:str>": {
        "GET": {
            "description": """Retrieve a `PortReservation`.""",
            "responses": {
                200: config.PortReservation(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update a port reservation.
            """,
            "responses": {
                202: config.PortReservation(),
            },
            "request": config.PortReservationUpdate(),
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """
                Update a port reservation.
            """,
            "responses": {
                202: config.PortReservation(),
            },
            "request": config.PortReservationPatch(partial=True),
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                Request decommissioning the port-reservation.
                The associated `port` will be deallocated and
                removed from the `connection`.
            """,
            "request": cancellation.CancellationRequest(),
            "responses": {
                202: config.PortReservation(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.CancellationPolicyErrorProblem(),
                problems.UnableToFulfillProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
    "/port-reservations/<id:str>/cancellation-policy": {
        "GET": {
            "operation": "port_reservation_cancellation_policy_read",
            "description": """
                The cancellation-policy can be queried to answer
                the questions:

                If I cancel my subscription, *when will it be technically
                decommissioned*?
                If I cancel my subscription, *until what date will I be charged*?

                When the query parameter `decommision_at` is not provided
                it will provide the first possible cancellation date
                and charge period if cancelled at above date.

                The granularity of the date field is a day, the start and end
                of which are to be interpreted by the IXP (some may use UTC,
                some may use their local time zone).
            """,
            "responses": {200: cancellation.CancellationPolicy()},
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
            "filters": components.FilterSet({
                "decommission_at": components.CharFilter(label="""
                    By providing a date in the format `YYYY-MM-DD` you can
                    query the policy what would happen if you request a
                    decommissioning on this date.
                """)
            }),
        },
    },
    "/port-reservations/<id:str>/loa": {
        "GET": {
            "operation": "port_reservations_loa_download",
            "description": """
                Download the *Letter Of Authorization* associated
                with the port-reservation.

                In case of a *subscriber initiated cross-connect*,
                it will be provided by the exchange.
            """,
            "responses": {
                200: "LOA",
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "POST": {
            "operation": "port_reservations_loa_upload",
            "description": """
                Upload a *Letter Of Authorization* for this
                `PortReservation`.
            """,
            "responses": {
                200: "UPLOAD_SUCCESS",
            },
            "request": "LOA",
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
    },
    #
    # Connections
    #
    "/connections": {
        "description": """
            A `Connection` is a functional group of physical
            ports collected together into a LAG (aka trunk).

            A `connection` with only one `port` can be also
            configured as standalone which means no link aggregation
            is configured on the switch.

            Ports will be allocated when creating a connection.
            The number of ports is determined by the
            `port_quantity` property.
            The `port_quantity` can be updated later by creating
            new or deleting `PortReservation`s for this `Connection`.

        """,
        "GET": {
            "description": """List all `connection`s.""",
            "responses": {
                200: config.Connection(many=True),
                206: config.Connection(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "mode": components.CharFilter(),
                    "mode__is_not": components.CharFilter(),
                    "name": components.CharFilter(),
                    "metro_area": components.CharFilter(
                        label="Filter connections by ID of the metro_area",
                    ),
                    "metro_area_network": components.CharFilter(
                        label="Filter connections by ID of the metro_area_network",
                    ),
                    "pop": components.CharFilter(),
                    "facility": components.CharFilter(),
                    "role_assignments": components.BulkIdFilter(),
                    "contacts": components.BulkIdFilter(),
                    "supported_network_service": components.CharFilter(
                        label="""
                            Filter connections that can be used in a network service
                            config for the service identified by ID
                        """,
                    ),
                },
            ),
        },
        "POST": {
            "description": """
                Create a new `connection` and request ports
                allocation.

                Two workflows for allocating ports is supported and
                dependent on the `cross_connect_initiator` property
                of the corresponding `product-offering`:

                When the initiator is the `subscriber`, a Letter Of
                Authorization (LOA) can
                be downloaded from the `/connection/<id>/loa`
                resource. In case the `exchange` is the initiator,
                the LOA can be uploaded to this resource.

                Creating a connection will also create
                PortReservations. See the `port_quantity` and
                `subscriber_side_demarcs` attributes for details.

                Please refer to the internet exchange's api usage
                guide for implementation specific details.
            """,
            "responses": {
                201: config.Connection(),
            },
            "request": config.ConnectionRequest(),
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/connections/<id:str>": {
        "GET": {
            "description": """Read a `connection`.""",
            "responses": {
                200: config.Connection(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update a connection.
            """,
            "responses": {
                202: config.Connection(),
            },
            "request": config.ConnectionUpdate(),
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """
                Update a connection.
            """,
            "responses": {
                202: config.Connection(),
            },
            "request": config.ConnectionPatch(partial=True),
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                Request decommissioning the connection.

                The cancellation policy of the connection applies
                here and is independent from the
                policy of the network-service and network-service-config
                using the connection.

                The connection will assume the state
                `decommission_requested`.

                Associated `port-reservation` will be also
                marked for decommissining and ports will
                be deallocated.

                The decommissioning request will *not* cascade
                to network services and configs.
            """,
            "request": cancellation.CancellationRequest(),
            "responses": {
                202: config.Connection(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.CancellationPolicyErrorProblem(),
                problems.UnableToFulfillProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
    "/connections/<id:str>/statistics": {
        "GET": {
            "operation": "connections_statistics_read",
            "description": """Read a `Connection`s aggregated statistics.

            """ + statistics.DESCRIPTION_STATS_AGGREGATED,
            "filters": components.FilterSet({
                "start": components.DateTimeFilter(
                    label="""
                        Beginning of the traffic aggregation.
                    """
                ),
                "end": components.DateTimeFilter(
                    label="""
                        End of the traffic aggregation.
                        Default is `now`.
                    """
                ),
            }),
            "responses": {
                200: statistics.Aggregate(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/connections/<id:str>/statistics/<aggregate:str>/timeseries": {
        "GET": {
            "operation": "connections_statistics_timeseries_read",
            "description": """
                Read a `Connection`s aggregated timeseries.
                The resolution is defined by the aggregate.
            """,
            "filters": TIMESERIES_FILTER_SET,
            "responses": {
                200: statistics.AggregateTimeseries(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/connections/<id:str>/loa": {
        "GET": {
            "operation": "connections_loa_download",
            "description": """
                Download the *Letter Of Authorization* associated with the `connection`.
                In case of a *subscriber initiated cross-connect*,
                it will be provided by the exchange.
            """,
            "responses": {
                200: "LOA",
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "POST": {
            "operation": "connections_loa_upload",
            "description": """
                Upload a *Letter Of Authorization* for this
                `connection`.

                The LOA is valid for the entire connection and must
                include all ports.
            """,
            "responses": {
                200: "UPLOAD_SUCCESS",
            },
            "request": "LOA",
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
    },
    "/connections/<id:str>/cancellation-policy": {
        "GET": {
            "operation": "connections_cancellation_policy_read",
            "description": """
                The cancellation-policy can be queried to answer
                the questions:

                If I cancel my subscription, *when will it be technically
                decommissioned*?
                If I cancel my subscription, *until what date will I be charged*?

                When the query parameter `decommision_at` is not provided
                it will provide the first possible cancellation date
                and charge period if cancelled at above date.

                The granularity of the date field is a day, the start and end
                of which are to be interpreted by the IXP (some may use UTC,
                some may use their local time zone).
            """,
            "responses": {200: cancellation.CancellationPolicy()},
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
            "filters": components.FilterSet({
                "decommission_at": components.CharFilter(label="""
                    By providing a date in the format `YYYY-MM-DD` you can
                    query the policy what would happen if you request a
                    decommissioning on this date.
                """)
            }),
        },
    },
    #
    # NetworkServiceConfigs
    #
    "/network-service-configs": {
        "description": """
            A `NetworkServiceConfig` is a customer's configuration for usage
            of a `NetworkService`, e.g. the configuration of a (subset of a)
            connection for that customer's traffic

            The `type` of the config determines the service you are
            configuring.

            You can find the services available to you on the platform,
            by querying the `NetworkService`s resource.

            For certain `NetworkFeature`s, which are marked as required,
            one `NetworkFeatureConfig` needs to be created in order to move
            the  `NetworkServiceConfig` into `production`.
        """,
        "GET": {
            "description": """Get all `network-service-config`s.""",
            "responses": {
                200: config.NetworkServiceConfig(many=True),
                206: config.NetworkServiceConfig(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        config.NETWORK_SERVICE_CONFIG_TYPES),
                    "inner_vlan": components.NumberFilter(),
                    "outer_vlan": components.NumberFilter(),
                    "capacity": components.NumberFilter(),
                    "external_ref": components.CharFilter(),
                    "network_service": components.CharFilter(),
                    "connection": components.CharFilter(),
                    "product_offering": components.CharFilter(),
                    "role_assignments": components.BulkIdFilter(),
                    "contacts": components.BulkIdFilter(),
                },
            ),
        },
        "POST": {
            "description": """Create a `network-service-config`.""",
            "request": config.NetworkServiceConfigRequest(),
            "responses": {
                201: config.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/network-service-configs/<id:str>": {
        "GET": {
            "description": """Get a `network-service-config`""",
            "responses": {
                200: config.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update an exisiting `network-service-config`
            """,
            "request": config.NetworkServiceConfigUpdate(),
            "responses": {
                202: config.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """
                Update an exisiting `network-service-config`.
            """,
            "request": config.NetworkServiceConfigPatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                202: config.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                Request decommissioning the network service configuration.

                The network service config will assume the state
                `decommission_requested`.
                This will cascade to related resources like
                `network-feature-configs`.
            """,
            "request": cancellation.CancellationRequest(),
            "responses": {
                202: config.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.CancellationPolicyErrorProblem(),
                problems.UnableToFulfillProblem(),
                problems.ValidationErrorProblem(),
            ]
        },
    },
    "/network-service-configs/<id:str>/statistics": {
        "GET": {
            "operation": "network_service_configs_statistics_read",
            "description": """Read the aggregated statistics of a `NetworkServiceConfig`.
            """ + statistics.DESCRIPTION_STATS_AGGREGATED,
            "filters": components.FilterSet({
                "start": components.DateTimeFilter(
                    label="""
                        Beginning of the traffic aggregation.
                    """
                ),
                "end": components.DateTimeFilter(
                    label="""
                        End of the traffic aggregation.
                        Default is `now`.
                    """
                ),
            }),
            "responses": {
                200: statistics.NetworkServiceConfigAggregate(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-service-configs/<id:str>/statistics/<aggregate:str>/timeseries": {
        "GET": {
            "operation": "network_service_configs_statistics_timeseries_read",
            "description": """
                Read a `NetworkServiceConfig`s aggregated timeseries.
                The resolution is defined by the aggregate.
            """,
            "filters": TIMESERIES_FILTER_SET,
            "responses": {
                200: statistics.AggregateTimeseries(),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-service-configs/<id:str>/peer-statistics": {
        "GET": {
            "operation": "network_service_configs_peer_statistics_read",
            "description": """Read the aggregated peer to peer statistics in relation
                to the peer represented by the
                `NetworkServiceConfig`.
                This operation will return a list of aggregated statistics.
            """ + statistics.DESCRIPTION_STATS_AGGREGATED,
            "filters": components.FilterSet({
                "start": components.DateTimeFilter(
                    label="""
                        Beginning of the traffic aggregation.
                    """
                ),
                "end": components.DateTimeFilter(
                    label="""
                        End of the traffic aggregation.
                        Default is `now`.
                    """
                ),
                "asn": components.NumberFilter(),
                "mac_address": components.CharFilter(),
                "ip_address": components.CharFilter(),
                "ip_version": components.NumberFilter(),
            }),
            "responses": {
                200: statistics.PeerAggregate(many=True),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-service-configs/<id:str>/peer-statistics/<aggregate:str>/timeseries": {
        "GET": {
            "operation": "network_service_configs_peer_statistics_timeseries_read",
            "description": """
                Read peer to peer aggregated timeseries.
                The resolution is defined by the aggregate.

                This operation will return a list of timeseries for each peer.
            """,
            "filters": components.FilterSet(
                TIMESERIES_FILTER_SET,
                {
                    "asn": components.NumberFilter(),
                    "mac_address": components.CharFilter(),
                    "ip_address": components.CharFilter(),
                    "ip_version": components.NumberFilter(),
                },
            ),
            "responses": {
                200: statistics.PeerTimeseries(many=True),
            },
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
        },
    },
    "/network-service-configs/<id:str>/cancellation-policy": {
        "GET": {
            "operation": "network_service_config_cancellation_policy_read",
            "description": """
                The cancellation-policy can be queried to answer
                the questions:

                If I cancel my subscription, *when will it be technically
                decommissioned*?
                If I cancel my subscription, *until what date will I be charged*?

                When the query parameter `decommision_at` is not provided
                it will provide the first possible cancellation date
                and charge period if cancelled at above date.

                The granularity of the date field is a day, the start and end
                of which are to be interpreted by the IXP (some may use UTC,
                some may use their local time zone).
            """,
            "responses": {200: cancellation.CancellationPolicy()},
            "problems": problems.PROTECTED_RESOURCE_FILTERED,
            "filters": components.FilterSet({
                "decommission_at": components.CharFilter(label="""
                    By providing a date in the format `YYYY-MM-DD` you can
                    query the policy what would happen if you request a
                    decommissioning on this date.
                """)
            }),
        },
    },
    #
    # Network Feature Configs
    #
    "/network-feature-configs": {
        "description": """
            A `NetworkFeatureConfig` is a configuration for
            using a `NetworkFeature`.

            For certain `NetworkFeature`s, which are marked as required,
            one `NetworkFeatureConfig needs to be created in order to move
            the `NetworkServiceConfig` into `production`.
        """,
        "GET": {
            "description": "Get all network feature configs.",
            "responses": {
                200: config.NetworkFeatureConfig(many=True),
                206: config.NetworkFeatureConfig(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": filters.Collection(
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        config.NETWORK_FEATURE_CONFIG_TYPES),
                    "service_config": components.CharFilter(
                        label="""
                            Filter by the id of the NetworkServiceConfig the
                            feature configuration is related to.

                            **DEPRECATION NOTICE**: Use `network_service_config` instead.
                        """),
                    "network_service_config": components.CharFilter(
                        label="""
                            Filter by the id of the NetworkServiceConfig the
                            feature configuration is related to.
                        """),
                    "network_feature": components.CharFilter(),
                    "role_assignments": components.BulkIdFilter(),
                    "contacts": components.BulkIdFilter(),
                    "external_ref": components.CharFilter(),
                }
            ),
        },
        "POST": {
            "description": """
                Create a configuration for a `NetworkFeature`
                defined in the `NetworkFeature`s collection.
            """,
            "request": config.NetworkFeatureConfigRequest(),
            "responses": {
                201: config.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
    },
    "/network-feature-configs/<id:str>": {
        "GET": {
            "description": "Get a single network feature config.",
            "responses": {
                200: config.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                **DEPRECATION NOTICE**: This operation will be removed in favor
                of using `PATCH` for all updates.

                Update a network feature configuration
            """,
            "request": config.NetworkFeatureConfigUpdate(),
            "responses": {
                202: config.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": "Update a network feature configuration.",
            "request": config.NetworkFeatureConfigPatch(partial=True),
            "request_content_type": "application/merge-patch+json",
            "responses": {
                202: config.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
            Remove a network feature config.

            The network feature config will be marked as
            `decommission_requested`.
            Decommissioning a network feature config will not
            cascade to related services or service configs.
            """,
            "responses": {
                202: config.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
            ],
        },
    },
}
