
import enum

# FIXME: We can not compare v1 and v2 enums.
#        This is annoying.
from ixapi_schema.v1.constants.access import (
    ConnectionMode,
    BGPSessionType,
    RouteServerSessionMode,
    ConnectionMode,
    LACPTimeout,
)

class VLanEthertype(enum.Enum):
    E_0x8100 = "0x8100"
    E_0x88a8 = "0x88a8"
    E_0x9100 = "0x9100"


class VLanType(enum.Enum):
    PORT = "port"
    DOT1Q = "dot1q"
    QINQ = "qinq"


VLAN_CONFIG_TYPE_DOT1Q = "dot1q"
VLAN_CONFIG_TYPE_QINQ = "qinq"
VLAN_CONFIG_TYPE_PORT = "port"

VLAN_CONFIG_TYPES = [
    VLAN_CONFIG_TYPE_DOT1Q,
    VLAN_CONFIG_TYPE_QINQ,
    VLAN_CONFIG_TYPE_PORT,
]

VLAN_CONFIG_ENTITIES = {
    "PortVLanConfig":
        VLAN_CONFIG_TYPE_PORT,
    "QinQVLanConfig":
        VLAN_CONFIG_TYPE_QINQ,
    "Dot1QVLanConfig":
        VLAN_CONFIG_TYPE_DOT1Q,
}

#
# Service Config Types
#
NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_CONFIG_TYPE_CLOUD = "cloud_vc"
NETWORK_SERVICE_CONFIG_TYPE_P2P = "p2p_vc"
NETWORK_SERVICE_CONFIG_TYPE_P2MP = "p2mp_vc"
NETWORK_SERVICE_CONFIG_TYPE_MP2MP = "mp2mp_vc"


NETWORK_SERVICE_CONFIG_TYPES = [
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
]

NETWORK_SERVICE_CONFIG_ENTITIES = {
    "ExchangeLanNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    "P2PNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_P2P,
    "MP2MPNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    "P2MPNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    "CloudNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
}

#
# Feature Config Types
#
NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER = "route_server"

NETWORK_FEATURE_CONFIG_TYPES = [
    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
]

NETWORK_FEATURE_CONFIG_ENTITIES = {
    "RouteServerNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
}

#
# P2MP Network Service Config
#
P2MP_ROLE_ROOT = "root"
P2MP_ROLE_LEAF = "leaf"

class P2MPRole(enum.Enum):
    ROOT = P2MP_ROLE_ROOT
    LEAF = P2MP_ROLE_LEAF


PORT_STATE_UP = "up"
PORT_STATE_DOWN = "down"
PORT_STATE_ERROR = "error"

class PortState(enum.Enum):
    UP = PORT_STATE_UP
    DOWN = PORT_STATE_DOWN
    ERROR = PORT_STATE_ERROR
