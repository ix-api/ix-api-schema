
import enum

class HealthStatus(enum.Enum):
    PASS = "pass"
    FAIL = "fail"
    WARN = "warn"

