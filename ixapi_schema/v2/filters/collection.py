
from ixapi_schema.openapi import components

# Re-Export FilterSet for convenience
FilterSet = components.FilterSet

def Ordered(*args, **kwargs):
    """
    A filter suports querying ordered items by appending
    the suffix `_lt`, `_lte`, `_gt`, `_gte` to the field name.
    """
    for arg in args:
        kwargs.update(arg)

    filters = {}
    for name, field in kwargs.items():
        filters[name] = field
        filters[name + "_lt"] = field
        filters[name + "_lte"] = field
        filters[name + "_gt"] = field
        filters[name + "_gte"] = field

    return filters


COLLECTION = {
    "id": components.BulkIdFilter(),

    # Pagination
    "page_limit": components.NumberFilter(
        label="""
        The maximum number of items in the response.
        [*(Pagination)*](#section/Pagination)
        """,
    ),
    "page_offset": components.NumberFilter(
        label="""
        The offset of the first item in the response.
        [*(Pagination)*](#section/Pagination)
        """,
    ),
    "page_token": components.CharFilter(
        label="""
        The pagination token from the initial response.
        [*(Pagination)*](#section/Pagination)
        """,
    ),
}


def Collection(*args, **kwargs):
    """Create a fillterset for collections"""
    filters = {**COLLECTION}
    for arg in args:
        filters.update(arg)
    filters.update(kwargs)

    return FilterSet(filters)

