
"""
Default Filters for CRM objects
"""

from ixapi_schema.openapi import components


MANAGING_ACCOUNT = components.CharFilter(
    label="Filter by the ID of the managing [account](#tag/accounts).",
)

CONSUMING_ACCOUNT = components.CharFilter(
    label="Filter by the ID of the consuming [account](#tag/accounts).",
)


OWNABLE = {
    "managing_account": MANAGING_ACCOUNT,
    "consuming_account": CONSUMING_ACCOUNT,
    "external_ref": components.CharFilter(),
}


INVOICEABLE = {
    "purchase_order": components.CharFilter(),
    "contract_ref": components.CharFilter(),
}
