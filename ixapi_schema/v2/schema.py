
"""
This file describes the api spec base information.
This will be included in the openapi schema.

This module exports all schema entities and constants
for use in an application and should be considered
as the library entry point.
"""
from copy import deepcopy as dc

from ixapi_schema import v2

# Export entities
from ixapi_schema.v2.entities.auth import *
from ixapi_schema.v2.entities.catalog import *
from ixapi_schema.v2.entities.service import *
from ixapi_schema.v2.entities.config import *
from ixapi_schema.v2.entities.crm import *
from ixapi_schema.v2.entities.events import *
from ixapi_schema.v2.entities.ipam import *
from ixapi_schema.v2.entities.problems import *
from ixapi_schema.v2.entities.introspection import *
from ixapi_schema.v2.entities.cancellation import *
from ixapi_schema.v2.entities.statistics import *

# Export constants
from ixapi_schema.v2.constants.config import *
from ixapi_schema.v2.constants.catalog import *
from ixapi_schema.v2.constants.ipam import *
from ixapi_schema.v2.constants.service import *
from ixapi_schema.v2.constants.introspection import *


DESCRIPTION = """
This API allows to configure/change/delete Internet Exchange services.

# Filters
When querying collections, the provided query parameters
are validated. Unknown query parameters are ignored.
Providing invalid filter values should yield a validation error.

When multiple filters are provided, the result must match all filters.

# Pagination

Pagination is an *optional* feature of the IX-API and can be used on
every collection endpoint. Use the
<a href="#operation/api_implementation_read">`api_implementation_read`</a>
operation to discover if it is available.

Pagination needs to be initiated by the client by providing the
`page_limit` query parameter.

### Query Parameters

Pagination is controlled using the following query parameters:

| Parameter | Description |
| ------ | ------ |
| `page_offset` | The offset of the first item in the collection. **Default:** `0` |
| `page_limit` | The maximum number of items in the response |
| `page_token` | **OPTIONAL**: The token from the initial response.<br /> If invalid a `ValidationError` with HTTP status `400` will be returned |


### Paginated Response

If the collection is paginated, the HTTP status code of the response
will be `206 Partial Content` and pagination metadata is provided
in the response headers:

| Header | Description |
| ------ | ------ |
| `X-Pagination-Limit` | Maximum number of items returned |
| `X-Pagination-Offset` | Current offset of the first item in the collection |
| `X-Pagination-Items` | Total number of items in the collection |
| `X-Pagination-Pages` | Total number of pages |
| `X-Pagination-Page` | Current page |
| `X-Pagination-Token` | An identifier that changes when the collection changes between requests |
| `X-Pagination-Next` | Path to the next page of the collection |

The `X-Pagination-Token` can be used to check if the result set has changed
between requests. If the token has changed, the client may re-query
the collection from the beginning, to ensure that no items are missed.
If you are tracking changes to the collection, the token can be implemented as
a combination of the time of most recent insert or update with the total number
of items.

The `X-Pagination-Next` header contains the URL path to the next page of the
collection, including all other query parameters:
`/collection?filter=foo&param=bar&page_offset=100&page_limit=50&page_token=MTIzMTM5MDEyMDg0NQ==`.
The path is relative the API base path (e.g. `/api/v2`). This can be used
as a cursor to navigate the collection.

# State
A lot of resources are stateful, indicated by the presence of
a `state` property, to support the
inherently asynchronous nature of provisioning, deployment and
on-boarding processes.

The following table describes the meaning of each state:

| State | Meaning |
| ------ | ------ |
| requested | Resource has been requested by the customer but not yet fully reserved (sub-resources required) |
| allocated | All resources required for service are reserved |
| testing | The resource is provisioned and is currently being tested |
| production | The resource is active and can be used by the customer |
| production_change_pending | The resource is active but the customer has requested a change that is awaiting completion |
| decommission_requested | The resource is active but the customer has requested disconnection that is awaiting completion |
| decommissioned | The resource has been de-provisioned and billing is terminated or scheduled for termination |
| archived | The resource was "deleted/purged" and is not listed unless explicitly requested in the filter (i.e. `?state=archived`). |
| error | The resource has experienced error during provisioning or after is has been activated |
| cancelled | The request for a service was cancelled before provisioning was completed |
| operator | Human intervention is needed |
| scheduled | The service has been scheduled for provisioning |

Please note, that not all implementers _HAVE_ to implement
all the listed states.

*Sidenote:* If the deleted operation is applied to an object in
state `decommissioned` the object will move to state archived.

# Sensitive Properties

Some properties contain sensitive information and should be redacted when
the resource is made available users outside the authorized scope.

This is for example the case when an `Account` is flagged as `discoverable`,
it becomes available to other API users. In this case only: `id`, `name`
and `metro_area_network_presence` should be exposed.

If a property is `required` and needs to be redacted, a zero value
should be used. For strings this would be an empty string `""`,
for numeric values `0` and booleans `false`.

Shared resources with sensitive properties: `Account`, `NetworkService`
"""

OAUTH_DESCRIPTION = """
This API uses OAuth 2.
We suggest to use the scope: `ix-api` but
it is up to the implementers to decide the details.
This includes what flows to support.
"""

OAUTH_AUTH_URL = {
    "authorizationUrl": "https://auth.example-ix/see-implementation-guide",
}

OAUTH_TOKEN_URL = {
    "tokenUrl": "https://auth.example-ix/see-implementation-guide",
}

OAUTH_FLOW = {
    "scopes": {
        "ix-api": "access the IX-API",
    },
}

JWT_DESCRIPTION = """
*DEPRECATION NOTICE:*

Authentication using the `/auth/token` endpoint
will be retired in favour of using OAuth 2 with the scope `ix-api`.
"""


SPEC = {
    "info": {
        "version": v2.__version__,
        "title": "IX-API",
        "description": DESCRIPTION,
        "contact": {
            "url": "https://ix-api.net",
        },
        "license": {
            "name": "Apache 2.0",
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
        },
    },
    "servers": [{
        "url": "/api/v2",
    }],
    "components": {
        "securitySchemes": {
            "Bearer": {
                "type": "http",
                "scheme": "bearer",
                "bearerFormat": "JWT",
            },
            "OAuth": {
                "type": "oauth2",
                "description": OAUTH_DESCRIPTION,
                "flows": {
                    "implicit": {
                        **dc(OAUTH_FLOW),
                        **OAUTH_AUTH_URL,
                    },
                    "authorizationCode": {
                        **dc(OAUTH_FLOW),
                        **OAUTH_AUTH_URL,
                        **OAUTH_TOKEN_URL,
                    },
                    "password": {
                        **dc(OAUTH_FLOW),
                        **OAUTH_TOKEN_URL,
                    },
                    "clientCredentials": {
                        **dc(OAUTH_FLOW),
                        **OAUTH_TOKEN_URL,
                    },
                },
            },
        },
    },
    "security": [
        {"OAuth": ["ix-api"]},
        {"Bearer": []},
    ],
    "error_documents_base_url":
        "https://errors.ix-api.net/v2/",
}
