
"""
Introspection Entities

These include API reflection for client runtime configuration
and health checks.
"""


from ixapi_schema.openapi import components
from ixapi_schema.v2.constants.introspection import (
    HealthStatus,
)


# Health Check Resource based on
# https://tools.ietf.org/id/draft-inadarei-api-health-check-04.html
class ApiHealth(
        components.Component,
    ):
    """Health Status Response"""
    status = components.EnumField(
        HealthStatus,
        help_text="""
            status indicates whether the service status is
            acceptable or not.
        """)
    version = components.CharField(
        required=False,
        help_text="""
            Public version of the service.

            Example: "1"
        """)
    releaseId = components.CharField(
        required=False,
        help_text="""
            Release version of the api implementation.

            Example: "1.23.0"
        """)
    notes = components.ListField(
        child=components.CharField(),
        default=[],
        required=False,
        help_text="""
            Array of notes relevant to current state of health.
        """)

    output = components.CharField(
        required=False,
        help_text="""
            Raw error output, in case of "fail" or "warn" states.
        """)

    serviceId = components.CharField(
        required=False,
        help_text="""
            A unique identifier of the service, in the application scope.
        """)
    description = components.CharField(
        required=False,
        help_text="""
            A human-friendly description of the service.
        """)

    checks = components.DictField(
        required=False,
        help_text="""
            The "checks" object MAY have a number of unique keys,
            one for each logical downstream dependency or sub-component.

            Since each sub-component may be backed by several nodes
            with varying health statuses, these keys point to arrays
            of objects. In case of a single-node sub-component
            (or if presence of nodes is not relevant), a single-element
            array SHOULD be used as the value, for consistency.

            Please see
            https://tools.ietf.org/id/draft-inadarei-api-health-check-04.html#the-checks-object
            for details.
        """,
        child = components.DictField(
            child = components.CharField()))

    links = components.DictField(
        required=False,
        help_text="""
            Is an object containing link relations and URIs [RFC3986]
            for external links that MAY contain more information about
            the health of the endpoint.
        """,
        child = components.CharField())


def make_capabilities_field(help_text):
    """Creates a new capabilities field with a help text"""
    return components.ListField(
        child=components.CharField(),
        required=True,
        help_text=help_text)


class ApiFeatures(components.Component):
    """Optional API Features"""
    pagination = components.BooleanField(
        required=False,
        help_text="""
            The API implementation supports pagination on `list`
            operations.
        """)


class ApiImplementation(
        components.Component,
    ):
    """API Implementation"""
    # Version
    schema_version = components.CharField(
        required=True,
        help_text="""
            Version of the implemented IX-API schema.

            Example: "2.0.0"
        """)

    service_version = components.CharField(
        required=False,
        help_text="""
            Version of the API service.

            Example: "1.23.0"
        """)

    # Capabilities
    supported_network_service_types = make_capabilities_field(
        """
        Array of network service types, supported by the IX.

        Example: ["exchange_lan", "p2p_vc", "cloud_vc"]
        """)
    supported_network_service_config_types = make_capabilities_field(
        """
        Array of supported network service config types.

        Example: ["exchange_lan", "p2p_vc", "cloud_vc"]
        """)
    supported_network_feature_types = make_capabilities_field(
        """
        Array of supported network feature types.

        Example: ["route_server"]
        """)
    supported_network_feature_config_types = make_capabilities_field(
        """
        Array of supported network feature config types.

        Example: ["route_server"]
        """)
    supported_operations = make_capabilities_field(
        """
        Array of implemented operations of the ix-api schema.

        Example: ["connections_list", "connections_read", "network_service_configs_create"]
        """)

    supported_features = ApiFeatures()


class ApiExtension(components.Component):
    """
    Implementation specific API extensions
    """
    name = components.CharField(
        required=True,
        help_text="""
            Name of the extension.

            Example: "iNet Services"
        """)

    publisher = components.CharField(
        required=True,
        help_text="""
            Publisher of the extension.

            Example: "EXAMPLE-IX"
        """)

    documentation_url = components.CharField(
        required=True,
        help_text="""
            URL of the documentation homepage of the extension.

            Example: "https://docs.ixp.example.com/ix-api/extensions/inet-services"
        """)

    base_url = components.CharField(
        required=True,
        help_text="""
            Extension endpoints are available under this base url.

            Example: "https://ixapi.ixp.example.com/v2/inet-services/"
        """)

    spec_url = components.CharField(
        required=True,
        help_text="""
            URL of the extensions schema specifications.
            The schema format schould be OpenAPI v3.

            Example: "https://docs.ixp.example.com/ix-api/extensions/inet-services/schema.json"
        """)

