
import enum

from ixapi_schema.openapi import components


class State(enum.Enum):
    """Enum containing all states the statemachine can transition"""
    REQUESTED = "requested"
    ALLOCATED = "allocated"

    TESTING = "testing"
    PRODUCTION = "production"
    PRODUCTION_CHANGE_PENDING = "production_change_pending"

    DECOMISSION_REQUESTED = "decommission_requested"
    DECOMMISSIONED = "decommissioned"

    ARCHIVED = "archived"

    ERROR = "error"
    OPERATOR = "operator"
    SCHEDULED = "scheduled" # I'm not convinced about this.
    CANCELLED = "cancelled"


class Severity:
    """Status Severity"""
    EMERGENCY = 0
    ALERT = 1
    CRITICAL = 2
    ERROR = 3
    WARNING = 4
    NOTICE = 5
    INFORMATIONAL = 6
    DEBUG = 7


class Status(components.Component):
    """Status Message"""
    severity = components.IntegerField(
        min_value=0,
        max_value=7,
        help_text="""
            We are using syslog severity levels: 0 = Emergency,
            1 = Alert, 2 = Critical, 3 = Error, 4 = Warning,
            5 = Notice, 6 = Informational, 7 = Debug.

            Example: 2
        """)
    tag = components.CharField(
        help_text="""
            A machine readable message identifier.

            Example: proxy_arp_detected
        """)
    message = components.CharField(
        help_text="""
            A human readable message, describing the problem
            and may contain hints for resolution.

            Example: "The peer sent an ARP reply for an IP addresses that has not been assigned to them."
        """
    )
    attrs = components.JSONField(
        required=False,
        help_text="""
            Optional machine readable key value pairs
            supplementing the message.

            A custom, detailed or localized error messagen can
            be presented to the user, derived from the `tag` and `attrs`.

            Example: { "mac": "02:42:23:42:a0:bc" }
        """)
    timestamp = components.DateTimeField(
        help_text="""
            The time and date when the event occured.
        """)


class Event(components.Component):
    """Event"""
    serial = components.IntegerField()
    account = components.PrimaryKeyRelatedField(
        related="Account",
        read_only=True)

    type = components.CharField()
    payload = components.JSONField()

    timestamp = components.DateTimeField()


class StatefulBase(components.Component):
    """A stateful object"""
    state = components.EnumField(State, required=True)
    status = Status(many=True, read_only=True, required=False)

