
"""
We adopt the RFC7807 error detail format. Details
are a json object with the content type: application/problem+json.
"""

from typing import Optional
from datetime import date

from ixapi_schema.openapi import (
    components,
    http_status,
)
from ixapi_schema.v2.constants.api import (
    Resources,
)

class ProblemResponse(components.Component):
    """
    Encodes a problem into an appropriate response body.
    """
    type = components.CharField(
        source="ptype",
        help_text="""
            A URI reference (see RFC3986) that identifies the
            problem type.

            This specification encourages that, when
            dereferenced, it provide human-readable documentation
            for the problem type (e.g., using HTML
            [W3C.REC-html5-20141028]).

            When this member is not present, its value is assumed
            to be "about:blank".

            Example: about:blank
        """)
    title = components.CharField(
        required=False,
        help_text="""
            A short, human-readable summary of the problem type.

            It SHOULD NOT change from occurrence to
            occurrence of the problem, except for purposes
            of localization (e.g., using proactive content
            negotiation; see [RFC7231], Section 3.4).

            Example: Some fields have validation errors.
        """)
    status = components.IntegerField(
        required=False,
        min_value=100,
        help_text="""
            The HTTP status code ([RFC7231], Section 6)
            generated by the origin server for this occurrence
            of the problem.
        """)
    detail = components.CharField(
        required=False,
        help_text="""
            A human-readable explanation specific to this
            occurrence of the problem.
        """)

    instance = components.CharField(
        required=False,
        help_text="""
            A URI reference that identifies the specific
            occurrence of the problem.  It may or may not yield
            further information if dereferenced.
        """)

    def to_representation(self, obj):
        """
        Add extra properties to result.
        Add base to type (yes this is lazy.)
        """
        from django.conf import settings
        data = super().to_representation(obj)
        if data["type"] != "about:blank":
            data["type"] = "{}{}.html".format(
                settings.ERROR_DOCS_BASE_URL, data["type"])

        # Assign extra properties
        extra = getattr(obj, "extra", None)
        if not extra:
            extra = {}
        for attr, value in extra.items():
            data[attr] = value

        # And now for the fun part: remove everything none.
        return {k: v for k, v in data.items() if v}


class Problem:
    """
    A problem can have the following properties:

       o  "type" (string) - A URI reference [RFC3986] that identifies the
          problem type.  This specification encourages that, when
          dereferenced, it provide human-readable documentation for the
          problem type (e.g., using HTML [W3C.REC-html5-20141028]).  When
          this member is not present, its value is assumed to be
          "about:blank".

       o  "title" (string) - A short, human-readable summary of the problem
          type.  It SHOULD NOT change from occurrence to occurrence of the
          problem, except for purposes of localization (e.g., using
          proactive content negotiation; see [RFC7231], Section 3.4).

       o  "status" (number) - The HTTP status code ([RFC7231], Section 6)
          generated by the origin server for this occurrence of the problem.

       o  "detail" (string) - A human-readable explanation specific to this
          occurrence of the problem.

       o  "instance" (string) - A URI reference that identifies the specific
          occurrence of the problem.  It may or may not yield further
          information if dereferenced.

    See RFC7807 for details.
    """
    default_type: str = "about:blank"
    default_title: Optional[str] = None
    default_status: Optional[int] = None
    default_response_status: int = http_status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail: Optional[str] = None
    default_instance: Optional[str] = None
    default_extra: dict = {}

    def __init__(
            self,
            type_=None,
            title=None,
            status=None,
            response_status=None,
            detail=None,
            instance=None,
            extra=None,
        ):
        """
        Initialize problem. Note that the resonse_status param
        is used to actually set the http status of the response,
        while status will be part of the information if present.
        """
        # We get the type from our name.
        # E.g. a ValidationErrorProblem would be a "validation-error" type.
        self.ptype = type_ if type_ else self.default_type
        self.title = title if title else self.default_title
        self.status = status if status else self.default_status
        self.detail = detail if detail else self.default_detail
        self.instance = instance if instance else self.default_instance
        self.extra = extra if extra else self.default_extra
        self.response_status = response_status if response_status else \
            self.default_response_status


#
# Generic Fallback Server-side Problem
#
class ServerErrorProblem(Problem):
    """
    An internal problem occurred on the server. This does not imply that
    the request was invalid.

    Please retry your request; if the problem persists please contact
    support.
    """
    default_response_status = http_status.HTTP_500_INTERNAL_SERVER_ERROR
    default_type = "server-error"
    default_title = "Something went wrong."


#
# Server Problems
#
class UnableToFulfillProblem(Problem):
    """
    The server cannot perform the requested task.

    Your request may be valid, but the server is unable to act on it.
    Further information may be available in the `detail` field.
    """
    default_response_status = http_status.HTTP_400_BAD_REQUEST
    default_type = "unable-to-fulfill"
    default_title = "Unable to fulfill your request."


#
# Client Problems
#

class ValidationErrorProperty(components.Serializer):
    """A failed validation"""
    name = components.CharField(required=True)
    reason = components.CharField(required=True)


class ValidationErrorProblem(Problem):
    """
    Some fields in the request could not be validated correctly.

    A list of field validation errors is available in the `properties`
    attribute of this error. An error is represented as an object with
    the properties `name` and `reason`.
    """
    default_response_status = http_status.HTTP_400_BAD_REQUEST
    default_type = "validation-error"
    default_title = "Some fields did not validate."
    default_extra: dict = {
        "properties": [],
    }

    extra_fields = {
        "properties": ValidationErrorProperty(
            required=True,
            many=True,
            help_text="""
                A list of failed validations identified by the
                property `name` giving a `reason` why it failed.
            """),
    }


class Conflict(components.Serializer):
    """A conflict is preventing success"""
    resource_type = components.EnumField(
        Resources,
        required=True,
        help_text="""
            The resource type refers to an ix-api resource.

            Example: "role-assignments"
        """)

    resource_id = components.CharField(
        required=True,
        help_text="""
            The id of the resource which has a conflict with the
            request operation on the current resource.

            Example: "RA:2819238"
        """)

    resource_property = components.CharField(
        allow_null=True,
        help_text="""
            Indicates the property where the resource is in use.

            Example: "contact"
        """)

    remote_resource_type = components.EnumField(
        Resources,
        required=True,
        help_text="""
            The type of the conflicting resource.

            Example: "contacts"
        """)

    remote_resource_id = components.CharField(
        required=True,
        help_text="""
            The id of the conflicting resource. This is in most
            cases the id of the current resource.

            Example: "C:523589"
        """)


class ConstraintViolationProblem(Problem):
    """
    When deleting a resource, it may be the case, that
    it is in still in use.

    In analogy to foreign key constraints preventing deletion
    of associated rows in a relational database, the error
    lists the dependent object which is referring to
    the row or object which is about to be deleted.
    """
    default_response_status = http_status.HTTP_409_CONFLICT
    default_type = "conflict"
    default_title = "There is a conflict preventing success"
    default_extra: dict = {
        "conflict": [],
    }

    extra_fields = {
        "conflict": Conflict(required=True, many=True),
    }


class CancellationPolicyErrorProblem(Problem):
    """
    The requested date for service cancellation can not be
    fulfilled because it is in violation on the cancellation
    policy of the service.

    This is usually when the requested `decommission_at` date
    is outside the notice period.

    The response fields `decommision_at` denotes the earliest
    date possible to cancel the service.

    Your obligation to pay for the service will end on the
    `charged_until` date.

    The earliest cancellation date can be queried through
    the service's `cancellation-policy`.
    """
    default_response_status = http_status.HTTP_400_BAD_REQUEST
    default_type = "cancellation-policy-error"
    default_title = "Requested cancellation date outside of notice period."
    default_extra: dict = {
        "decommission_at": date.min,
        "charged_until": date.min,
    }

    extra_fields = {
        "decommission_at": components.DateField(
            required=True,
            help_text="""
            This field denotes the first possible cancellation
            date of the service.

            See the service `cancellation-policy` for details.
            """),
        "charged_until": components.DateField(
            required=True,
            help_text="""
                The date until the service is payed for.
                Typically `≥ decommission_at`.
            """),
    }


class ParseErrorProblem(Problem):
    """
    The request payload could not be parsed.
    This may mean that the client is sending invalid JSON or that the
    request has been corrupted.

    Please check the syntax of your request, and ensure that your
    `Content-Type` header is `application/json`.
    """
    default_response_status = http_status.HTTP_400_BAD_REQUEST
    default_type = "parse-error"
    default_title = "Malformed request."
    default_detail = "The request could not be parsed without errors."


class AuthenticationProblem(Problem):
    """
    The authentication failed because incorrect authentication
    credentials were provided.

    Please check your credentials and try again.

    If the problem persists, please contact support.
    """
    default_response_status = http_status.HTTP_401_UNAUTHORIZED
    default_title = 'Authentication Credentials Invalid'
    default_type = 'authentication-error'


class SignatureExpiredProblem(AuthenticationProblem):
    """
    The authentication has failed because the signature in your token
    has expired.

    Please refresh your token and try again.
    If your refresh token has expired, you will need to reauthenticate.
    """
    default_title = "Signature Expired Error"
    default_type = "signature-expired"


class NotAuthenticatedProblem(AuthenticationProblem):
    """
    The endpoint you called requires the client to have authenticated
    against the server, but your request did not include the resulting
    authorization.

    Please ensure that your request's `Authorization` header is present
    and includes the access_token returned by the `/auth/token` or
    `/auth/refresh` endpoint.
    """
    default_type = 'not-authenticated'
    default_title = 'Authentication credentials were not provided.'


class PermissionDeniedProblem(Problem):
    """
    You do not have permission to access or modify the resource you
    requested.

    Please ensure that your request is correctly authenticated as a
    account with access rights to this specific entity.
    """
    default_response_status = http_status.HTTP_403_FORBIDDEN
    default_type = "permission-denied"
    default_title = 'You do not have permission to perform this action.'


class NotFoundProblem(Problem):
    """
    You are trying to access a resource which does not exist.

    Either this resource has never existed, or it has been deleted,
    or we cannot reveal its existence to the authenticated user.
    """
    default_response_status = http_status.HTTP_404_NOT_FOUND
    default_type = "not-found"
    default_title = "Not found."


class MethodNotAllowedProblem(Problem):
    """
    The HTTP verb you have used is not supported for this endpoint.

    Currently supported verbs are `GET`, `POST`, `PUT`, `PATCH` and
    `DELETE`. Some verbs may not be supported for all endpoints.
    """
    default_response_status = http_status.HTTP_405_METHOD_NOT_ALLOWED
    default_type = "method-not-allowed"
    default_title = 'Method is not allowed.'


class NotAcceptableProblem(Problem):
    """
    The server cannot produce a representation of the requested resource
    that meets your client's requirements.

    This is dependant on the negotiation headers supplied by your client
    """
    default_response_status = http_status.HTTP_406_NOT_ACCEPTABLE
    default_type = "not-acceptable"
    default_title = "Could not satisfy the request's `Accept` header."


class UnsupportedMediaTypeProblem(Problem):
    """
    The media format your client is using is not supported by
    the server.

    The expected media format for requests is UTF8 JSON.
    """
    default_response_status = http_status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
    default_title = "Unsupported media type in request."
    default_type = "unsupported-media-type"


# Alias: ThrottledProblem
class TooManyRequestsProblem(Problem):
    """
    The server has received too many requests from your client in
    a short space of time. This may indicate an error in your client.

    If this message occurs in normal usage, please contact support
    to discuss a more appropriate threshold.
    """
    default_response_status = http_status.HTTP_429_TOO_MANY_REQUESTS
    default_title = "We received too many requests from your client."
    default_type = "too-many-requests"

#
# Default Problem Sets
#
PROTECTED_COLLECTION = [
    PermissionDeniedProblem(),
    AuthenticationProblem(),
    SignatureExpiredProblem(),
    ValidationErrorProblem(),
]


PROTECTED_RESOURCE = [
    PermissionDeniedProblem(),
    AuthenticationProblem(),
    SignatureExpiredProblem(),
    NotFoundProblem(),
]


PROTECTED_RESOURCE_UPDATE = PROTECTED_RESOURCE + [
    ValidationErrorProblem(),
]

PROTECTED_RESOURCE_FILTERED = PROTECTED_RESOURCE + [
    ValidationErrorProblem(),
]
