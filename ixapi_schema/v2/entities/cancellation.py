"""
Service cancellation related entities:
"""

from ixapi_schema.openapi import components


class CancelableBase(components.Component):
    """Service or config can be canceled"""
    decommission_at = components.DateField(
        required=False,
        allow_null=False,
        help_text="""
            The service will be decommissioned on this date.

            This field is only used when
            the state is `DECOMMISSION_REQUESTED` or
            `DECOMMISSIONED`.
        """)

    charged_until = components.DateField(
        required=False,
        allow_null=False,
        help_text="""
            The service continues incurring charges until this date.
            Typically `≥ decommission_at`.

            This field is only used when
            the state is `DECOMMISSION_REQUESTED` or
            `DECOMMISSIONED`.

            *(Sensitive Property)*
        """)

    current_billing_start_date = components.DateField(
        required=False,
        allow_null=False,
        help_text="""
            Your obligation to pay for the service will start on this date.

            However, this date may change after an upgrade and not reflect
            the inital start date of the service.

            *(Sensitive Property)*
        """)


class CancellationPolicy(components.Component):
    """Cancellation Policy"""
    decommission_at = components.DateField(
        required=True,
        help_text="""
            This field denotes the first possible cancellation
            date of the service.
        """)

    charged_until = components.DateField(
        required=True,
        help_text="""
            Your obligation to pay for the service will end on this date.
            Typically `≥ decommission_at`.
        """)


class CancellationRequest(components.Component):
    """Service Cancellation Request"""
    decommission_at = components.DateField(
        required=False,
        help_text="""
            An optional date for scheduling the cancellation
            and service decommissioning.
        """)

