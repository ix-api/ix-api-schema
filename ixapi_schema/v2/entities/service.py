"""
Service Entities
----------------

"""

from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import crm, events, cancellation
from ixapi_schema.v2.constants.ipam import AddressFamilies
from ixapi_schema.v2.constants.service import (
    NETWORK_FEATURE_TYPE_ROUTESERVER,
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_CLOUD,
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_P2MP,
    NETWORK_SERVICE_TYPE_MP2MP,
    MEMBER_JOINING_RULE_TYPE_ALLOW,
    MEMBER_JOINING_RULE_TYPE_DENY,
)
from ixapi_schema.v2.constants.config import (
    RouteServerSessionMode,
    BGPSessionType,
)


class IXPSpecificFeatureFlag(components.Component):
    """IXP-Specific Feature Flag"""
    name = components.CharField(
        max_length=40,
        help_text="""
            The name of the feature flag.

            Example: RPKI-HARD-FILTER
        """)

    description = components.CharField(
        max_length=80,
        help_text="""
            The description of the feature flag.

            Example: RPKI reject invalid filtering is available
        """)

    mandatory = components.BooleanField(
        help_text="""
            This feature will always be enabled, even if not provided in
            the corresponding config's list of `flags`.

            Example: false
        """)


class NetworkFeatureBase(
        components.Component,
    ):
    """Feature Base"""
    id = components.PrimaryKeyField()

    name = components.CharField(max_length=80)
    required = components.BooleanField()
    network_service = components.PrimaryKeyRelatedField(
        related="NetworkService")
    
    __polymorphic__ = "NetworkFeature"


class RouteServerNetworkFeature(NetworkFeatureBase):
    """Route Server Network Feature"""
    nfc_required_contact_roles = components.PrimaryKeyRelatedField(
        source="all_nfc_required_contact_roles",
        related="Role",
        read_only=True,
        many=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to contacts.

            The role assignments is associated with the network feature
            config through the `role_assignments` list property.
        """)

    flags = IXPSpecificFeatureFlag(
        source="ixp_specific_flags",
        many=True,
        help_text="""
            A list of IXP specific feature flags. This can be used
            to see if e.g. RPKI hard filtering is available.
        """)
    
    asn = components.IntegerField(min_value=0)
    fqdn = components.CharField(
        max_length=80,
        help_text="""
            The FQDN of the route server.

            Example: rs1.moon-ix.net
        """)
    looking_glass_url = components.CharField(
        required=False,
        help_text="""
            The url of the looking glass.

            Example: https://lg.moon-ix.net/rs1
        """)

    address_families = components.ListField(
        child=components.EnumField(AddressFamilies),
        min_length=1,
        max_length=2,
        help_text="""
            When creating a route server feature config, remember
            to specify which address family or families to use:

            If the route server network feature only supports `af_inet`,
            then the `as_set_v4` in the network feature config is required.

            If only `af_inet6` is supported, then the `as_set_v6` is required.

            If both `af_inet` and `af_inet6` are supported, either
            `as_set_v4` or `as_set_v6` is required, but both can be provided
            in the network service config.

            Example: ["af_inet"]
        """)

    session_mode = components.EnumField(
        RouteServerSessionMode,
        help_text="""
            When creating a route server feature config, remember
            to specify the same session_mode as the route server.

            Example: "public"
        """)

    available_bgp_session_types = components.ListField(
        child=components.EnumField(BGPSessionType),
        min_length=1,
        max_length=2,
        help_text="""
            The route server provides the following session modes.

            Example: ["passive"]
        """)

    ip_v4 = components.CharField(
        required=False,
        help_text="""
            IPv4 address in [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
            notation.

            This field is only set if the `address_families` include `af_inet`.

            Example: "23.42.0.1"
        """)
    ip_v6 = components.CharField(
        required=False,
        help_text="""
            IPv6 address in hexadecimal colon separated notation.

            This field is only set if the `address_families` include `af_inet6`.

            Example: "2001:23:42::1"
        """)

    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_ROUTESERVER


class NetworkFeature(components.PolymorphicComponent):
    """Polymorphic Network Feature"""
    serializer_classes = {
        NETWORK_FEATURE_TYPE_ROUTESERVER:
            RouteServerNetworkFeature,
    }

    entity_types = {
        "RouteserverNetworkFeature":
            NETWORK_FEATURE_TYPE_ROUTESERVER,
    }


class MemberJoiningRuleBase(
        crm.ExternalReferenceBase,
        crm.ManageableBase,
        components.Component,
    ):
    """A rule for members joining a private vlan"""
    consuming_account = components.PrimaryKeyRelatedField(
        related="Account",
        help_text="""
            The `id` of the account to which access to the
            network service should be granted or denied.

            Example: "2381982"
        """)

    __polymorphic__ = "MemberJoiningRule"


class AllowMemberJoiningRuleBase(MemberJoiningRuleBase):
    """ELan Membership"""
    id = components.PrimaryKeyField()
    capacity_min = components.IntegerField(
        min_value=1,
        allow_null=True,
        required=False,
        help_text="""
            Require an optional minimum capacity to join
            the network service.
        """)
    capacity_max = components.IntegerField(
        min_value=1,
        allow_null=True,
        required=False,
        help_text="""
            An optional rate limit which has precedence over
            the capacity set in the network service config.
        """)

    __polymorphic_type__ = \
        MEMBER_JOINING_RULE_TYPE_ALLOW


class AllowMemberJoiningRule(AllowMemberJoiningRuleBase):
    """A rule for members joining a private vlan"""
    network_service = components.PrimaryKeyRelatedField(
        related="NetworkService")


class AllowMemberJoiningRuleRequest(AllowMemberJoiningRuleBase):
    """A new vlan member joining rule"""
    network_service = components.PrimaryKeyRelatedField(
        related="NetworkService")

    __polymorphic__ = "MemberJoiningRuleRequest"


class AllowMemberJoiningRuleUpdate(AllowMemberJoiningRuleBase):
    """A vlan member joining rule update"""
    # We do not allow changing the network service
    # of a member joining rule.
    __polymorphic__ = "MemberJoiningRuleUpdate"


class AllowMemberJoiningRulePatch(AllowMemberJoiningRuleUpdate):
    """A vlan member joining rule update"""
    __polymorphic__ = "MemberJoiningRulePatch"


class DenyMemberJoiningRuleBase(MemberJoiningRuleBase):
    """Deny Member Joining Rule"""
    __polymorphic_type__ = \
        MEMBER_JOINING_RULE_TYPE_DENY


class DenyMemberJoiningRule(DenyMemberJoiningRuleBase):
    """A rule for members joining a private vlan"""
    network_service = components.PrimaryKeyRelatedField(
        related="NetworkService")


class DenyMemberJoiningRuleRequest(DenyMemberJoiningRuleBase):
    """A new vlan member joining rule"""
    network_service = components.PrimaryKeyRelatedField(
        related="NetworkService")

    __polymorphic__ = "MemberJoiningRuleRequest"


class DenyMemberJoiningRuleUpdate(DenyMemberJoiningRuleBase):
    """A vlan member joining rule update"""
    # We do not allow changing the network service
    # of a member joining rule.
    __polymorphic__ = "MemberJoiningRuleUpdate"


class DenyMemberJoiningRulePatch(DenyMemberJoiningRuleUpdate):
    """A vlan member joining rule update"""
    __polymorphic__ = "MemberJoiningRulePatch"


class MemberJoiningRule(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_ALLOW:
            AllowMemberJoiningRule,
        MEMBER_JOINING_RULE_TYPE_DENY:
            DenyMemberJoiningRule,
    }

    entity_types = {
        "AllowMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_ALLOW,
        "DenyMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_DENY,
    }


class MemberJoiningRuleRequest(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule Request"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_ALLOW:
            AllowMemberJoiningRuleRequest,
        MEMBER_JOINING_RULE_TYPE_DENY:
            DenyMemberJoiningRuleRequest,
    }

    entity_types = {
        "AllowMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_ALLOW,
        "DenyMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_DENY,
    }


class MemberJoiningRuleUpdate(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule Update"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_ALLOW:
            AllowMemberJoiningRuleUpdate,
        MEMBER_JOINING_RULE_TYPE_DENY:
            DenyMemberJoiningRuleUpdate,
    }

    entity_types = {
        "AllowMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_ALLOW,
        "DenyMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_DENY,
    }


class MemberJoiningRulePatch(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule Update"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_ALLOW:
            AllowMemberJoiningRulePatch,
        MEMBER_JOINING_RULE_TYPE_DENY:
            DenyMemberJoiningRulePatch,
    }

    entity_types = {
        "AllowMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_ALLOW,
        "DenyMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_DENY,
    }

#
# Network Services
#
class NetworkServiceBase(components.Component):
    """Network Service (Base)"""
    id = components.PrimaryKeyField()
    __polymorphic__ = "NetworkService"


class NetworkServiceRequestBase(components.Component):
    """NetworkService request base"""
    __polymorphic__ = "NetworkServiceRequest"


class NetworkServiceUpdateBase(components.Component):
    """NetworkService update base"""
    __polymorphic__ = "NetworkServiceUpdate"


class NetworkServiceCreatableBase(components.Component):
    """Createable Network Service"""
    product_offering = components.PrimaryKeyRelatedField(
        related="ProductOffering")


class NetworkServiceResponseBase(
        events.StatefulBase,
        components.Component,
    ):
    """A Network Service"""
    nsc_required_contact_roles = components.PrimaryKeyRelatedField(
        related="Role",
        source="all_nsc_required_contact_roles",
        many=True,
        read_only=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to contacts.

            The `RoleAssignment` is associated through the
            `role_assignments` list property of the network service configuration.
        """)


class ExchangeLanNetworkServiceBase(
        crm.OwnableBase,
        NetworkServiceBase,
    ):
    """Exchange Lan Network Service"""
    name = components.CharField(
        max_length=40,
        help_text="""
            Exchange-dependent service name, will be shown on the invoice.
        """)

    metro_area_network = components.PrimaryKeyRelatedField(
        related="MetroAreaNetwork",
        help_text="""
            Id of the `MetroAreaNetwork` where
            the exchange lan network service is directly provided.

            Same as `service_metro_area_network` on the related
            `ProductOffering`.

            Example: "man:293225:LON2"
        """)
    peeringdb_ixid = components.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            PeeringDB ixid
        """)
    ixfdb_ixid = components.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            id of ixfdb
        """)

    network_features = components.PrimaryKeyRelatedField(
        related="NetworkFeature",
        many=True)

    subnet_v4 = components.CharField(
        help_text="""
            IPv4 subnet in [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
            CIDR notation.

            Example: "23.142.52.0/21"
        """)
    subnet_v6 = components.CharField(
        help_text="""
            IPv6 subnet in hexadecimal colon separated CIDR notation.

            Example: "2001:23:42::/48"
        """)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkService(
        NetworkServiceResponseBase,
        ExchangeLanNetworkServiceBase,
    ):
    """Exchange Lan Network Service"""
    product_offering = components.PrimaryKeyRelatedField(
        help_text="""*deprecation notice*""",
        required=False,
        related="ProductOffering")


class EVPNetworkServiceBase(components.Component):
    """Ethernet Virtual Private Network Services"""
    nsc_product_offerings = components.PrimaryKeyRelatedField(
        related="ProductOffering",
        many=True,
        required=False,
        help_text="""
            An optional list of `ProductOffering` which can be used in the
            network service configs for this service.
        """)


class P2PNetworkServiceBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        NetworkServiceBase,
    ):
    """P2P Network Service"""
    display_name = components.CharField(
        required=False,
        help_text="""
            Name of the point to point virtual circuit.

            It is visible to all parties allowed to connect
            to this virtual circuit.

            It is intended for humans to make sense of.

            Example: "E-Line Customer"
        """)

    joining_member_account = components.PrimaryKeyRelatedField(
        related="Account",
        help_text="""
            The account of the B-side member joining the virtual circuit.

            Example: "231829"
        """)

    availability_zones = components.PrimaryKeyRelatedField(
        related="AvailabilityZone",
        many=True,
        required=False,
        help_text="""
            The availability zones for the service.
        """)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_P2P


class P2PNetworkService(
        NetworkServiceResponseBase,
        NetworkServiceCreatableBase,
        EVPNetworkServiceBase,
        cancellation.CancelableBase,
        P2PNetworkServiceBase,
    ):
    """P2P Network Service"""
    capacity = components.IntegerField(
        required=False,
        default=None,
        min_value=1,
        allow_null=True,
        help_text="""
             The capacity of the service in Mbps. When null,
             the maximum capacity will be used.
         """)


class P2PNetworkServiceRequest(
        NetworkServiceCreatableBase,
        NetworkServiceRequestBase,
        P2PNetworkServiceBase,
    ):
    """P2P Network Service Request"""


class P2PNetworkServiceUpdate(
        NetworkServiceCreatableBase,
        NetworkServiceUpdateBase,
        P2PNetworkServiceBase,
    ):
    """P2P Network Service Update"""


class P2PNetworkServicePatch(P2PNetworkServiceUpdate):
    """P2P Network Service Update"""
    __polymorphic__ = "NetworkServicePatch"


class P2MPNetworkServiceBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        NetworkServiceBase,
    ):
    """P2MP Network Service"""
    display_name = components.CharField(
        required=False,
        help_text="""
            Name of the point to multi-point virtual circuit.

            It is visible to all parties allowed to connect
            to this virtual circuit.

            It is intended for humans to make sense of.

            Example: "E-Tree Customer"
        """)

    public = components.BooleanField(
        default=False,
        help_text="""
            A public p2mp network service can be joined
            by everyone on the exchange unless denied by
            a member-joining-rule.

            Public network services are visible to other
            members of the IXP, however only `name`, `type`,
            `product_offering`, `consuming_account` and
            `managing_account` are made
            available.

            Other required fields are redacted.
        """)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_P2MP


class P2MPNetworkService(
        NetworkServiceResponseBase,
        NetworkServiceCreatableBase,
        EVPNetworkServiceBase,
        cancellation.CancelableBase,
        P2MPNetworkServiceBase,
    ):
    """P2MP Network Service"""
    network_features = components.PrimaryKeyRelatedField(
        related="NetworkFeature",
        many=True)

    member_joining_rules = components.PrimaryKeyRelatedField(
        related="MemberJoiningRule",
        many=True)


class P2MPNetworkServiceRequest(
        NetworkServiceCreatableBase,
        NetworkServiceRequestBase,
        P2MPNetworkServiceBase,
    ):
    """P2MP Network Service Request"""


class P2MPNetworkServiceUpdate(
        NetworkServiceCreatableBase,
        NetworkServiceUpdateBase,
        P2MPNetworkServiceBase,
    ):
    """P2MP Network Service Update"""


class P2MPNetworkServicePatch(P2MPNetworkServiceUpdate):
    """P2MP Network Service Update"""
    __polymorphic__ = "NetworkServicePatch"


class MP2MPNetworkServiceBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        NetworkServiceBase,
    ):
    """MP2MP Network Service"""
    public = components.BooleanField(
        default=False,
        help_text="""
            A public mp2mp network service can be joined
            by everyone on the exchange unless denied by
            a member-joining-rule.

            Public network services are visible to other
            members of the IXP, however only `display_name`, `type`,
            `product_offering`, `consuming_account` and
            `managing_account` are made available.

            Other required fields are redacted.
        """)

    display_name = components.CharField(
        required=False,
        help_text="""
            Name of the multi-point to multi-point virtual circuit.

            It is visible to all parties allowed to connect
            to this virtual circuit.

            It is intended for humans to make sense of, for example:
            "Financial Clearance LAN".

            Example: "Closed User Group Finance"
        """)

    subnet_v4 = components.CharField(
        required=False,
        help_text="""
            IPv4 subnet in [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
            CIDR notation.

            Example: "23.142.52.0/21"
        """)

    subnet_v6 = components.CharField(
        required=False,
        help_text="""
            IPv6 subnet in hexadecimal colon separated CIDR notation.

            Example: "2001:23:42::/48"
        """)


    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_MP2MP


class MP2MPNetworkService(
        NetworkServiceResponseBase,
        NetworkServiceCreatableBase,
        EVPNetworkServiceBase,
        cancellation.CancelableBase,
        MP2MPNetworkServiceBase,
    ):
    """MP2MP Network Service"""
    member_joining_rules = components.PrimaryKeyRelatedField(
        related="MemberJoiningRule",
        many=True)

    network_features = components.PrimaryKeyRelatedField(
        related="NetworkFeature",
        many=True)

    mac_acl_protection = components.BooleanField(
        required=False,
        help_text="""
            When enabled, only MAC addresses in the referenced in the network
            service config's `macs` property are allowed to send and receive
            traffic on this network service.
        """)


class MP2MPNetworkServiceRequest(
        NetworkServiceCreatableBase,
        NetworkServiceRequestBase,
        MP2MPNetworkServiceBase,
    ):
    """MP2MP Network Service Request"""


class MP2MPNetworkServiceUpdate(
        NetworkServiceCreatableBase,
        NetworkServiceUpdateBase,
        MP2MPNetworkServiceBase,
    ):
    """MP2MP Network Service Update"""


class MP2MPNetworkServicePatch(MP2MPNetworkServiceUpdate):
    """MP2MP Network Service Update"""
    __polymorphic__ = "NetworkServicePatch"


class CloudNetworkServiceBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        NetworkServiceBase,
    ):
    """Cloud Network Service"""
    capacity = components.IntegerField(
        required=False,
        default=None,
        min_value=1,
        allow_null=True,
        help_text="""
             The capacity of the service in Mbps. When null,
             the maximum capacity will be used.
         """)

    __polymorphic_type__ = NETWORK_SERVICE_TYPE_CLOUD


class CloudNetworkService(
        NetworkServiceResponseBase,
        NetworkServiceCreatableBase,
        cancellation.CancelableBase,
        CloudNetworkServiceBase,
    ):
    """Cloud Network Service"""
    diversity = components.IntegerField(
        min_value=1,
        allow_null=False,
        help_text="""
            Same value as the corresponding `ProductOffering`.

            The service can be delivered over multiple handovers from
            the exchange to the `service_provider`.

            The `diversity` denotes the number of handovers between the
            exchange and the service provider. A value of two signals a
            redundant service.

            Only one network service configuration for each `handover` and
            `cloud_vlan` can be created.
        """)

    provider_ref = components.CharField(
        required=True,
        allow_null=True,
        help_text="""
            For a cloud network service with the exchange first
            workflow, the `provider_ref` will be a reference
            to a resource of the cloud provider. (E.g. the UUID of
            a virtual circuit.)

            The `provider_ref` is managed by the exchange and its
            meaning may vary between different cloud services.

            Example: "331050d5-76fb-498b-b72a-248520278fbd"
        """)

    cloud_key = components.CharField(
        allow_null=False,
        help_text="""
            The cloud key is used to specify to which user or
            existing circuit of a cloud provider this `network-service`
            should be provisioned.

            For example, for a provider like *AWS*, this would be the
            *account number* (Example: `123456789876`), or for a provider
            like Azure, this would be the service key
            (Example: `acl9edcf-f11c-4681-9c7b-6d16b2973997`)
        """
    )

    availability_zones = components.PrimaryKeyRelatedField(
        related="AvailabilityZone",
        many=True,
        required=False,
        help_text="""
            The availability zones the service can support.
        """)

    network_features = components.PrimaryKeyRelatedField(
        related="NetworkFeature",
        many=True,
        required=False)

    nsc_product_offerings = components.PrimaryKeyRelatedField(
        related="ProductOffering",
        many=True,
        required=False,
        help_text="""
            An optional list of `ProductOffering` which can be used in the
            network service configs for this service.
        """)


class CloudNetworkServiceRequest(
        NetworkServiceCreatableBase,
        NetworkServiceRequestBase,
        CloudNetworkServiceBase,
    ):
    """Cloud Network Service Request"""
    cloud_key = components.CharField(
        allow_null=False,
        help_text="""
            The cloud key is used to specify to which user or
            existing circuit of a cloud provider this `network-service`
            should be provisioned.

            For example, for a provider like *AWS*, this would be the
            *account number* (Example: `123456789876`), or for a provider
            like Azure, this would be the service key
            (Example: `acl9edcf-f11c-4681-9c7b-6d16b2973997`)
        """
    )


class CloudNetworkServiceUpdate(
        NetworkServiceCreatableBase,
        NetworkServiceUpdateBase,
        CloudNetworkServiceBase,
    ):
    """Cloud Network Service Update"""
    cloud_key = components.CharField(
        allow_null=False,
        help_text="""
            The cloud key is used to specify to which user or
            existing circuit of a cloud provider this `network-service`
            should be provisioned.

            For example, for a provider like *AWS*, this would be the
            *account number* (Example: `123456789876`), or for a provider
            like Azure, this would be the service key
            (Example: `acl9edcf-f11c-4681-9c7b-6d16b2973997`)

            **Please note: *Any update to this field may be rejected if the
            service has successfully been provisioned*.**
        """
    )


class CloudNetworkServicePatch(CloudNetworkServiceUpdate):
    """Cloud Network Service Update"""
    __polymorphic__ = "NetworkServicePatch"
    cloud_key = components.CharField(
        allow_null=False,
        help_text="""
            The cloud key is used to specify to which user or
            existing circuit of a cloud provider this `network-service`
            should be provisioned.

            For example, for a provider like *AWS*, this would be the
            *account number* (Example: `123456789876`), or for a provider
            like Azure, this would be the service key
            (Example: `acl9edcf-f11c-4681-9c7b-6d16b2973997`)

            **Please note: *Any update to this field may be rejected if the
            service has successfully been provisioned*.**
        """
    )


class NetworkService(components.PolymorphicComponent):
    """Polymorphic Network Services"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkService,
        NETWORK_SERVICE_TYPE_P2P:
            P2PNetworkService,
        NETWORK_SERVICE_TYPE_P2MP:
            P2MPNetworkService,
        NETWORK_SERVICE_TYPE_MP2MP:
            MP2MPNetworkService,
        NETWORK_SERVICE_TYPE_CLOUD:
            CloudNetworkService,
    }

    entity_types = {
        "ExchangeLanNetworkService":
            NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
        "CloudNetworkService":
            NETWORK_SERVICE_TYPE_CLOUD,
        "P2PNetworkService":
            NETWORK_SERVICE_TYPE_P2P,
        "MP2MPNetworkService":
            NETWORK_SERVICE_TYPE_MP2MP,
        "P2MPNetworkService":
            NETWORK_SERVICE_TYPE_P2MP,
    }


class NetworkServiceRequest(components.PolymorphicComponent):
    """Polymorphic Network Service Request"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_P2P:
            P2PNetworkServiceRequest,
        NETWORK_SERVICE_TYPE_P2MP:
            P2MPNetworkServiceRequest,
        NETWORK_SERVICE_TYPE_MP2MP:
            MP2MPNetworkServiceRequest,
        NETWORK_SERVICE_TYPE_CLOUD:
            CloudNetworkServiceRequest,
    }

    entity_types = {
        "P2PNetworkService":
            NETWORK_SERVICE_TYPE_P2P,
        "MP2MPNetworkService":
            NETWORK_SERVICE_TYPE_MP2MP,
        "P2MPNetworkService":
            NETWORK_SERVICE_TYPE_P2MP,
        "CloudNetworkService":
            NETWORK_SERVICE_TYPE_CLOUD
    }


class NetworkServiceUpdate(components.PolymorphicComponent):
    """Polymorphic Network Service Update"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_P2P:
            P2PNetworkServiceUpdate,
        NETWORK_SERVICE_TYPE_P2MP:
            P2MPNetworkServiceUpdate,
        NETWORK_SERVICE_TYPE_MP2MP:
            MP2MPNetworkServiceUpdate,
        NETWORK_SERVICE_TYPE_CLOUD:
            CloudNetworkServiceUpdate,
    }

    entity_types = {
        "P2PNetworkService":
            NETWORK_SERVICE_TYPE_P2P,
        "MP2MPNetworkService":
            NETWORK_SERVICE_TYPE_MP2MP,
        "P2MPNetworkService":
            NETWORK_SERVICE_TYPE_P2MP,
        "CloudNetworkService":
            NETWORK_SERVICE_TYPE_CLOUD
    }


class NetworkServicePatch(components.PolymorphicComponent):
    """Polymorphic Network Service Patch"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_P2P:
            P2PNetworkServicePatch,
        NETWORK_SERVICE_TYPE_P2MP:
            P2MPNetworkServicePatch,
        NETWORK_SERVICE_TYPE_MP2MP:
            MP2MPNetworkServicePatch,
        NETWORK_SERVICE_TYPE_CLOUD:
            CloudNetworkServicePatch,
    }

    entity_types = {
        "P2PNetworkService":
            NETWORK_SERVICE_TYPE_P2P,
        "MP2MPNetworkService":
            NETWORK_SERVICE_TYPE_MP2MP,
        "P2MPNetworkService":
            NETWORK_SERVICE_TYPE_P2MP,
        "CloudNetworkService":
            NETWORK_SERVICE_TYPE_CLOUD
    }


class NetworkServiceDeleteResponse(components.PolymorphicComponent):
    """Polymorphic Network Service Request"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_P2P:
            P2PNetworkService,
        NETWORK_SERVICE_TYPE_P2MP:
            P2MPNetworkService,
        NETWORK_SERVICE_TYPE_MP2MP:
            MP2MPNetworkService,
        NETWORK_SERVICE_TYPE_CLOUD:
            CloudNetworkService,
    }

    entity_types = {
        "P2PNetworkService":
            NETWORK_SERVICE_TYPE_P2P,
        "MP2MPNetworkService":
            NETWORK_SERVICE_TYPE_MP2MP,
        "P2MPNetworkService":
            NETWORK_SERVICE_TYPE_P2MP,
        "CloudNetworkService":
            NETWORK_SERVICE_TYPE_CLOUD
    }


class NetworkServiceChangeRequest(components.Component):
    """NetworkServiceChangeRequest"""
    product_offering = components.PrimaryKeyRelatedField(
        related="ProductOffering",
        help_text="""
            Migrate to a diffrent product offering. Please note, that
            the offering only may differ in bandwidth.
        """)

    capacity = components.IntegerField(
        required=False,
        default=None,
        min_value=1,
        allow_null=True,
        help_text="""
             The desired capacity of the service in Mbps.

             Must be within the range of `bandwidth_min` and
             `bandwidth_max` of the `ProductOffering`.

             When `null` the maximum capacity wil be used.
         """)

class RoutingFunctionBase(
        
        crm.OwnableBase,
        crm.InvoiceableBase,
        components.Component,
    ):
    """Routing Function"""
    id = components.PrimaryKeyField()
    product_offering = components.PrimaryKeyRelatedField(
        related="ProductOffering",
        help_text="""
            The product offering to be used for the
            routing function.
        """)

    asn = components.IntegerField(
        required=True,
        help_text="""
            Any routing function instance needs to be
            assigned a 2-byte or 4-byte ASN of the
            customer's choice. There is no restriction on
            private or public ASNs.
        """)

    capacity = components.IntegerField(
        required=False,
        allow_null=True,
        help_text="""
            The desired upper bound of the capacity for
            the routing function.
        """)

class RoutingFunctionRequest(
        RoutingFunctionBase,
    ):
    """Routing Function Request"""


class RoutingFunctionPatch(
        RoutingFunctionBase,
    ):
    """Routing Function Patch"""


class RoutingFunction(
        events.StatefulBase,
        RoutingFunctionBase,
    ):
    """Routing Function"""
