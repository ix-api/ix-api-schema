from ixapi_schema.openapi import components
from ixapi_schema.v2.entities.ipam import Peer


DESCRIPTION_STATS_AGGREGATED = """
    A `start` and `end` query parameter can be used to
    retrieve the aggregated traffic for a given window.
    In this case the key of the returned statistics is `custom`.

    With a given `start` and `end` window, the resolution for
    the aggregated data is chosen by the implementation.

    You need to check the `accuracy` attribute of the aggregate,
    to see if the data can be used for the desired
    usecase. The `accuracy` is the ratio of *total samples* to
    *expected samples*.

    If no `start` or `end` parameter is given, a sliding window
    is assumed and key value pairs of resolutions and aggregated
    statistics are returned.
"""

RTT_EVENTS_EXAMPLE_SHORT = """
data: {"serial": 421233, "asn": 42000001, "ip": "fd42:1000::1", ...}
id: 421233

data: {"serial": 421234, "asn": 42000002, "ip": "fd42:1000::1", ...}
id: 421234
"""

RTT_EVENTS_SSE_BODY = """
A stream of Server Sent Events is returned.
The measurement result is encoded as JSON.

### Example:

```
data: {"serial": 42834781723, "timestamp": "2019-08-24T14:15:22Z", "asn": 42000001, ...}
id: 42834781723

data: {"serial": 42834781724, "timestamp": "2019-08-24T14:16:22Z", "asn": 42000002, ...}
id: 42834781724

...
```

For more information on how to use server sent events, see:
https://html.spec.whatwg.org/multipage/server-sent-events.html#server-sent-events

"""


class PeerRTT(components.Component):
    """Peer RTT Statistics"""

    time_ms = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            The total duration of the measurement in
            milliseconds.

            Example: 16000
        """)

    tx = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            The number of probe packets *transmitted*
            within the duration of the measurement.
            
            Example: 10
        """)

    rx = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            The number of probe packets *received*
            within the duration of the measurement.

            Example: 9
        """)

    loss = components.FloatField(
        required=True,
        min_value=0.0,
        max_value=1.0,
        help_text="""
            Ratio of *transmitted packets* to *received packets*:
            `loss = 1.0 - (rx / tx)`.

            Example: 0.1
        """)

    rtt_min_ms = components.FloatField(
        required=True,
        min_value=0.0,
        help_text="""
            The minimum RTT in milliseconds.

            Example: 0.35
        """)

    rtt_avg_ms = components.FloatField(
        required=True,
        min_value=0.0,
        help_text="""
            The average RTT in milliseconds.

            Example: 0.495
        """)

    rtt_max_ms = components.FloatField(
        required=True,
        min_value=0.0,
        help_text="""
            The maximum RTT in milliseconds.

            Example: 0.522
        """)

    rtt_mdev_ms = components.FloatField(
        required=True,
        min_value=0.0,
        help_text="""
            The median RTT in milliseconds.
            Standard deviation in milliseconds.

            Example: 0.032
        """)


    neighbor = components.CharField(
        required=True,
        help_text="""
            The name of the peer.

            Example: "FastPackets LLC"
        """)

    asn = components.IntegerField(
        required=False,
        help_text="""
            The Autonomous System Number (ASN) of the peer.

            Example: 65535
        """)

    ip = components.CharField(
        required=True,
        help_text="""
            The IP address of the peer.
            For IPv6 addresses the canonical form is used.

            Example: "10.2.3.42"
        """)

    timestamp = components.DateTimeField(
        required=True,
        help_text="""
            The date and time when the RTT statistic was measured.
        """)

    serial = components.IntegerField(
        required=True,
        help_text="""
            The `serial` is an incrementing counter. You can use it
            to poll for changes.

            Example: 1730823490
        """)


class AggregateStatistics(components.Component):
    """Statistics"""
    title = components.CharField(
        required=True,
        help_text="""
            Title of the aggregated statistics.

            Example: "30 Days"
        """)

    start = components.DateTimeField(
        required=True,
        help_text="""
            Start of the traffic aggregation.
        """)

    end = components.DateTimeField(
        required=True,
        help_text="""
            End of the traffic aggregation.
        """)

    accuracy = components.FloatField(
        required=True,
        min_value=0.0,
        max_value=1.0,
        help_text="""
            The accuracy is the ratio of *total aggregated samples* to
            *expected samples*.

            The expected number of samples is the size of the window
            of the aggregate, divided by the aggregation resolution.

            For example: A window of `24 h` with an aggregation resolution
            of `5 m` would yield `288` samples.

            If only `275` samples are available for aggregation, the
            accuracy would be `0.95`.

            Example: 0.95
        """)

    created_at = components.DateTimeField(
        required=True,
        help_text="""
            Timestamp when the statistics were created.
        """)

    next_update_at = components.DateTimeField(
        required=True,
        help_text="""
            Next update of the statistical data.
            This may not correspond to the aggregate interval.
        """)

    # Average
    average_pps_in = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average number of inbound **packets per second**.

            Example: 1730224
        """)

    average_pps_out = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average number outbound **packets per second**.

            Example: 17456
        """)

    average_ops_in = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average inbound **octets per second**.

            Example: 1734882240
        """)

    average_ops_out = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average outbound **octets per second**.

            Example: 173220
        """)

    average_eps_in = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Average **errors per second** inbound.

            Example: 0.32
        """)

    average_eps_out = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Averages **errors per second** outbound.

            Example: 0.01
        """)

    average_dps_in = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Average **discards per second** inbound.

            Example: 0.01
        """)

    average_dps_out = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Averages **discards per second** outbound.

            Example: 0.12
        """)

    # 95th percentile
    percentile95_pps_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of inbound **packets per second**.

            Example: 1730224
        """)

    percentile95_pps_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of outbound **packets per second**.

            Example: 17456
        """)

    percentile95_pps_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of the inbound **octets per second**.

            Example: 1734882240
        """)

    percentile95_ops_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of outbound **octets per second**.

            Example: 173220
        """)

    # Peak
    maximum_pps_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak inbound **packets per second** during the interval.

            Example: 1730224
        """)

    maximum_pps_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak outbound **packets per second** during the interval.

            Example: 17456
        """)

    maximum_ops_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak inbound **octets per second** during the interval.

            Example: 5734882240
        """)

    maximum_ops_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak outbound **octets per second** during the interval.

            Example: 473220
        """)

    maximum_in_at = components.DateTimeField(
        required=False,
        help_text="""
            Timestamp when the inbound peak occured.
        """)

    maximum_out_at = components.DateTimeField(
        required=False,
        help_text="""
            Timestamp when the outbound peak occured.
        """)


class Aggregate(
        components.Component,
    ):
    """
    Mapping of Aggregated Statistics
    """
    aggregates = components.DictField(
        child=AggregateStatistics(),
        required=True,
        help_text="""
            Aggregated statistics for a connection or service configuration.

            For the **property name** the string representation of the
            aggregate interval in ISO8601 period notation is recommended.

            For example: `PT5M`, `P1D`, `P30D`,`P1Y`.

            If a window is defined via the `start` and `end` query parameter,
            the **property name** will be `custom`.

            The available intervals can differ by implementation.

            Example: ###AGGREGATE_EXAMPLE###
        """)

    __aggregates_example__ = {
        "PT5M": {
            "title": "5 Minutes",
            "accuracy": 1.0,
            "created_at": "2019-08-24T14:15:22Z",
            "next_update_at": "2019-08-24T14:15:22Z",
            "average_pps_in": 1730224,
            "average_pps_out": 17456,
            "average_ops_in": 1734882240,
            "average_ops_out": 173220,
        },
        "P30D": {
            "title": "30 Days",
            "accuracy": 0.95,
            "created_at": "2019-08-24T14:15:22Z",
            "next_update_at": "2019-08-24T14:15:22Z",
            "average_pps_in": 1730224,
            "average_pps_out": 17456,
            "average_ops_in": 1734882240,
            "average_ops_out": 173220,
        },
    }


class NetworkServiceConfigAggregateStatistics(AggregateStatistics):
    """AggregateStatistics for NetworkServiceConfig"""
    nsc_available_capacity = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            The capacity left on the `NetworkServiceConfig` in
            **megabits per second** (Mbps).

            Example: 23000
        """)

    nsc_available_capacity_change_perc = components.FloatField(
        required=False,
        help_text="""
            The percentage change of the available capacity since
            the last update.

            Example: -13.2
        """)


class NetworkServiceConfigAggregate(Aggregate, components.Component):
    """Statistics for NetworkServiceConfig"""
    aggregates = components.DictField(
        child=NetworkServiceConfigAggregateStatistics(),
        required=True,
        help_text="""
            Aggregated statistics for a connection or service configuration.

            For the **property name** the string representation of the
            aggregate interval in ISO8601 period notation is recommended.

            For example: `PT5M`, `P1D`, `P30D`,`P1Y`.

            If a window is defined via the `start` and `end` query parameter,
            the **property name** will be `custom`.

            The available intervals can differ by implementation.

            Example: ###AGGREGATE_EXAMPLE###
        """)


class PortStatistics(Aggregate, components.Component):
    """Port Statistics"""
    light_levels_tx = components.ListField(
        child=components.FloatField(),
        help_text="""
            A list of light levels in **dBm** for each channel.

            Example: [-5.92, -5.29]
        """)
    light_levels_rx = components.ListField(
        child=components.FloatField(),
        help_text="""
            A list of light levels in **dBm** for each channel.

            Example: [-2.92, -3.89]
        """)


class AggregateTimeseries(
        components.Component,
    ):
    """
    Aggregated Statistics Timeseries
    """
    title = components.CharField(
        required=True,
        help_text="""
            Title of the timeseries.

            Example: "5 Minutes"
        """)

    precision = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Precision indicates the sampling rate of the aggregated traffic data
            in seconds.
            For example if the data is aggregated over 5 minutes, the precision
            would be 300.

            Example: 300
        """)

    created_at = components.DateTimeField(
        required=True,
        help_text="""
            Timestamp when the statistics were created.
        """)

    next_update_at = components.DateTimeField(
        required=True,
        help_text="""
            Next update of the statistical data.
            This may not correspond to the aggregate interval.
        """)

    origin_timezone = components.CharField(
        required=True,
        help_text="""
            The timezone where the data was collected in tz database
            format.

            Example: "Europe/Amsterdam"
        """)

    fields = components.ListField(
        required=True,
        child=components.CharField(),
        help_text="""
            The fields used in the samples. They can be explicitly
            selected using the `fields` query parameter. The `timestamp` is
            always included.

            Example: ["timestamp", "average_pps_in", "average_pps_out", "average_ops_in", "average_ops_out"]
        """)

    samples = components.ListField(
        required=True,
        child=components.TimeseriesSampleField(),
        help_text="""
            A list of timeseries samples, where samples are n-tuples, where
            the first element is a timestamp.

            * `timestamp` (DateTime, `string`, ISO 8601, Example: `"2001-10-23T23:42:10Z"`)

            The other fields can be selected in the `fields` query from the
            aggregates. Default fields are:

            * `average_pps_in`  (`integer`)
            * `average_pps_out` (`integer`)
            * `average_ops_in` (`integer`)
            * `average_ops_out` (`integer`)

            Missing values in the timeseries are represented by `null`.
        """)


class PeerAggregate(NetworkServiceConfigAggregate, components.Component):
    """PeerStatistics"""
    peer = Peer()


class PeerTimeseries(AggregateTimeseries, components.Component):
    """PeerStatistics Timeseries"""
    peer = Peer()
