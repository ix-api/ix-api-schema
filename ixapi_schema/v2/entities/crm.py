

from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import events


class ManageableBase(components.Component):
    """Managed"""
    managing_account = components.PrimaryKeyRelatedField(
        related="Account",
        help_text="""
            The `id` of the account responsible for managing the service via
            the API. A manager can read and update the state of entities. 

            Example: "238189294"
        """)


class ExternalReferenceBase(components.Component):
    """External Reference"""
    external_ref = components.CharField(
        default=None,
        allow_null=True,
        allow_blank=False,
        max_length=128,
        help_text="""
            Reference field, free to use for the API user.
            *(Sensitive Property)*

            Example: IX:Service:23042
        """)


class ConsumableBase(components.Component):
    """Consumable"""
    consuming_account = components.PrimaryKeyRelatedField(
        related="Account",
        help_text="""
            The `id` of the account consuming a service.

            Used to be `owning_customer`.

            Example: "2381982"
        """)


# Composit shortcut
class OwnableBase(
        ManageableBase,
        ConsumableBase,
        ExternalReferenceBase,
        components.Component,
    ):
    """Ownable"""


class ContactableBase(components.Component):
    """Contactable"""
    role_assignments = components.PrimaryKeyRelatedField(
        required=True,
        related="RoleAssignment",
        many=True,
        help_text="""
            A set of `RoleAssignment`s. See the documentation
            on the specific `required_contact_roles`,
            `nfc_required_contact_roles` or `nsc_required_contact_roles`
            on what `RoleAssignment`s to provide.

            Please note, that any contact role can additionally be provided.
            The presence of at least one of each required contact roles
            is necessary.

            Example: ["c-impl:123", "c-noc:331"]
        """)


class ContractableBase(components.Component):
    """Invoiceable"""
    purchase_order = components.CharField(
        allow_blank=True,
        allow_null=False,
        default="",
        max_length=80,
        help_text="""
            Purchase Order ID which will be displayed on the invoice.
            *(Sensitive Property)*

            Example: "Project: DC Moon"
        """)
    contract_ref = components.CharField(
        allow_null=True,
        allow_blank=False,
        default=None,
        max_length=128,
        help_text="""
            A reference to a contract. If no specific contract is used,
            a default MAY be chosen by the implementer.
            *(Sensitive Property)*

            Example: "contract:31824"
        """)


class BillableBase(components.Component):
    """Has BillingAccount"""
    billing_account = components.PrimaryKeyRelatedField(
        related="Account",
        help_text="""
            An account requires billing_information to be used as a `billing_account`.
            *(Sensitive Property)*
        """)


class InvoiceableBase(ContractableBase, BillableBase):
    """Invoiceable"""


class Address(components.Component):
    """A postal address. *(Sensitive Property)*"""
    country = components.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE
            example: US
        """)
    locality = components.CharField(
        max_length=40,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    region = components.CharField(
        allow_null=True,
        required=False,
        max_length=80,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = components.CharField(
        max_length=24,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = components.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)
    post_office_box_number = components.CharField(
        allow_null=True,
        required=False,
        max_length=80,
        help_text="""
            The post office box number for PO box addresses.
            example: "2335232"
        """)


#
# Account Serializers
#

class BillingInformation(components.Component):
    """
    Optional information required for issuing invoices.
    Only accounts with `billing_information` present can be used
    as a `billing_account`. *(Sensitive Property)*
    """
    name = components.CharField(
        max_length=80,
        help_text="""
            Name of the organisation receiving invoices.

            Example: Moonoc Network Services LLS.
        """)

    address = Address(help_text="""
        Billing-Address of the account.
    """)

    vat_number = components.CharField(
        min_length=2,
        max_length=20,
        required=False,
        allow_null=True,
        help_text="""
            Value-added tax number, required for
            european reverse charge system.

            Example: UK2300000042
        """)


class AccountBase(components.Component):
    """Account Base"""
    id = components.PrimaryKeyField()

    managing_account = components.PrimaryKeyRelatedField(
        default=None,
        allow_null=True,
        related="Account",
        help_text="""
            The `id` of a managing account. Can be used for creating
            a customer hierachy. *(Sensitive Property)*

            Example: IX:Account:231
        """)

    name = components.CharField(
        max_length=80, required=True,
        help_text="""
            Name of the account, how it gets represented
            in e.g. a "customers list".

            Example: Moonpeer Inc.
        """)

    legal_name = components.CharField(
        default=None,
        allow_blank=False,
        allow_null=True,
        max_length=80,
        help_text="""
            Legal name of the organisation.
            Only required when it's different from the account name.
            *(Sensitive Property)*

            Example: Moon Network Services LLS.
        """)


    billing_information = BillingInformation(
        default=None,
        allow_null=True,
        required=False)

    external_ref = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        required=False,
        label="External Reference",
        help_text="""
            Reference field, free to use for the API user. 
            *(Sensitive Property)*
            Example: IX:Service:23042
        """)

    discoverable = components.BooleanField(
        default=False,
        help_text="""
            The account will be included for all members of the
            ix in the list of accounts.

            Only `id`, `name` and `present_in_metro_area_networks`
            are provided to other members.
        """)

    metro_area_network_presence = components.PrimaryKeyRelatedField(
        related="MetroAreaNetwork",
        many=True,
        default=[],
        help_text="""
            Informal list of `MetroAreaNetwork` ids, indicating the
            presence to other accounts.
            The list is maintained by the account and can be empty.

            Example: ["14021", "12939"]
        """)



class Account(events.StatefulBase, AccountBase):
    """Account"""
    # Note that state is not inherited from
    # the events.Stateful trait, because the state property
    # is not part of the account response, if e.g. the account
    # is visible due to being discoverable.
    state = components.EnumField(events.State, required=False)

    address = Address(allow_null=False, required=False)

    metro_area_network_presence = components.PrimaryKeyRelatedField(
        related="MetroAreaNetwork",
        many=True,
        required=True,
        help_text="""
            Informal list of `MetroAreaNetwork` ids, indicating the
            presence to other accounts.
            The list is maintained by the account and can be empty.

            Example: ["14021", "12939"]
        """)


class AccountRequest(AccountBase):
    """Account Request"""
    address = Address()


class AccountUpdate(AccountBase):
    """Account Update"""
    address = Address()
    metro_area_network_presence = components.PrimaryKeyRelatedField(
        related="MetroAreaNetwork",
        many=True,
        help_text="""
            Informal list of `MetroAreaNetwork` ids, indicating the
            presence to other accounts.
            The list is maintained by the account and can be empty.

            Example: ["14021", "12939"]
        """)


class AccountPatch(AccountUpdate):
    """Account Update"""

#
# Contacts
#


class ContactBase(OwnableBase, components.Component):
    """
    A contact is a collection of VCARD properties.
    Which fields are required is dependent on the
    role(s) the contact is assigned to.

    For now we only allow name, telephone an d email
    """
    id = components.PrimaryKeyField()
    name = components.CharField(
        max_length=128,
        allow_null=True,
        required=False,
        help_text="""
            A name of a person or an organisation
            Example: "Some A. Name"
        """)
    telephone = components.CharField(
        max_length=40,
        allow_null=True,
        required=False,
        help_text="""
            The telephone number in E.164 Phone Number Formatting
            Example: +442071838750
        """)

    email = components.EmailField(
        allow_null=True,
        required=False,
        max_length=80,
        help_text="""
            The email of the legal company entity.

            Example: info@moon-peer.net
        """)


class Contact(ContactBase):
    """Contact"""

class ContactRequest(ContactBase):
    """Contact Create Request"""


class ContactUpdate(ContactBase):
    """Contact Update"""


class ContactPatch(ContactUpdate):
    """Contact Update"""


class RoleBase(components.Component):
    """A Role"""
    name = components.CharField(
        max_length=80,
        help_text="""
            The name of the role.

            Example: "noc"
        """)

    required_fields = components.ListField(
        child=components.CharField(max_length=80),
        help_text="""
            A list of required field names.

            Example: ["name", "email"]
        """)


class Role(RoleBase):
    """Role for a Contact"""
    id = components.PrimaryKeyField()


class RoleRequest(RoleBase):
    """Create Role"""


class RoleUpdate(RoleBase):
    """Role Update"""


class RolePatch(RoleUpdate):
    """Role Update"""


class RoleAssignmentBase(components.Component):
    """A role can be assigned to a contact"""
    role = components.PrimaryKeyRelatedField(
        related="Role",
        help_text="""
            The `id` of a role the contact is assigned to.

            Example: "role:23"
        """)
    contact = components.PrimaryKeyRelatedField(
        related="Contact",
        help_text="""
            The `id` of a contact the role is assigned to.

            Example: "contact:42b"
        """)


class RoleAssignment(RoleAssignmentBase):
    """A role assignment for a contact"""
    id = components.PrimaryKeyField()


class RoleAssignmentRequest(RoleAssignmentBase):
    """A role assignment request"""


class RoleAssignmentUpdate(RoleAssignmentBase):
    """A role assignemnt update"""


class RoleAssignmentPatch(RoleAssignmentUpdate):
    """A role assignment update"""
