
import enum

from ixapi_schema.openapi import components, timezone
from ixapi_schema.v2.entities import crm


class IpAddressShort(components.Component):
    """IP-Address"""
    version = components.IpVersionField(
        help_text="""
            The version of the internet protocol.

            Example: 6
        """)
    address = components.CharField(
        help_text="""
            The IP address in the following format:
            - IPv4: [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
            - IPv6: hexadecimal colon separated notation

            Example: "fd42:2342::1"
        """)


class IpAddressBase(
        crm.OwnableBase,
        components.Component,
    ):
    """IP-Address"""
    id = components.PrimaryKeyField()
    version = components.IpVersionField(
        help_text="""
            The version of the internet protocol.

            Example: 4
        """)
    address = components.CharField(
        help_text="""
                IPv4 or IPv6 Address in the following format:
                - IPv4: [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
                - IPv6: hexadecimal colon separated notation

                Example: "23.142.52.0"
            """)
    prefix_length = components.IntegerField(
        min_value=0,
        max_value=128,
        help_text="""
            The CIDR ip prefix length

            Example: 23
        """)

    fqdn = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        allow_blank=False)

    valid_not_before = components.DateTimeField(
        default=timezone.now,
        allow_null=True)
    valid_not_after = components.DateTimeField(
        default=None,
        allow_null=True)


class IpAddressRequest(IpAddressBase):
    """IP-Address / Prefix allocation Request"""


class IpAddressUpdate(IpAddressBase):
    """IP-Address Update"""


class IpAddressPatch(IpAddressUpdate):
    """IP-Address Update"""


class IpAddress(IpAddressBase):
    """IP-Address"""


class MacAddressBase(
        crm.OwnableBase,
        components.Component,
    ):
    """MAC-Address""" # And cheese address
    id = components.PrimaryKeyField()
    address = components.MacAddressField(
        help_text="""
            Unicast MAC address, formatted hexadecimal values with colons.

            Example: 42:23:bc:8e:b8:b0
        """)

    valid_not_before = components.DateTimeField(
        allow_null=True,
        default=timezone.now,
        help_text="""
             When a mac address is assigned to a NSC, and the current
             datetime is before this value, then the MAC address *cannot*
             be used on the peering platform.

             Afterwards, it is supposed to be available. If the value is
             `null` or the property does not exist, the mac address is
             valid from the creation date.
        """)
    valid_not_after = components.DateTimeField(
        default=None,
        allow_null=True,
        help_text="""
            When a mac address is assigned to an NSC, and the current datetime
            is before this value, the MAC address *can* be used on the peering platform.

            Afterwards, it is supposed to be unassigned from the NSC and cannot
            any longer be used on the peering platform.

            If the value is null or the property does not exist, the MAC address
            is valid indefinitely. The value may not be in the past.
        """)


class MacAddressRequest(MacAddressBase):
    """MAC-Address Request"""


class MacAddress(MacAddressBase):
    """MAC-Address"""


class Peer(components.Component):
    """
    MAC-, IP-Address and ASN of the peer.
    """
    asn = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            The ASN of the peer.

            Example: 65534
        """)

    ip = IpAddressShort(required=False)

    mac_address = components.MacAddressField(
        help_text="""
            Unicast MAC address, formatted hexadecimal values with colons.

            Example: 42:23:bc:8e:b8:b0
        """)

