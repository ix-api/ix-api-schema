
import enum

from ixapi_schema.openapi import components, timezone
from ixapi_schema.v1.entities import crm
from ixapi_schema.v1.constants import ipam



class IpAddress(
        crm.Ownable,
        components.Component,
    ):
    """IP-Address"""
    id = components.PrimaryKeyField()

    version = components.IpVersionField(read_only=True)
    address = components.CharField(
        read_only=True,
        help_text="""
                IPv4 or IPv6 Address in the following format:
                - IPv4: [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
                - IPv6:

                Example: "23.142.52.0"
            """)
    prefix_length = components.IntegerField(
        read_only=True,
        min_value=0,
        max_value=128,
        help_text="""
            The CIDR ip prefix length

            Example: 23
        """)

    fqdn = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        allow_blank=False)

    valid_not_before = components.DateTimeField(read_only=True)
    valid_not_after = components.DateTimeField(read_only=True)


class IpAddressPatch(IpAddress):
    """IP-Address Patch"""


class MacAddress(
        crm.Ownable,
        components.Component,
    ):
    """MAC-Address""" # And cheese address
    id = components.PrimaryKeyField()

    address = components.MacAddressField(
        help_text="""
            MAC Address Value, formatted hexadecimal values with colons.

            Example: 42:23:bc:8e:b8:b0
        """)

    valid_not_before = components.DateTimeField(
        allow_null=True,
        default=timezone.now)
    valid_not_after = components.DateTimeField(
        default=None,
        allow_null=True)
