
import enum

from ixapi_schema.openapi import components


class State(enum.Enum):
    """Enum containing all states the statemachine can transition"""
    REQUESTED = "requested"
    ALLOCATED = "allocated"

    TESTING = "testing"
    PRODUCTION = "production"

    DECOMISSION_REQUESTED = "decommission_requested"
    DECOMMISSIONED = "decommissioned"

    ARCHIVED = "archived"

    ERROR = "error"
    OPERATOR = "operator"
    SCHEDULED = "scheduled" # I'm not convinced about this.


class Severity:
    """Status Severity"""
    EMERGENCY = 0
    ALERT = 1
    CRITICAL = 2
    ERROR = 3
    WARNING = 4
    NOTICE = 5
    INFORMATIONAL = 6
    DEBUG = 7


class Status(components.Component):
    """Status Message"""
    severity = components.IntegerField(
        min_value=0,
        max_value=7,
        help_text="""
            We are using syslog severity levels: 0 = Emergency,
            1 = Alert, 2 = Critical, 3 = Error, 4 = Warning,
            5 = Notice, 6 = Informational, 7 = Debug.

            Example: 2
        """)
    tag = components.CharField()
    message = components.CharField()
    attrs = components.JSONField()
    timestamp = components.DateTimeField()


class Event(components.Component):
    """Event"""
    serial = components.IntegerField()
    customer = components.PrimaryKeyRelatedField(
        related="Customer",
        read_only=True)

    type = components.CharField()
    payload = components.JSONField()

    timestamp = components.DateTimeField()


class Stateful(components.Component):
    """A stateful object"""
    state = components.EnumField(State, required=True)
    status = Status(many=True, read_only=True)

