
from ixapi_schema.openapi import components
from ixapi_schema.v1.entities import events


class Ownable(components.Component):
    """Ownable"""
    managing_customer = components.PrimaryKeyRelatedField(
        related="Customer",
        help_text="""
            The `id` of the customer responsible for managing the service via
            the API.
            Used to be `billing_customer`.

            Example: "238189294"
        """)
    consuming_customer = components.PrimaryKeyRelatedField(
        related="Customer",
        help_text="""
            The `id` of the customer account consuming a service.

            Used to be `owning_customer`.

            Example: "2381982"
        """)
    external_ref = components.CharField(
        default=None,
        allow_null=True,
        allow_blank=False,
        max_length=128,
        help_text="""
            Reference field, free to use for the API user.
            Example: IX:Service:23042
        """)


class Contactable(components.Component):
    """Contactable"""
    contacts = components.PrimaryKeyRelatedField(
        required=True,
        related="Contact",
        many=True,
        help_text="""
            Polymorphic set of contacts. See the documentation
            on the specific `required_contact_types` on what
            contacts to provide.

            Example: ["c-impl:123", "c-noc:331"]
        """)


class Invoiceable(Contactable, components.Component):
    """Invoiceable"""
    purchase_order = components.CharField(
        allow_blank=True,
        allow_null=False,
        default="",
        max_length=80,
        help_text="""
            Purchase Order ID which will be displayed on the invoice.

            Example: "Project: DC Moon"
        """)
    contract_ref = components.CharField(
        allow_null=True,
        allow_blank=False,
        default=None,
        max_length=128,
        help_text="""
            A reference to a contract.

            Example: "contract:31824"
        """)


#
# Customer Serializers
#

class CustomerBase(components.Component):
    """Customer Base"""
    id = components.PrimaryKeyField()

    parent = components.PrimaryKeyRelatedField(
        related="Customer",
        default=None,
        allow_null=True,
        help_text="""
            The `id` of a parent customer. Can be used for creating
            a customer hierachy.

            Example: IX:Customer:231
        """)

    name = components.CharField(
        max_length=80, required=True,
        help_text="""
            Name of customer, how it gets represented
            in all customer lists.

            Example: Moonpeer Inc.
        """)

    external_ref = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        required=False,
        label="External Reference",
        help_text="""
            Reference field, free to use for the API user.
            Example: IX:Service:23042
        """)


class Customer(events.Stateful, CustomerBase):
    """Customer"""


class CustomerInput(CustomerBase):
    """Customer Request"""


class CustomerUpdate(CustomerBase):
    """Customer Update Request"""


class CustomerPatch(CustomerUpdate):
    """Customer Patch Request"""

#
# Contacts Serializers
#
CONTACT_TYPE_LEGAL = "legal"
CONTACT_TYPE_NOC = "noc"
CONTACT_TYPE_PEERING = "peering"
CONTACT_TYPE_IMPLEMENTATION = "implementation"
CONTACT_TYPE_BILLING = "billing"

CONTACT_TYPES = [
    CONTACT_TYPE_LEGAL,
    CONTACT_TYPE_NOC,
    CONTACT_TYPE_PEERING,
    CONTACT_TYPE_IMPLEMENTATION,
    CONTACT_TYPE_BILLING,
]


class ContactBase(Ownable, components.Component):
    """Contact Base"""
    id = components.PrimaryKeyField()

    __polymorphic__ = "Contact"


class LegalContact(ContactBase):
    """Legal Contact"""
    legal_company_name = components.CharField(
        max_length=80,
        help_text="""
            The official name of the organization,
            e.g. the registered company name.

            Example: Example Inc.
        """)

    address_country = components.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE
            example: US
        """)
    address_locality = components.CharField(
        max_length=40,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = components.CharField(
        max_length=24,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = components.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)
    post_office_box_number = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        help_text="""
            The post office box number for PO box addresses.
            example: "2335232"
        """)

    email = components.EmailField(
        max_length=80,
        help_text="""
            The email of the legal company entity.

            Example: info@moon-peer.net
        """)

    __polymorphic_type__ = CONTACT_TYPE_LEGAL


class NocContact(ContactBase):
    """NOC Contact Response"""
    telephone = components.CharField(
        max_length=40,
        default=None,
        allow_null=True,
        help_text="""
            The telephone number in E.164 Phone Number Formatting
            example: +442071838750
        """)
    email = components.EmailField(
        max_length=80,
        help_text="""
            The email of the NOC. Typically noc@<customer>

            Example: noc@moon-peer.net
        """)

    __polymorphic_type__ = CONTACT_TYPE_NOC


class NocContactPatch(NocContact):
    """NOC Contact Patch Request"""
    __polymorphic__ = "ContactPatch"


class PeeringContact(ContactBase):
    """Peering Contact Response"""
    email = components.EmailField(
        max_length=80,
        help_text="""
            The peering email of the customer. Typically peering@<customer>

            Example: peering@moon-peer.net
        """)

    __polymorphic_type__ = CONTACT_TYPE_PEERING


class PeeringContactPatch(PeeringContact):
    """Peering Contact Patch Request"""
    __polymorphic__ = "ContactPatch"


class ImplementationContact(ContactBase):
    """Implementation Contact Response"""
    name = components.CharField(
        max_length=80,
        help_text="""
            Name of implementation contact
            Example: John Dough
        """)
    telephone = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        help_text="""
            The telephone number in E.164 Phone Number Formatting
            example: +442071838750
        """)
    email = components.EmailField(
        max_length=80,
        help_text="""
            Email address of the implementation contact.
            Example: implementation@example.org
        """)
    legal_company_name = components.CharField(
        default=None,
        allow_blank=False,
        allow_null=True,
        max_length=80,
        help_text="""
            Name of the company the implentation person is
            working for, e.g. a third party company.

            Example: Moonoc Network Services LLS.
        """)

    __polymorphic_type__ = CONTACT_TYPE_IMPLEMENTATION


class ImplementationContactPatch(ImplementationContact):
    """Implementation Contact Patch Request"""
    __polymorphic__ = "ContactPatch"


class BillingContact(ContactBase):
    """Billing Contact Response"""
    legal_company_name = components.CharField(
        max_length=80,
        help_text="""
            The official name of the organization,
            e.g. the registered company name.

            Example: Example Inc.
        """)

    address_country = components.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE
            example: US
        """)
    address_locality = components.CharField(
        max_length=40,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = components.CharField(
        max_length=24,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = components.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)
    post_office_box_number = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        help_text="""
            The post office box number for PO box addresses.
            example: "2335232"
        """)

    email = components.EmailField(
        max_length=80,
        help_text="""
            The email address for sending the invoice.
            example: invoice@example.org
        """)

    vat_number = components.CharField(
        min_length=2,
        max_length=20,
        allow_blank=False,
        allow_null=True,
        help_text="""
            Value-added tax number, required for
            european reverse charge system.

            Example: UK2300000042
        """)

    __polymorphic_type__ = CONTACT_TYPE_BILLING


class BillingContactPatch(BillingContact):
    """Billing Contact Patch Request"""
    __polymorphic__ = "ContactPatch"


# As we now have defined our base contact type components,
# we can now create a polymorphic serializer:
class Contact(components.PolymorphicComponent):
    """Polymorphic Contact"""
    serializer_classes = {
        CONTACT_TYPE_LEGAL:
            LegalContact,
        CONTACT_TYPE_NOC:
            NocContact,
        CONTACT_TYPE_PEERING:
            PeeringContact,
        CONTACT_TYPE_IMPLEMENTATION:
            ImplementationContact,
        CONTACT_TYPE_BILLING:
            BillingContact,
    }

    entity_types = {
        "crm.LegalContact":
            CONTACT_TYPE_LEGAL,
        "crm.NocContact":
            CONTACT_TYPE_NOC,
        "crm.PeeringContact":
            CONTACT_TYPE_PEERING,
        "crm.ImplementationContact":
            CONTACT_TYPE_IMPLEMENTATION,
        "crm.BillingContact":
            CONTACT_TYPE_BILLING,
    }


class ContactPatch(components.PolymorphicComponent):
    """Polymorphic Contact Patch"""
    serializer_classes = {
        CONTACT_TYPE_LEGAL:
            LegalContact,
        CONTACT_TYPE_NOC:
            NocContactPatch,
        CONTACT_TYPE_PEERING:
            PeeringContactPatch,
        CONTACT_TYPE_IMPLEMENTATION:
            ImplementationContactPatch,
        CONTACT_TYPE_BILLING:
            BillingContactPatch,
    }

    entity_types = {
        "crm.LegalContact":
            CONTACT_TYPE_LEGAL,
        "crm.NocContact":
            CONTACT_TYPE_NOC,
        "crm.PeeringContact":
            CONTACT_TYPE_PEERING,
        "crm.ImplementationContact":
            CONTACT_TYPE_IMPLEMENTATION,
        "crm.BillingContact":
            CONTACT_TYPE_BILLING,
    }
