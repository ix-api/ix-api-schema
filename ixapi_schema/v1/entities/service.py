"""
Service Entities
----------------

"""

from ixapi_schema.openapi import components
from ixapi_schema.v1.entities import crm
from ixapi_schema.v1.constants.ipam import AddressFamilies
from ixapi_schema.v1.entities.access import (
    RouteServerSessionMode,
    BGPSessionType,
)


NETWORK_FEATURE_TYPE_BLACKHOLING = "blackholing"
NETWORK_FEATURE_TYPE_ROUTESERVER = "route_server"
NETWORK_FEATURE_TYPE_IXPROUTER = "ixp_router"

NETWORK_FEATURE_TYPES = [
    NETWORK_FEATURE_TYPE_BLACKHOLING,
    NETWORK_FEATURE_TYPE_ROUTESERVER,
    NETWORK_FEATURE_TYPE_IXPROUTER,
]



class IXPSpecificFeatureFlag(components.Component):
    """IXP-Specific Feature Flag"""
    name = components.CharField(
        max_length=20,
        help_text="""
            The name of the feature flag.

            Example: RPKI
        """)
    description = components.CharField(
        max_length=80,
        help_text="""
            The description of the feature flag.

            Example: RPKI Hard Filtering is available
        """)


class NetworkFeatureBase(
        components.Component,
    ):
    """Feature Base"""
    id = components.PrimaryKeyField()

    name = components.CharField(max_length=80)
    required = components.BooleanField()

    required_contact_types = components.ListField(
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified contact types.

            They can be assigned in the `contacts` list property of the
            network feature configuration.
        """)

    flags = IXPSpecificFeatureFlag(
        source="ixp_specific_flags",
        many=True,
        help_text="""
            A list of IXP specific feature flags. This can be used
            to see if e.g. RPKI hard filtering is available.
        """)

    network_service = components.PrimaryKeyRelatedField(
        related="NetworkService")

    __polymorphic__ = "NetworkFeature"


class BlackholingNetworkFeature(NetworkFeatureBase):
    """Blackholing Network Feature"""
    # Includes only base fields
    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_BLACKHOLING
    __hidden_in_docs__ = True


class RouteServerNetworkFeature(NetworkFeatureBase):
    """Route Server Network Feature"""
    asn = components.IntegerField(min_value=0)
    fqdn = components.CharField(
        max_length=80,
        help_text="""
            The FQDN of the route server.

            Example: rs1.moon-ix.net
        """)
    looking_glass_url = components.CharField(
        help_text="""
            The url of the looking glass.

            Example: https://lg.moon-ix.net/rs1
        """)

    address_families = components.ListField(
        child=components.EnumField(AddressFamilies),
        min_length=1,
        max_length=2,
        help_text="""
            When creating a route server feature config, remember
            to specify which address family or families to use.

            Example: ["af_inet"]
        """)

    session_mode = components.EnumField(
        RouteServerSessionMode,
        help_text="""
            When creating a route server feature config, remember
            to specify the same session_mode as the route server.

            Example: "public"
        """)

    available_bgp_session_types = components.ListField(
        child=components.EnumField(BGPSessionType),
        min_length=1,
        max_length=2,
        help_text="""
            The route server provides the following session modes.

            Example: ["passive"]
        """)

    ips = components.PrimaryKeyRelatedField(
        related="IpAddress",
        source="ip_addresses",
        many=True,
        read_only=True)

    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_ROUTESERVER


class IXPRouterNetworkFeature(NetworkFeatureBase):
    """IXP Router Network Feature"""
    asn = components.IntegerField(min_value=0)
    fqdn = components.CharField(max_length=80)

    ips = components.PrimaryKeyRelatedField(
        related="IpAddress",
        source="ip_addresses",
        read_only=True,
        many=True)

    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_IXPROUTER
    __hidden_in_docs__ = True



class NetworkFeature(components.PolymorphicComponent):
    """Polymorphic Network Feature"""
    serializer_classes = {
        NETWORK_FEATURE_TYPE_BLACKHOLING:
            BlackholingNetworkFeature,
        NETWORK_FEATURE_TYPE_ROUTESERVER:
            RouteServerNetworkFeature,
        NETWORK_FEATURE_TYPE_IXPROUTER:
            IXPRouterNetworkFeature,
    }

    entity_types = {
        "BlackholingNetworkFeature":
            NETWORK_FEATURE_TYPE_BLACKHOLING,
        "RouteserverNetworkFeature":
            NETWORK_FEATURE_TYPE_ROUTESERVER,
        "IXPRouterNetworkFeature":
            NETWORK_FEATURE_TYPE_IXPROUTER,
    }


#
# Network Services
#
NETWORK_SERVICE_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_TYPE_CLOSED_USER_GROUP = "closed_user_group"
NETWORK_SERVICE_TYPE_ELINE = "eline"
NETWORK_SERVICE_TYPE_CLOUD = "cloud"

NETWORK_SERVICE_TYPES = [
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_CLOSED_USER_GROUP,
    NETWORK_SERVICE_TYPE_ELINE,
    NETWORK_SERVICE_TYPE_CLOUD,
]


class NetworkServiceBase(
        crm.Ownable,
        crm.Invoiceable,
        components.Component,
    ):
    """Network Service (Base)"""
    id = components.PrimaryKeyField()

    product = components.PrimaryKeyRelatedField(
        related="Product")

    required_contact_types = components.ListField(
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified contact types.
            They can be assigned in the `contacts` list property of the
            config.
        """)

    network_features = components.PrimaryKeyRelatedField(
        related="NetworkFeature",
        many=True, read_only=True)

    __polymorphic__ = "NetworkService"


class ExchangeLanNetworkService(NetworkServiceBase):
    """Exchange Lan Network Service"""
    name = components.CharField(
        max_length=40,
        help_text="""
            Exchange dependend service name, will be shown on the invoice.
        """)
    metro_area = components.CharField(
        max_length=3,
        help_text="""
            3 Letter (IATA) Airport Code of the MetroArea where
            the exchange lan network service is available.

            Example: FRA
        """)
    peeringdb_ixid = components.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            PeeringDB ixid
        """)
    ixfdb_ixid = components.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            id of ixfdb
        """)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        related="IpAddress",
        read_only=True, many=True)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_EXCHANGE_LAN


class ClosedUserGroupNetworkService(NetworkServiceBase):
    """Closed User Group Network Service"""
    # Well. D'oh.
    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_CLOSED_USER_GROUP
    __hidden_in_docs__ = True


class ELineNetworkService(NetworkServiceBase):
    """E-Line Network Service"""
    # Well. D'oh. Does not get any better here.
    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_ELINE
    __hidden_in_docs__ = True


class CloudNetworkService(NetworkServiceBase):
    """Cloud Network Service"""
    # We have to update this according to the usecase...
    cloud_provider_name = components.CharField()

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_CLOUD
    __hidden_in_docs__ = True


class NetworkService(components.PolymorphicComponent):
    """Polymorphic Network Services"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_ELINE:
            ELineNetworkService,
        NETWORK_SERVICE_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkService,
        NETWORK_SERVICE_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkService,
        NETWORK_SERVICE_TYPE_CLOUD:
            CloudNetworkService,
    }

    entity_types = {
        "service.ExchangeLanNetworkService":
            NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
        "service.ELineNetworkService":
            NETWORK_SERVICE_TYPE_ELINE,
        "service.ClosedUserGroupNetworkService":
            NETWORK_SERVICE_TYPE_CLOSED_USER_GROUP,
        "service.CloudNetworkService":
            NETWORK_SERVICE_TYPE_CLOUD,
    }

