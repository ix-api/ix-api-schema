
"""
Auth entities
"""

from ixapi_schema.openapi import components


#
# Auth Components
#

class AuthTokenRequest(components.Component):
    """AuthTokenRequest"""
    api_key = components.CharField(max_length=16,
                                    min_length=16,
                                    required=True)
    api_secret = components.CharField(max_length=86,
                                       min_length=86,
                                       required=True)



class AuthToken(components.Component):
    """AuthToken"""
    access_token = components.CharField(required=True)
    refresh_token = components.CharField(required=True)


class RefreshTokenRequest(components.Component):
    """RefreshTokenRequest"""
    refresh_token = components.JWTField(required=True)


