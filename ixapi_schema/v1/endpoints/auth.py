
from ixapi_schema.openapi import components
from ixapi_schema.v1.entities import auth, problems


PATHS = {
    "/auth/token": {
        "description": """
            Open an authorized session with the IX-API by
            sending an `api_key` and `api_secret` pair to the
            `/api/v1/tokens` resource.

            This is equivalent to a 'login' endpoint.

            Upon success, you will receive an `access_token` and
            a `refreh_token`. The refresh token can be used for
            regenerating the lifetime limited access token.

            Use the `access_token` as `Bearer` in your `Authorziation` header
            for getting access to the API.

            The `auth_token` (and `refresh_token`) have limited lifetimes.
            As both are JWTs, you can directy get this information from them.

            When the session (`access_token`) expires, you can (within
            the lifetime of the `refresh_token`) reauthenticate by
            providing only the `refresh_token`.

            Make a `POST` request to the `/api/v1/auth/refresh` resource.
        """,
        "POST": {
            "description": """
                Authenticate a customer identified by
                `api_key` and `api_secret`.
            """,
            "operation": "auth_token_create",
            "request": auth.AuthTokenRequest(),
            "responses": {
                200: auth.AuthToken(),
            },
            "problems": [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotAuthenticatedProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
    "/auth/refresh": {
        "description": """
            Open an authorized session with the IX-API by
            sending an `api_key` and `api_secret` pair to the
            `/api/v1/tokens` resource.

            This is equivalent to a 'login' endpoint.

            Upon success, you will receive an `access_token` and
            a `refreh_token`. The refresh token can be used for
            regenerating the lifetime limited access token.

            Use the `access_token` as `Bearer` in your `Authorziation` header
            for getting access to the API.

            The `auth_token` (and `refresh_token`) have limited lifetimes.
            As both are JWTs, you can directy get this information from them.

            When the session (`access_token`) expires, you can (within
            the lifetime of the `refresh_token`) reauthenticate by
            providing only the `refresh_token`.

            Make a `POST` request to the `/api/v1/auth/refresh` resource.
        """,
        "POST": {
            "description": """
                Reauthenticate the API user, issue a new `access_token`
                and `refresh_token` pair by providing the `refresh_token`
                in the request body.
            """,
            "operation": "auth_token_refresh",
            "request": auth.RefreshTokenRequest(),
            "responses": {
                200: auth.AuthToken(),
            },
            "problems": [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotAuthenticatedProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
}


