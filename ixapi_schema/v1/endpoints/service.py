
"""
Service Endpoints
"""

from ixapi_schema.openapi import components
from ixapi_schema.v1 import filters
from ixapi_schema.v1.entities import service, problems


PATHS = {
    #
    # Network Services
    #
    "/network-services": {
        "description": """
            A `NetworkService` is an instances of a `Product` accessible by one
            or multiple users, depending on the type of product

            For example, each Exchange Network LAN is considered as a shared
            instance of the related LAN's `Product`
        """,
        "GET": {
            "description": """
                List available `network-services`.
            """,
            "responses": {
                200: service.NetworkService(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        service.NETWORK_SERVICE_TYPES),
                    "pop": components.CharFilter(),
                    "product": components.CharFilter(),
                },
            ),
        },
    },
    "/network-services/<id:str>": {
        "description": """
            A `NetworkService` is an instances of a `Product` accessible by one
            or multiple users, depending on the type of product

            For example, each Exchange Network LAN is considered as a shared
            instance of the related LAN's `Product`
        """,
        "GET": {
            "description": """
                Get a specific `network-service` by id.
            """,
            "responses": {
                200: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },

    #
    # Network Features
    #
    "/network-features": {
        "description": """
            `NetworkFeatures` are functionality made available to customers
            within a `NetworkService`.
            Certain features may need to be configured by a customer to use
            that service.

            This can be for example a `route server` on an `exchange lan`.

            Some of these features are mandatory to configure if you
            want to access the platform. Which of these features you have to
            configure you can query using: 
                `/api/v1/network-features?required=true`
        """,
        "GET": {
            "description": """
                List available network features.
            """,
            "responses": {
                200: service.NetworkFeature(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                {
                    "type": components.ChoiceFilter(
                        service.NETWORK_FEATURE_TYPES),
                    "required": components.CharFilter(),
                    "network_service": components.CharFilter(),
                    "name": components.CharFilter(),
                },
            ),
        },
    },
    "/network-features/<id:str>": {
        "description": """
            `NetworkFeatures` are functionality made available to customers
            within a `NetworkService`.
            Certain features may need to be configured by a customer to use
            that service.

            This can be for example a `route server` on an `exchange lan`.

            Some of these features are mandatory to configure if you
            want to access the platform. Which of these features you have to
            configure you can query using: 
                `/api/v1/network-features?required=true`
        """,
        "GET": {
            "description": """
                Get a single network feature by it's id.
            """,
            "responses": {
                200: service.NetworkFeature(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
}

