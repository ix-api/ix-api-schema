

"""
IPAM
====

IP- and MAC-address related endpoints.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v1 import filters
from ixapi_schema.v1.entities import ipam, problems


PATHS = {
    #
    # IP Addresses
    #
    "/ips": {
        "description": """
            An `IP` is a IPv4 or 6 addresses, with a given validity period.
            Some services require IP addresses to work.

            When you are joining an `exchange_lan` network service
            for example, addresses on the peering lan will be assigned
            to you.
        """,
        "GET": {
            "description": """
                List all ip addresses (and prefixes).
            """,
            "responses": {
                200: ipam.IpAddress(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE,
                {
                    "network_service": components.CharFilter(),
                    "network_service_config": components.CharFilter(),
                    "network_feature": components.CharFilter(),
                    "network_feature_config": components.CharFilter(),
                    "version": components.NumberFilter(),
                    "fqdn": components.CharFilter(),
                    "prefix_length": components.NumberFilter(),
                    "valid_not_before": components.CharFilter(),
                    "valid_not_after": components.CharFilter(),
                },
            ),
        },
    },
    "/ips/<id:str>": {
        "description": """
            An `IP` is a IPv4 or 6 addresses, with a given validity period.
            Some services require IP addresses to work.

            When you are joining an `exchange_lan` network service
            for example, addresses on the peering lan will be assigned
            to you.
        """,
        "GET": {
            "description": """
                Get a single ip addresses by it's id.
            """,
            "responses": {
                200: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """
                Update an ip address object.

                You can only update
                IP addresses within your current scope. Not all
                addresses you can read you can update.
            """,
            "request": ipam.IpAddress(),
            "responses": {
                200: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """
                Update parts of an ip address.

                As with the `PUT` opertaion, IP addresses, where you
                don't have update rights, will yield a `resource access denied`
                error when attempting an update.
            """,
            "request": ipam.IpAddressPatch(partial=True),
            "responses": {
                200: ipam.IpAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
    },
    #
    # Mac Addresses
    #
    "/macs": {
        "description": """
            A `MAC` is a MAC addresses with a given validity period.

            Some services require MAC addresses to work.

            The address itself can not be updated after creation.
            However: It can be expired by changing the `valid_not_before`
            and `valid_not_after` properties.
        """,
        "GET": {
            "description": """
                List all mac addresses managed by the authorized customer.
            """,
            "responses": {
                200: ipam.MacAddress(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE,
                {
                    "network_service_config": components.CharFilter(),
                    "address": components.CharFilter(),
                    "assigned_at": components.CharFilter(),
                    "valid_not_before": components.CharFilter(),
                    "valid_not_after": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """
                Create a new mac address.
            """,
            "request": ipam.MacAddress(),
            "responses": {
                201: ipam.MacAddress(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/macs/<id:str>": {
        "description": """
            A `MAC` is a MAC addresses with a given validity period.

            Some services require MAC addresses to work.

            The address itself can not be updated after creation.
            However: It can be expired by changing the `valid_not_before`
            and `valid_not_after` properties.
        """,
        "GET": {
            "description": """
                Get a single mac address by it's id.
            """,
            "responses": {
                200: ipam.MacAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "DELETE": {
            "description": """
                Remove a mac address.
            """,
            "responses": {
                200: ipam.MacAddress(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
}

