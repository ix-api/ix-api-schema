
"""
CRM
---

Manage customers and customer contacts.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v1.entities import crm, problems
from ixapi_schema.v1 import filters


PATHS = {
    #
    # Customers
    #
    "/customers": {
        "description": """
            A `Customer` is a company using services from an IXP.
            The customers can have a hierarchy. A customer can have
            subcustomers. Each customer needs to have different contacts,
            depending on the exchange and the service he wants to use.

            For a customer to become operational, you need to provide a
            `legal` contact.
        """,
        "GET": {
            "description": """
                Retrieve a list of customers.

                This includes all customers the current authorized customer
                is managing and the current customer itself.
            """,
            "responses": {
                200: crm.Customer(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL, {
                    "parent": components.CharFilter(),
                    "external_ref": components.CharFilter(),
                    "name": components.CharFilter(),
                }),
        },
        "POST": {
            "description": """
                Create a new customer.

                Please remember that a customer may require some `contacts`
                to be created. Otherwise it will stay in an `error` state.
            """,
            "request": crm.CustomerInput(),
            "responses": {
                201: crm.Customer(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/customers/<id:str>": {
        "description": """
            A `Customer` is a company using services from an IXP.
            The customers can have a hierarchy. A customer can have
            subcustomers. Each customer needs to have different contacts,
            depending on the exchange and the service he wants to use.

            For a customer to become operational, you need to provide a
            `legal` contact.
        """,
        "GET": {
            "description": "Get a single customer.",
            "responses": {
                200: crm.Customer(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": "Update the entire customer.",
            "request": crm.Customer(),
            "responses": {
                200: crm.Customer(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": "Update the entire customer.",
            "request": crm.CustomerPatch(partial=True),
            "responses": {
                200: crm.Customer(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                A customer can be deleted, when all services and configs
                are decommissioned or the customer is not longer referenced
                e.g. as a `managing_customer` or `consuming_customer`.

                Deleting a customer will cascade to `contacts`, so
                contacts associated with the customer may not referenced
                in configs or services.

                Otherwise the request will immediately fail.
            """,
            "responses": {
                200: crm.Customer(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },

    #
    # Contacts
    #
    "/contacts": {
        "description": """
            A `Contact` is a role undertaking a specific responsibility
            within a customer, typically a department or agent of the
            customer company.

            These can be `implementation`, `noc`, `legal`, `peering`
            or `billing`.

            A contact is bound to the customer by the `consuming_customer`
            property.
        """,
        "GET": {
            "description": """
                List available contacts managed by the authorized customer.
            """,
            "responses": {
                200: crm.Contact(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE, {
                    "type": components.ChoiceFilter(crm.CONTACT_TYPES),
                    "external_ref": components.CharFilter(),
                })
        },
        "POST": {
            "description": """
                Create a new contact.

                Please remember to set the correct `type` of the contact
                when sending the request. Available types are `noc`, `billing`,
                `legal` and `implementation`.
            """,
            "request": crm.Contact(),
            "responses": {
                201: crm.Contact(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/contacts/<id:str>": {
        "description": """
            A `Contact` is a role undertaking a specific responsibility
            within a customer, typically a department or agent of the
            customer company. 

            These can be `implementation`, `noc`, `legal`, `peering`
            or `billing`.

            A contact is bound to the customer by the `consuming_customer`
            property.
        """,
        "GET": {
            "description": """Get a contact by it's id""",
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """Update a contact""",
            "request": crm.Contact(),
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """Update parts of a contact""",
            "request": crm.ContactPatch(partial=True),
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """Remove a contact""",
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
}

