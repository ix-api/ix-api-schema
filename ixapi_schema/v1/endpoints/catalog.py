
"""
Catalog
-------

The catalog bundles endpoints to query the
ix platform for facilities, pops, devices and so on.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v1.entities import catalog, problems


PATHS = {
    #
    # Facilities
    #
    "/facilities": {
        "description": """
            A `Facility` is a data centre, with a determined physical address,
            from which a defined set of PoPs can be accessed
        """,
        "GET": {
            "description": """
                Get a (filtered) list of `facilities`.
            """,
            "responses": {
                200: catalog.Facility(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": {
                "id": components.BulkIdFilter(),
                "cluster": components.CharFilter(),
                "device": components.CharFilter(),
                "capability_speed": components.NumberFilter(),
                "capability_speed__lt": components.NumberFilter(),
                "capability_speed__lte": components.NumberFilter(),
                "capability_speed__gt": components.NumberFilter(),
                "capability_speed__gte": components.NumberFilter(),
                "organisation_name": components.CharFilter(),
                "metro_area": components.CharFilter(),
                "address_country": components.CharFilter(),
                "address_locality": components.CharFilter(),
                "postal_code": components.CharFilter(),
            },
        },
    },
    "/facilities/<id:str>": {
        "description": """
            A `Facility` is a data centre, with a determined physical address,
            from which a defined set of PoPs can be accessed
        """,
        "GET": {
            "description": """
                Retrieve a facility by id
            """,
            "responses": {
                200: catalog.Facility(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # Devices
    #
    "/devices": {
        "description": """
            A `Device` is a network hardware device, typically a Switch, which
            is located at a specified facility and connected to one or more
            PoPs.

            They may be physically located at their related PoPs or remotely
            available.
        """,
        "GET": {
            "description": "List available devices",
            "responses": {
                200: catalog.Device(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": {
                "id": components.BulkIdFilter(),
                "name": components.CharFilter(),
                "capability_media_type": components.CharFilter(),
                "capability_speed": components.NumberFilter(),
                "capability_speed__lt": components.NumberFilter(),
                "capability_speed__lte": components.NumberFilter(),
                "capability_speed__gt": components.NumberFilter(),
                "capability_speed__gte": components.NumberFilter(),
                "faclitiy": components.CharFilter(),
                "product": components.CharFilter(),
            }
        }
    },
    "/devices/<id:str>": {
        "description": """
            A `Device` is a network hardware device, typically a Switch, which
            is located at a specified facility and connected to one or more
            PoPs.

            They may be physically located at their related PoPs or remotely
            available.
        """,
        "GET": {
            "description": "Get a specific device identified by id",
            "responses": {
                200: catalog.Device(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        }
    },
    #
    # Points Of Presence
    #
    "/pops": {
        "description": """
            A `PoP` is a location within a Facility which is connected to a
            single Network Infrastructure and has defined reachability of
            other facilities.

            A single room may contain multiple PoPs, each linking to a
            different infrastructure.
        """,
        "GET": {
            "description": "List all PoPs",
            "responses": {
                200: catalog.PointOfPresence(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": {
                "id": components.BulkIdFilter(),
                "physical_facility": components.CharFilter(),
                "reachable_facility": components.CharFilter(),
                "reachable_devices": components.CharFilter(),
            }
        }
    },
    "/pops/<id:str>": {
        "description": """
            A `PoP` is a location within a Facility which is connected to a
            single Network Infrastructure and has defined reachability of
            other facilities.

            A single room may contain multiple PoPs, each linking to a
            different infrastructure.
        """,
        "GET": {
            "description": "Get a single point of presence",
            "responses": {
                200: catalog.PointOfPresence(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        }
    },
    #
    # Products
    #
    "/products": {
        "description": """
            A `Product` is a network or peering-related product of a defined
            type sold by an IXP to its `Customers`
        """,
        "GET": {
            "description": """
                List all (filtered) products available on the platform
            """,
            "responses": {
                200: catalog.Product(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": {
                "id": components.BulkIdFilter(),
                "type": components.ChoiceFilter(catalog.PRODUCT_TYPES),
                "name": components.CharFilter(),
                "metro_area": components.CharFilter(),
                "provider": components.CharFilter(),
                "handover_point": components.CharFilter(),
                "zone": components.CharFilter(),
                "device": components.CharFilter(),
            },
        },
    },
    "/products/<id:str>": {
        "description": """
            A `Product` is a network or peering-related product of a defined
            type sold by an IXP to its `Customers`
        """,
        "GET": {
            "description": "Get a specific product by id",
            "responses": {
                200: catalog.Product(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        }
    },
}

