
"""
Access Endpoints
"""

from ixapi_schema.openapi import components
from ixapi_schema.v1 import filters
from ixapi_schema.v1.entities import access, problems


PATHS = {
    #
    # Demarcs
    #
    "/demarcs": {
        "description": """
            A `Demarc` (demarcation point) is the point at which customer and
            IXP networks meet, eg a physical port / socket, generally with a
            specified bandwidth.

            Demarcs are listed on a LoA (Letter of Authorisation).
            Exchange customers need this information to order a cross connect
            from the datacenter operator to be interconnected to the exchange.

            Due to the reason a `demarc` is patched to a switch, it comes with
            necessary extra informations like speed and optics `type`.
            A `demarc` is always associated to one `pop`.
        """,
        "GET": {
            "description": """List all `demarc`s.""",
            "responses": {
                200: access.DemarcationPoint(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL, {
                    "media_type": components.CharFilter(),
                    "pop": components.CharFilter(),
                    "name": components.CharFilter(),
                    "external_ref": components.CharFilter(),
                    "speed": components.CharFilter(),
                    "connection": components.CharFilter(),
                },
            ),
        },
    },
    "/demarcs/<id:str>": {
        "description": """
            A `Demarc` (demarcation point) is the point at which customer and
            IXP networks meet, eg a physical port / socket, generally with a
            specified bandwidth.

            Demarcs are listed on a LoA (Letter of Authorisation).
            Exchange customers need this information to order a cross connect
            from the datacenter operator to be interconnected to the exchange.

            Due to the reason a `demarc` is patched to a switch, it comes with
            necessary extra informations like speed and optics `type`.
            A `demarc` is always associated to one `pop`.
        """,
        "GET": {
            "description": """Retrieve a `demarc`""",
            "responses": {
                200: access.DemarcationPoint(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },

    #
    # Connections
    #
    "/connections": {
        "description": """
            A `Connection` is a functional group of physical connections
            collected together into a LAG (aka trunk).

            A `connection` with only one `demarc` can be also configured as
            standalone which means no LAG configuration on the switch.
        """,
        "GET": {
            "description": """List all `connection`s.""",
            "responses": {
                200: access.Connection(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL,
                {
                    "mode": components.CharFilter(),
                    "mode__is_not": components.CharFilter(),
                    "demarc": components.CharFilter(),
                    "name": components.CharFilter(),
                    "external_ref": components.CharFilter(),
                },
            ),

        },
    },
    "/connections/<id:str>": {
        "description": """
            A `Connection` is a functional group of physical connections
            collected together into a LAG (aka trunk).

            A `connection` with only one `demarc` can be also configured as
            standalone which means no LAG configuration on the switch.
        """,
        "GET": {
            "description": """Read a `connection`.""",
            "responses": {
                200: access.Connection(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        }
    },

    #
    # NetworkServiceConfigs
    #
    "/network-service-configs": {
        "description": """
            A `NetworkServiceConfig` is a customer's configuration for usage
            of a `NetworkService`, eg the configuration of a (subset of a)
            connection for that customer's traffic

            The `type` of the config determines the service you are
            configuring.

            You can find the services available to you on the platform,
            by querying the `/api/v1/network-services` resource.
        """,
        "GET": {
            "description": """Get all `network-service-config`s.""",
            "responses": {
                200: access.NetworkServiceConfig(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        access.NETWORK_SERVICE_CONFIG_TYPES),
                    "inner_vlan": components.NumberFilter(),
                    "outer_vlan": components.NumberFilter(),
                    "capacity": components.NumberFilter(),
                    "external_ref": components.CharFilter(),
                    "connection": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """Create a `network-service-config`.""",
            "request": access.NetworkServiceConfigRequest(),
            "responses": {
                201: access.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/network-service-configs/<id:str>": {
        "description": """
            A `NetworkServiceConfig` is a customer's configuration for usage
            of a `NetworkService`, eg the configuration of a (subset of a)
            connection for that customer's traffic

            The `type` of the config determines the service you are
            configuring.

            You can find the services available to you on the platform,
            by querying the `/api/v1/network-services` resource.
        """,
        "GET": {
            "description": """Get a `network-service-config`""",
            "responses": {
                200: access.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """Update an exisiting `network-service-config`""",
            "request": access.NetworkServiceConfigUpdate(),
            "responses": {
                200: access.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """
                Update parts of an exisiting `network-service-config`.
            """,
            "request": access.NetworkServiceConfigPatch(partial=True),
            "responses": {
                200: access.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
                Decommission the network service configuration.

                The `network-service-config` will assume the state
                `decommission_requested`.
                This will cascade to related `network-feature-configs`.
            """,
            "responses": {
                200: access.NetworkServiceConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # Network Feature Configs
    #
    "/network-feature-configs": {
        "description": """
            A `NetworkFeatureConfig` is a customer's configuration for
            usage of a `NetworkFeature`.
        """,
        "GET": {
            "description": "Get all network feature configs.",
            "responses": {
                200: access.NetworkFeatureConfig(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        access.NETWORK_FEATURE_CONFIG_TYPES),
                    "service_config": components.CharFilter(),
                    "network_feature": components.CharFilter(),
                    "external_ref": components.CharFilter(),
                }
            ),
        },
        "POST": {
            "description": """
                Create a new feature configuration.

                Remeber to provide a feature `type` and the id of the
                `network_feature` you want to configure.
                Additionally you have to provide the `network_service_config`
                where you want to use the network feature.

                You can query the available features from the
                `/api/v1/network-features` resource.
            """,
            "request": access.NetworkFeatureConfigRequest(),
            "responses": {
                201: access.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/network-feature-configs/<id:str>": {
        "description": """
            A `NetworkFeatureConfig` is a customer's configuration for
            usage of a `NetworkFeature`.
        """,
        "GET": {
            "description": "Get a single network feature config.",
            "responses": {
                200: access.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": "Update a network feature configuration",
            "request": access.NetworkFeatureConfigUpdate(),
            "responses": {
                200: access.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": "Update parts of a network feature configuration",
            "request": access.NetworkFeatureConfigPatch(partial=True),
            "responses": {
                200: access.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """
            Remove a network feature config.

            Removing a network feature config, will not cascade to
            the network service config, however, it might fail, if
            the network feature is required and a replacement
            configuration is not present.
            """,
            "responses": {
                200: access.NetworkFeatureConfig(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
}

