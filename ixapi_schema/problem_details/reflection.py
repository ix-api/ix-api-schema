
"""
Get documentation from problem classes.
Find problem classes given a path.
"""

import inspect


class ProblemNotFound(Exception):
    pass


def find_problems(problems):
    """Find all problems in a module."""
    module_problems = inspect.getmembers(problems, inspect.isclass)
    problem_classes = (c for (_, c) in module_problems
                       if issubclass(c, problems.Problem)
                       and not c == problems.Problem)

    # Sort classes
    problem_classes = sorted(
        problem_classes, key=lambda c: c.__name__)

    return problem_classes


def find_problem(problems, name):
    """Find a problem by name"""
    classes = find_problems(problems)
    for c in classes:
        if c.__name__ == name:
            return c
    raise ProblemNotFound


def find_problem_by_ptype(problems, ptype):
    """Find a problem by name"""
    classes = find_problems(problems)
    all_problems = (problem() for problem in classes)
    for prob in all_problems:
        if prob.ptype == ptype:
            return prob.__class__

    raise ProblemNotFound

