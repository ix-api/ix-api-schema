
"""
The IX-API schema provides the contract for interacting
with IX-API enabled internet exchanges.

This project is used to generate the OpenAPI schema
and provides serializer classes for the ix-api sandbox
project.
"""

__title__ = 'ix-api-schema'
__author__ = 'The IX-API Working Group'
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2018-2020, The IX-API Working Group'

# We use the newest released version here
from ixapi_schema.v1 import __version__
