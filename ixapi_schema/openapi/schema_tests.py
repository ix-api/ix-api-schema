
"""
Test OpenAPI Schema Generation
"""

from ixapi_schema import v1
from ixapi_schema import v2
from ixapi_schema.openapi import schema


def test_generate_v1():
    """Test generating the schema"""
    doc = schema.generate(v1)
    assert doc


def test_generate_v2():
    """Test generating the schema"""
    doc = schema.generate(v2)
    assert doc
