
import json


def is_json_serializable(obj):
    """Check if the object is serializable"""
    # For now, do this by invoking the json serializer
    # on the object and see if it raises an error.
    #
    # This might be an expensive way to do this.
    try:
        json.dumps(obj)
    except TypeError:
        return False

    return True

