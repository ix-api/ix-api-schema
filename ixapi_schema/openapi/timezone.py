

def now():
    """Return current timezone now"""
    from django.utils import timezone
    return timezone.now()

