"""
OpenAPI components

This module containts component classes used for
generating the OpenAPI schema.

The components can be used to create rest_framework
serializers.
"""
from enum import Enum

class ComponentMeta(type):
    """Components Meta Class"""
    def __new__(cls, name, bases, attrs):
        """Create a new component"""
        if not bases:
            # This is the base class.
            return super().__new__(cls, name, bases, attrs)

        # Get all fields of the component and all bases
        fields = {
            **{field: component
                for base in bases
                for field, component in getattr(base, "__fields__", {}).items()},
            **{field: component
                for field, component in attrs.items()
                if isinstance(component, Component)},
        }

        request_suffixes = ["Request", "Update", "Patch"]
        is_request = any(name.endswith(suffix) for suffix in request_suffixes)

        # Remove components from the class attributes
        attrs = {k: v for k, v in attrs.items() if not isinstance(v, Component)}
        attrs = {
            **attrs,
            "__fields__": fields,
            "__request_component__": is_request,
        }

        components_class = super().__new__(cls, name, bases, attrs)

        return components_class


class Component(metaclass=ComponentMeta):
    """
    An OpenAPI component.
    The component captures the invocation (args, kwargs)
    and wraps subcomponents as fields
    """
    args = []
    kwargs = {}

    init_args = []
    init_kwargs = {}

    parent_component = None
    field_name = None

    __fields__ = {}

    def __init__(self, *args, **kwargs):
        """Initialize component"""
        self.init_args = args
        self.init_kwargs = kwargs

        keys = kwargs.keys()
        if "required" in keys:
            required = kwargs["required"]
            read_only = kwargs.get("read_only", False)

            if required and "default" in keys:
                raise ValueError(
                    "A field cannot have a default value and be required at the same time")

            if required and read_only:
                raise ValueError(
                    "A field cannot be required and read_only at the same time")



        # Set parent component and field name for all fields
        for name, field in self.__fields__.items():
            field.parent_component = self
            field.field_name = name

    def __call__(self, *args, **kwargs):
        """Capture the invocation"""
        self.args = args
        self.kwargs = kwargs

        return self

    def __str__(self):
        return f"{self.__class__.__name__}({self.init_args}, {self.init_kwargs})"

    def __repr__(self):
        return self.__str__()

    def get_fields(self):
        """
        Get all fields of the component. Fields are components.
        """
        return self.__fields__
#
# Make components
#
components = [
    "PolymorphicComponent",

    # Serializers
    "Serializer",
    "ListSerializer",

    # Fields
    "CharField",
    "IntegerField",
    "FloatField",
    "BooleanField",
    "DateTimeField",
    "DateField",
    "EmailField",
    "ListField",
    "DictField",
    "EnumField",
    "JSONField",

    # Special fields
    "TimeseriesSampleField",
    "MacAddressField",
    "IpAddressField",
    "IpVersionField",
    "JWTField",
    "OuterVlanField",
    "NamedVlanField",

    # Filters
    "CharFilter",
    "BulkIdFilter",
    "NumberFilter",
    "ChoiceFilter",
    "DateTimeFilter",
    "ModelChoiceFilter",
    "ModelMultipleChoiceFilter",
]

for component in components:
    globals()[component] = type(component, (Component,), {})


class PrimaryKeyField(CharField):
    """The primary key (ID) field"""
    def __init__(self, *args, **kwargs):
        kwargs["read_only"] = True
        super().__init__(*args, **kwargs)


class PrimaryKeyRelatedField(CharField):
    """The primary key related field"""
    def __init__(self, *args, **kwargs):
        if "related" not in kwargs:
            raise ValueError("The `related` argument is required")

        super().__init__(*args, **kwargs)


class ManyRelatedField(Component):
    pass


class IpVersion(Enum):
    IPV4 = 4
    IPV6 = 6

class BgpAddressFamilies(Enum):
    IPV4 = "ipv4"
    IPV6 = "ipv6"

class AddressFamilies(Enum):
    AF_INET = "af_inet"
    AF_INET6 = "af_inet6"

class FilterSet:
    """A set of filters"""
    def __init__(self, *filters):
        self.filters = {}
        for f in filters:
            self.filters.update(f)

    def items(self):
        return self.filters.items()

    def __iter__(self):
        return iter(self.filters.items())
