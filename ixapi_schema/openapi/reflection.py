

"""
API Introspection
=================

Find all API related component classes and
create the schema from them.

Find all endpoints and enumerate paths.
"""

import importlib
import pkgutil
import inspect
import copy

from ixapi_schema.openapi import components
from ixapi_schema.openapi.generators import components_meta
from ixapi_schema.openapi.utils import text as extra_text_utils


def _get_module_relative(base, name):
    """
    Get a module relative to the base module.
    """
    module_name = f"{base.__name__}.{name}"
    return importlib.import_module(module_name)


def find_endpoints(api_base):
    """
    Find all declared API paths in the endpoints
    modules.
    """
    endpoints_base = _get_module_relative(api_base, "endpoints")
    modules = [p.name
               for p in pkgutil.walk_packages(endpoints_base.__path__)
               if not p.ispkg]

    endpoints = []
    for module_name in modules:
        try:
            module = _get_module_relative(endpoints_base, module_name)
        except ImportError as e:
            print(e)
            continue
        endpoints.append(get_endpoint_meta(module))

    return endpoints


def tag_from_path(path):
    """
    Get the tag from a path.
    By convention for now this is the first part of the path.

    :param path: A path /like/this
    """
    tokens = path.split("/")
    return [t for t in tokens if t][0]


def get_endpoint_meta(module):
    """
    Get endpoint metadata from module.
    This contains the paths and tags.
    """
    module_name = module.__name__[len(module.__package__)+1:]
    description = extra_text_utils.trim_docstring(module.__doc__)

    # Add additional information
    paths = module.PATHS
    for path, info in paths.items():
        info["module"] = module_name
        info["path"] = path
        if not info.get("tag"):
            info["tag"] = tag_from_path(path)

    return {
        "module": module_name,
        "description": description,
        "paths": paths,
    }


def find_components(api_base):
    """Find serializer classes in entities package"""
    entities_base = _get_module_relative(api_base, "entities")
    modules = [p.name for p in pkgutil.walk_packages(entities_base.__path__)
               if not p.ispkg]
    # Import all modules and add component classes
    # to the set.
    component_classes = set()
    for module in modules:
        module_name = f"{entities_base.__name__}.{module}"
        try:
            module = importlib.import_module(module_name)
        except ImportError:
            continue

        for component in _find_module_components(module):
            if getattr(component, "__hidden_in_docs__", False):
                continue
            if not component.__module__.startswith(entities_base.__name__):
                continue
            component_classes.add(component)

    # Sort components by module name
    component_classes = sorted(
        component_classes,
        key=lambda s: f"{s.__module__}.{s.__name__}")

    return component_classes


def _find_module_components(module):
    """Get components from a components collection module"""
    return [component
            for _name, component
            in inspect.getmembers(module, inspect.isclass)
            if issubclass(component, components.Component)]


def get_schemas(entities_base):
    """Get all component schemas"""
    component_classes = find_components(entities_base)
    schema = {}
    for component in component_classes:
        schema.update(components_meta.spec_from_serializer(component))

    return schema


def get_api_info(module):
    """
    Get API information, the basic structure
    for the document.
    """
    schema = _get_module_relative(module, "schema")
    return schema.SPEC
