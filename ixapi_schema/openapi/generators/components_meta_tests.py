

"""
Test getting metadata from serializer fields
"""

import enum

from ixapi_schema.openapi import components
from ixapi_schema.openapi.generators import components_meta


def test__frag_max_value():
    """Get max value from field, if present"""
    field = components.IntegerField(max_value=42)
    frag = components_meta._frag_max_value({}, field)
    assert frag["maximum"] == 42

    field = components.CharField(min_length=42)
    frag = components_meta._frag_max_value({}, field)
    assert not frag.get("maximum")


def test__frag_min_value():
    """Get min value from field, if present"""
    field = components.IntegerField(min_value=42)
    frag = components_meta._frag_min_value({}, field)
    assert frag["minimum"] == 42

    field = components.CharField(min_length=42)
    frag = components_meta._frag_min_value({}, field)
    assert not frag.get("minimum")


def test__frag_max_length():
    """Get min length from field"""
    field = components.CharField(max_length=42)
    frag = components_meta._frag_max_length({}, field)
    assert frag["maxLength"] == 42

    field = components.IntegerField(min_value=42)
    frag = components_meta._frag_max_length({}, field)
    assert not frag.get("maxLength")


def test__frag_min_length():
    """Get min length from field"""
    field = components.CharField(min_length=42)
    frag = components_meta._frag_min_length({}, field)
    assert frag["minLength"] == 42

    field = components.IntegerField(min_value=42)
    frag = components_meta._frag_min_length({}, field)
    assert not frag.get("minLength")


def test__frag_nullable():
    """Test nullable"""
    field = components.BooleanField(allow_null=True)
    frag = components_meta._frag_nullable({}, field)
    assert frag["nullable"]


def test__frag_read_only():
    """Test read only"""
    field = components.BooleanField(read_only=True, allow_null=True)
    frag = components_meta._frag_read_only({}, field)
    assert frag["readOnly"]


def test__frag_description():
    """Get description from help_text"""
    field = components.IntegerField(help_text="help_text_help")
    frag = components_meta._frag_description({}, field)
    assert frag["description"] == "help_text_help"


def test__frag_enum_field():
    """Test enum field"""
    class FooEnum(enum.Enum):
        FOO = 42
        BAR = 23

    field = components.EnumField(FooEnum)
    frag = components_meta._frag_enum_field({}, field)
    assert frag["enum"]
    assert 42 in frag["enum"]
    assert not 999 in frag["enum"]


def test__frag_pk_related_field():
    """Test pk related field frag"""
    field = components.PrimaryKeyRelatedField(
        read_only=True,
        related="Related")
    frag = components_meta._frag_pk_related_field({}, field)
    assert frag["type"] == "string"


def test__frag_pk_related_field__many():
    """Test many pk related field frag"""
    field = components.PrimaryKeyRelatedField(
        read_only=True,
        many=True,
        related="Related")
    frag = components_meta._frag_many_related_field({}, field)
    assert frag["type"] == "array"
    assert frag["items"]


def test__frag_date_time_field():
    """Test datetime field frag"""
    field = components.DateTimeField()
    frag = components_meta._frag_date_time_field({}, field)
    assert frag["type"] == "string"
    assert frag["format"]


def test__frag_json_field():
    """Test free form json field"""
    field = components.JSONField()
    frag = components_meta._frag_json_field({}, field)
    assert frag["type"] == "object"


def test__frag_boolean_field():
    """Test boolean field"""
    field = components.BooleanField()
    frag = components_meta._frag_boolean_field({}, field)
    assert frag["type"] == "boolean"


def test__frag_integer_field():
    """Test integer field"""
    field = components.IntegerField()
    frag = components_meta._frag_integer_field({}, field)
    assert frag["type"] == "integer"


def test__frag__char_field():
    """Test char field"""
    field = components.CharField()
    frag = components_meta._frag_char_field({}, field)
    assert frag["type"] == "string"


def test__frag_type():
    """Test fragment for field type"""
    fields = [
        components.IntegerField(),
        components.BooleanField(),
        components.CharField(),
        components.JSONField(),
        components.DateTimeField(),
        components.PrimaryKeyRelatedField(read_only=True, related="Related"),
        components.PrimaryKeyRelatedField(read_only=True, many=True, related="Related"),
    ]
    expected = [
        "integer", "boolean", "string", "object",
        "string", "string", "array",
    ]
    fragments = [components_meta._frag_type({}, field)["type"]
                 for field in fields]
    assert fragments == expected


#
# Integer hints
#
def test__int_format_hint():
    """Test deriving the format from bounds"""
    assert components_meta._int_format_hint(None, 23) == None
    assert components_meta._int_format_hint(23, None) == None
    assert components_meta._int_format_hint(None, None) == None

    assert components_meta._int_format_hint(0, 2**32-1) == "int64"
    assert components_meta._int_format_hint(0, 2**31-1) == "int32"
    assert components_meta._int_format_hint(-2**32, 2**31-1) == "int64"
    assert components_meta._int_format_hint(-2**31, 2**31-1) == "int32"
    assert components_meta._int_format_hint(-2**31-1, 0) == "int64"


#
# Serializers schema
#

def test_polymorphic_schemas():
    """Test generating schemas from a polymorphic serializer"""
    class FooSerializer(components.Serializer):
        a = components.CharField()

    class BarSerializer(components.Serializer):
        b = components.IntegerField()


    class PolySerializer(components.PolymorphicComponent):
        serializer_classes = {
            "foo": FooSerializer,
            "bar": BarSerializer,
        }

    schemas = components_meta.polymorphic_schemas(PolySerializer)
    assert len(schemas) == 2


def test_polymorphic_schema_mapping():
    """Test generating the schema mapping"""
    class FooSerializer(components.Serializer):
        a = components.CharField()

    class BarSerializer(components.Serializer):
        b = components.IntegerField()

    class PolySerializer(components.PolymorphicComponent):
        serializer_classes = {
            "foo": FooSerializer,
            "bar": BarSerializer,
        }

    mapping = components_meta.polymorphic_schema_mapping(PolySerializer)
    assert mapping["foo"] == "#/components/schemas/Foo"
    assert mapping["bar"] == "#/components/schemas/Bar"


def test_polymorphic_discriminator():
    """Test generating descriminator"""
    class FooSerializer(components.Serializer):
        a = components.CharField()

    class BarSerializer(components.Serializer):
        b = components.IntegerField()

    class PolySerializer(components.PolymorphicComponent):
        serializer_classes = {
            "foo": FooSerializer,
            "bar": BarSerializer,
        }

    discriminator = components_meta.polymorphic_discriminator(
        PolySerializer)
    assert discriminator["mapping"]["foo"]


def test_serializer_ref_name():
    """Test serializer ref name"""
    class EntitySerializer:
        pass

    class ExchangeFooSerializer:
        pass

    class SomeClass:
        pass

    assert components_meta.serializer_ref_name(SomeClass) == "SomeClass"
    assert components_meta.serializer_ref_name(EntitySerializer) == "Entity"
    assert components_meta.serializer_ref_name(ExchangeFooSerializer) == \
        "ExchangeFoo"


def test_serializer_title():
    """Test getting serializer title"""
    class FooSerializer:
        """test23"""

    assert components_meta.serializer_title(FooSerializer) == "Foo"


def test_serializer_description():
    """Test getting the description"""
    class FooSerializer:
        """test23"""

    assert components_meta.serializer_description(FooSerializer) == "test23"


def test_required_fieldnames():
    """Test getting a list of required field names"""
    class FooSerializer(components.Serializer):
        """Just a test serializer"""
        a = components.CharField(required=False)
        b = components.IntegerField(required=True)

    required_fields = components_meta.serializer_required_fieldnames(
        FooSerializer)
    assert "b" in required_fields
    assert not "a" in required_fields


def test_field_schemas():
    """Test getting the schema props"""
    class FooSerializer(components.Serializer):
        """Just a test serializer"""
        username = components.CharField(max_length=23)
        login_count = components.IntegerField(read_only=True)
        last_login = components.DateTimeField(read_only=True)

    properties = components_meta.serializer_properties(FooSerializer)

    assert properties['username']
    assert properties['login_count']
    assert properties['last_login']


def test_component_ref():
    """Test getting the reference for a component"""
    class MyEntity(components.Component):
        field = components.CharField()

    assert components_meta.component_ref(MyEntity()) \
        == "#/components/schemas/MyEntity"

