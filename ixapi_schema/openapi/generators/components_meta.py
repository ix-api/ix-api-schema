
"""
Generate an OpenAPI (3) representation from serializer metadata
"""

from typing import List, Mapping
import collections
import json
import base64

from ixapi_schema.openapi import components
from ixapi_schema.openapi.utils import (
    text as extra_text_utils,
    types as types_utils,
)

def spec_from_field(field):
    """
    Generate a spec fragment from a field.
    If the field is a serializer, make a reference.

    :param field: A serializer field
    """
    # This is a bit unsatisfying, but we check if it
    # is a field or a 'serializer' if the name ends with 'Field'.
    field_name = field.__class__.__name__
    if not field_name.endswith("Field"):
        return _spec_from_serializer_ref(field)


    if isinstance(field, components.TimeseriesSampleField):
        return _spec_from_timeseries_sample_field(field)
    if isinstance(
            field, (
                components.ListField,
                components.ListSerializer,
            )):
        spec = _spec_from_list_serializer(field)
        spec = _frag_example(spec, field)
        return spec


    fragments = [
        _frag_type,
        _frag_description,
        _frag_default,
        _frag_example,
        _frag_read_only,
        _frag_nullable,
        _frag_min_length,
        _frag_max_length,
        _frag_min_value,
        _frag_max_value,
    ]

    spec = {}
    for mapper in fragments:
        spec = mapper(spec, field)

    return spec


def _spec_from_serializer_ref(field):
    """
    Reference a serializer

    :param field: A serializer field
    """
    if field.init_kwargs.get("many", False):
        return _spec_from_many_serializer_ref(field)

    ref_name = serializer_ref_name(type(field))
    return {
        "$ref": f"#/components/schemas/{ref_name}"
    }


def _spec_from_many_serializer_ref(field):
    """
    Reference a serializer

    :param field: A serializer field
    """
    ref_name = serializer_ref_name(field.__class__)
    spec = {
        "type": "array",
        "items": {
            "$ref": f"#/components/schemas/{ref_name}"
        }
    }

    spec = _frag_description(spec, field)
    spec = _frag_example(spec, field)

    return spec


def _spec_from_list_serializer(field):
    """The field is a list."""
    spec = {
        "type": "array",
    }
    child = field.init_kwargs.get("child")
    if child:
        spec["items"] = spec_from_field(child)

    help_text = field.init_kwargs.get("help_text")
    if help_text:
        help_text = extra_text_utils.trim_docstring(help_text)
        spec["description"] = extra_text_utils.description_text(help_text)

    return spec


def _spec_from_timeseries_sample_field(field):
    """Timeseries sample"""
    spec = _spec_from_list_serializer(field)
    spec = _frag_example(spec, field)

    # As long as we are still on OpenAPI 3.0.0 we
    # have to do this not strictly formal
    spec["items"] = {
    # Use any type, because oneOf confuses redoc
    # for now...
    # "oneOf": [
    #        { "type": "string" },
    #        { "type": "integer" },
    #    ],
    }
    spec["minItems"] = 1

    spec["example"] = [
        "2019-08-24T14:15:22Z",
        120302, 42980203, 320302, 84990203,
    ]

    return spec


def _frag_type(frag, field):
    """
    Get field type mapping and base fragment specials
    """
    if isinstance(field, components.PrimaryKeyRelatedField):
        return _frag_pk_related_field(frag, field)
    if isinstance(field, components.CharField):
        return _frag_char_field(frag, field)
    if isinstance(field, components.IntegerField):
        return _frag_integer_field(frag, field)
    if isinstance(field, components.FloatField):
        return _frag_float_field(frag, field)
    if isinstance(field, components.BooleanField):
        return _frag_boolean_field(frag, field)
    if isinstance(field, components.JSONField):
        return _frag_json_field(frag, field)
    if isinstance(field, components.DateTimeField):
        return _frag_date_time_field(frag, field)
    if isinstance(field, components.DateField):
        return _frag_date_field(frag, field)
    if isinstance(field, components.ManyRelatedField):
        return _frag_many_related_field(frag, field)
    if isinstance(field, components.EnumField):
        return _frag_enum_field(frag, field)
    if isinstance(field, components.JWTField):
        return _frag_char_field(frag, field)
    if isinstance(field, components.MacAddressField):
        return _frag_char_field(frag, field)
    if isinstance(field, components.IpAddressField):
        return _frag_char_field(frag, field)
    if isinstance(field, components.IpVersionField):
        return _frag_integer_enum_field(frag, field, [4, 6])
    if isinstance(field, components.EmailField):
        return _frag_char_field(frag, field)
    if isinstance(field, components.ListSerializer):
        return _spec_from_list_serializer(field)
    if isinstance(field, components.OuterVlanField):
        return _frag_outer_vlan_field(frag, field)
    if isinstance(field, components.NamedVlanField):
        return _frag_named_vlan_field(frag, field)
    if isinstance(field, components.DictField):
        return _frag_dict_field(frag, field)

    raise NotImplementedError("Spec fragment missing for: {}".format(
        type(field)))


def _frag_char_field(frag, field):
    """Fragment for a char field"""
    frag["type"] = "string"

    format_hint = _string_format_hint(field)
    if format_hint:
        frag["format"] = format_hint

    return frag


def _frag_integer_field(frag, field):
    """Fragment for integer field"""
    frag["type"] = "integer"
    min_value = field.init_kwargs.get("min_value", None)
    max_value = field.init_kwargs.get("max_value", None)
    int_type = _int_format_hint(min_value, max_value)
    if int_type:
        frag["format"] = int_type
    return frag

def _frag_float_field(frag, field):
    """Fragment for numeric / float field"""
    frag["type"] = "number"
    return frag

def _frag_float_field(frag, field):
    """Fragment for float fields"""
    frag["type"] = "number"
    return frag

def _frag_integer_enum_field(frag, field, values):
    """Fragment for integeer field"""
    frag["type"] = "integer"
    frag["enum"] = values
    return frag

def _frag_boolean_field(frag, field):
    """Fragment for boolean"""
    frag["type"] = "boolean"
    return frag

def _frag_json_field(frag, field):
    """Fragment for json field"""
    frag["type"] = "object"
    return frag

def _frag_date_time_field(frag, field):
    """Fragment for date time field"""
    frag["type"] = "string"
    frag["format"] = "date-time"
    return frag

def _frag_date_field(frag, field):
    """Fragment for date time field"""
    frag["type"] = "string"
    frag["format"] = "date"
    return frag

def _frag_pk_related_field(frag, field):
    """Fragment for primary key related field"""
    many = field.init_kwargs.get("many", False)
    if many:
        frag["type"] = "array"
        frag["items"] = {
            "type": "string",
        }
        return frag

    # Otherwise this is a plain reference
    frag["type"] = "string"
    return frag


def _frag_many_related_field(frag, field):
    """Fragment for primary key related field"""
    frag["type"] = "array"
    frag["items"] = {
        "type": "string",
    }
    return frag


def _frag_enum_field(frag, field):
    """Fragment for enum field"""
    enum_class = field.init_args[0]
    values = [e.value for e in enum_class]
    if not values:
        return frag

    frag["type"] = "string"
    if not isinstance(values[0], str):
        frag["type"] = "integer"
    frag["enum"] = values
    return frag


def _frag_named_vlan_field(frag, field):
    """Fragment for named vlan fields"""
    frag["anyOf"] = [
        {
            "type": "array",
            "items": {
                "anyOf": [
                    {"type": "integer"},
                    {"type": "string"},
                ],
            },
        },
        {
            "type": "integer",
        }
    ]
    return frag

def _frag_dict_field(frag, field):
    """Fragment for dict fields"""
    child = field.init_kwargs["child"]
    frag["type"] = "object"
    frag["additionalProperties"] = spec_from_field(child)

    return frag

def _frag_outer_vlan_field(frag, field):
    """Fragment for outer vlan field"""
    frag["type"] = "array"
    frag["items"] = {}
    frag["minLength"] = 2
    frag["maxLength"] = 2

    return frag

#
# Field helpers
#
def _example_from_field(field):
    """Gets the example from the field's help_text"""
    help_text = field.init_kwargs.get("help_text")
    if not help_text:
        return None

    # Examples can be provided in the serializer class
    # using a `__<field_name>_example__` attribute.
    parent = field.parent_component
    if parent:
        example_prop = f"__{field.field_name}_example__"
        example = getattr(parent, example_prop, None)
        if example:
            return example


    example_text = extra_text_utils.example_text(
        extra_text_utils.trim_docstring(help_text))

    try:
        example = json.loads(example_text)
    except:
        example = example_text

    return example

#
# Int format helper
#
def _int_format_hint(min_value, max_value):
    """
    Calculate the integer format when min and max
    values are present.
    """
    # We only do this with explict bounds
    if min_value is None or max_value is None:
        return None

    # Get min / max values for signed integers
    max_signed_int32 = 2**31-1
    min_signed_int32 = -2**31
    max_signed_int64 = 2**63-1
    min_signed_int64 = -2**63

    # Preconditions: As the swagger spec explicitly
    # states that there are no unsigned integers, we 
    # have to fit this into the signed integer range.
    if max_value > max_signed_int64:
        return None
    if min_value < min_signed_int64:
        return None

    if max_value > max_signed_int32:
        return "int64"
    if min_value < min_signed_int32:
        return "int64"

    # I guess we can fit it in an int32
    return "int32"

#
# String format helper
#
def _string_format_hint(field):
    """
    If the example starts with http(s), we assume the
    field's format to be 'uri'.
    """
    example = _example_from_field(field)
    if not example:
        return None
    if isinstance(example, str) and example.startswith("http"):
        return "uri"
    return None

#
# Commons
#
def _generate_primary_key_example(field, name=None):
    """
    Generate an example primary key
    """
    # Encode name to base64
    if not name:
        name = field.parent_component.__class__.__name__

    name += ":42"

    return base64.b64encode(bytes(name, "utf-8")).decode("utf-8")


def _frag_primary_key_description(frag, field):
    """Get the description for a primary key field"""
    # Generate the primary key description from
    # the parent component.
    parent = field.parent_component
    parent_name = component_title(parent)
    help_text = f"The *primary identifier* of the `{parent_name}`."
    frag["description"] = help_text
    frag["example"] = _generate_primary_key_example(field)

    return frag


def _frag_primary_key_related_description(frag, field):
    """Get the description for a primary key related field"""
    related = field.init_kwargs["related"]
 
    many = field.init_kwargs.get("many", False)
    if many:
        description = "A list of `id`s of the related `{}`.\n\n".format(related)
        example = [
            _generate_primary_key_example(field, name=related)
        ]
    else:
        description = "The `id` of the related `{}`.\n\n".format(related)
        example = _generate_primary_key_example(field, name=related)

    if not frag.get("description"):
        frag["description"] = description

    if not frag.get("example"):
        frag["example"] = example

    return frag


def _frag_description(frag, field):
    """Get description from field's help_text"""
    if isinstance(field, components.PrimaryKeyField):
        return _frag_primary_key_description(frag, field)

    help_text = field.init_kwargs.get("help_text")
    if help_text:
        help_text = extra_text_utils.trim_docstring(help_text)
        frag["description"] = extra_text_utils.description_text(help_text)

    if isinstance(field, components.PrimaryKeyRelatedField):
        return _frag_primary_key_related_description(frag, field)

    return frag


def _frag_default(frag, field):
    """If the field as a default value set"""
    if not "default" in field.init_kwargs:
        return frag

    default = field.init_kwargs["default"]

    if types_utils.is_json_serializable(default):

        # In case the default value is the 'null' type,
        # openapi seems to see the field rather ommitted, than set
        # to null as default. Does this makes sense?!
        # However, setting default to null on a nullable
        # field leads to a type linting error: "expected 'string'
        # to be 'object'" where 'string' is referencing the field's
        # type.
        if default is None and field.init_kwargs.get("allow_null"):
            return frag

        frag["default"] = default

    return frag


def _frag_example(frag, field):
    """Get example from field's help_text"""
    example = _example_from_field(field)
    if example:
        frag["example"] = example

    return frag


def _frag_read_only(frag, field):
    """Get read only property"""
    read_only = field.init_kwargs.get("read_only", None)
    if read_only is None:
        return frag

    if read_only:
        frag["readOnly"] = True

    return frag


def _frag_nullable(frag, field):
    """Is nullable?"""
    allow_null = field.init_kwargs.get("allow_null", None)
    if allow_null is None:
        return frag

    if allow_null:
        frag["nullable"] = True

    return frag


def _frag_min_length(frag, field):
    """
    Update schema fragment with min length.

    :param frag: The current shard
    :param field: A serializer field
    """
    min_length = field.init_kwargs.get("min_length", None)
    if min_length is not None:
        frag["minLength"] = min_length
    return frag


def _frag_max_length(frag, field):
    """
    Update schema fragment with max length.

    :param frag: The current shard
    :param field: A serializer field
    """
    max_length = field.init_kwargs.get("max_length", None)
    if max_length is not None:
        frag["maxLength"] = max_length
    return frag


def _frag_min_value(frag, field):
    """Optional min value fragment"""
    min_value = field.init_kwargs.get("min_value", None)
    if min_value is not None:
        frag["minimum"] = min_value
    return frag


def _frag_max_value(frag, field):
    """Optional max value fragment"""
    max_value = field.init_kwargs.get("max_value", None)
    if max_value is not None:
        frag["maximum"] = max_value
    return frag


def spec_from_serializer(serializer):
    """
    Like the above, as we can not just override
    the 'required' property in an allOf: inheritance,
    as required may not be an empty array.
    """
    ref = serializer_ref_name(serializer)
    if ref.endswith("Base"):
        return {} # By convention we can skipt base serializers
    if issubclass(serializer, components.PolymorphicComponent):
        return _spec_from_polymorphic_serializer(serializer)


    # Generate normal serializer spec
    serializer_spec = {
        "title": serializer_title(serializer),
        "type": "object",
        "description": serializer_description(serializer),
        "properties": serializer_properties(serializer),
    }

    # Eventhough this is debatable if this is sensible
    # OpenAPI 3.x.x needs the polymorphic discriminator property
    # to be required.
    instance = serializer()
    polymorphic = getattr(instance, "__polymorphic__", None)
    polymorphic_pname = getattr(
        instance, "__polymorphic_on__", "type")

    if serializer_spec["properties"].get(polymorphic_pname):
        # Remove polymorphic property
        del serializer_spec["properties"][polymorphic_pname]

    if polymorphic:
        serializer_spec["allOf"] = [
            {"$ref": f"#/components/schemas/{polymorphic}"},
        ]

    is_partial = ref.endswith("Patch")

    # Set required array only if not empty
    required_fields = serializer_required_fieldnames(serializer)
    if required_fields and not is_partial:
        serializer_spec["required"] = required_fields

    if serializer_spec.get("required"):
       serializer_spec["required"].reverse()


    # But this has no required array.
    return {
        ref: serializer_spec,
    }


def polymorphic_serializer_types(serializer):
    """Get types from discriminator"""
    discriminator = polymorphic_discriminator(serializer)
    return list(discriminator["mapping"])


def polymorphic_serializer_title(serializer):
    """Get title for polymorphic serializer"""
    return serializer.__name__


def _spec_from_polymorphic_serializer(serializer):
    """Create polymorphic serializer spec"""
    subschemas = polymorphic_schemas(serializer)
    if not subschemas:
        return {} # This is not required

    title = serializer_title(serializer)
    description = polymorphic_serializer_title(serializer)

    discriminator = polymorphic_discriminator(serializer)


    property_name = getattr(
        serializer, "__polymorphic_on__", "type")
    spec = {
        serializer_ref_name(serializer): {
            "title": title,
            "description": description,
            "type": "object",
            "properties": {
                property_name: {
                    "type": "string",
                },
            },
            "required": [property_name],
            "discriminator": discriminator,
        }
    }

    return spec


def _polymorphic_base_ref(instance):
    """
    The serializer instance should have an (inherited) attribute
    __polymorphic__ indicating the base serializer class.
    """
    base = getattr(instance, "__polymorphic__")
    return f"#/components/schemas/{base}"


def polymorphic_schemas(serializer):
    """Get all subschema references"""
    schema_refs = [{
            "$ref": "#/components/schemas/" + serializer_ref_name(ser),
        } for _, ser in serializer.serializer_classes.items()
        if not getattr(ser, "__hidden_in_docs__", False)]


    return schema_refs


def polymorphic_schema_mapping(serializer):
    """Get polymorphic schema mapping"""
    mapping = {
        type_name: "#/components/schemas/" + serializer_ref_name(ser)
        for type_name, ser in serializer.serializer_classes.items()
        if not getattr(ser, "__hidden_in_docs__", False)
    }

    return mapping


def polymorphic_discriminator(serializer):
    """Create discriminator mapping"""
    property_name = getattr(
        serializer, "__polymorphic_on__", "type")
    discriminator = {
        "propertyName": property_name,
        "mapping": polymorphic_schema_mapping(serializer),
    }

    return discriminator


def serializer_ref_name(serializer) -> str:
    """
    Get entities reference name for a serializer.
    """
    # Convention: <Entity>Serializer
    ref_name = serializer.__name__.replace("Serializer", "")
    return ref_name


def component_ref(component) -> str:
    """Get a reference to the component"""
    component_cls = component.__class__
    ref_name = serializer_ref_name(component_cls)

    return f"#/components/schemas/{ref_name}"


def component_title(component) -> str:
    """Get the title from the instanciated component"""
    doc = component.__doc__
    if doc:
        return extra_text_utils.trim_docstring(doc)

    cls = component.__class__
    if issubclass(cls, components.PolymorphicComponent):
        return polymorphic_serializer_title(cls)

    return serializer_title(cls)


def serializer_title(serializer) -> str:
    """Get the title from the serializer"""
    return serializer.__name__.replace("Serializer", "")


def serializer_description(serializer) -> str:
    """Get the description of the serializer"""
    # For now, just use the docstring.
    return extra_text_utils.trim_docstring(serializer.__doc__)



def _is_field_required(field):
    """Check if the field is required"""
    # Explicity set required has precedence
    if field.init_kwargs.get("required") is not None:
        return field.init_kwargs["required"]

    if field.init_kwargs.get("allow_null"):
        return False
    if field.init_kwargs.get("allow_blank"):
        return False
    if field.init_kwargs.get("default") is not None:
        return False

    return True



def serializer_required_fieldnames(serializer) -> List[str]:
    """
    Get all required fields from the serializer.
    """
    instance = serializer()
    fields = [name for name, field in instance.get_fields().items()
              if _is_field_required(field)]

    polymorphic_pname = getattr(
        instance, "__polymorphic_on__", "type")
    if getattr(instance, "__polymorphic_type__", False):
        fields.append(polymorphic_pname)

    return fields


def serializer_properties(serializer) -> Mapping[str, dict]:
    """
    Get all field schemas using the serializer fields
    meta information.
    """
    instance = serializer()
    properties = {
        name: spec_from_field(field)
        for name, field in instance.get_fields().items()
    }

    polymorphic_type = getattr(instance, "__polymorphic_type__", False)
    if polymorphic_type:
        polymorphic_pname = getattr(
            instance, "__polymorphic_on__", "type")
        # Use an ordered dict to sort the type to the beginning
        poly_properties = collections.OrderedDict({
            polymorphic_pname: {
                "type": "string",
                "example": polymorphic_type,
            }
        })
        poly_properties.update(properties)
        # Prevent yaml from adding a OrderedDict tag.
        # Fingers crossed, that type stays at the top.
        properties = dict(poly_properties)

    return properties

