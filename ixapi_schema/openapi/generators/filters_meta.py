
"""
Extract query parameters from filterset
"""
import enum
import json

from ixapi_schema.openapi import components
from ixapi_schema.openapi.utils import (
    text as extra_text_utils,
)


def frag_from_filter(name, field):
    """Get filter field schema fragment"""
    frag = {
        "name": name,
        "schema": _schema_from_filter(field),
        "required": False, # We only have optional filters
        "in": "query",
        "description": f"Filter by {name}",
    }

    # Extract description and example
    label = field.init_kwargs.get("label")
    if label is not None:
        label = extra_text_utils.trim_docstring(label)
        frag["description"] = extra_text_utils.description_text(label)

        # Extract example from docstring
        example_text = extra_text_utils.example_text(label)
        try:
            example = json.loads(example_text)
        except:
            example = example_text

        if example:
            frag["example"] = example

    if isinstance(field, components.BulkIdFilter):
        frag["style"] = "form"
        frag["explode"] = False

    return frag


def _schema_from_filter(field):
    """Get schema from filter field"""
    if isinstance(field, components.NumberFilter):
        return {
            "type": "integer",
        }
    if isinstance(field, components.DateTimeFilter):
        return {
            "type": "string",
            "format": "date-time",
        }
    if isinstance(field, components.CharFilter):
        return {
            "type": "string",
        }
    if isinstance(field, components.ModelMultipleChoiceFilter):
        return {
            "type": "string",
            "example": "id1,id2",
        }
    if isinstance(field, components.ModelChoiceFilter):
        return {
            "type": "string",
        }
    if isinstance(field, components.ChoiceFilter):
        choices = field.init_args[0]
        return {
            "type": _choices_type(choices),
            "enum": _choices_enum(choices),
        }
    if isinstance(field, components.BulkIdFilter):
        return {
            "type": "array",
            "items": {
                "type": "string",
            },
            "example": "id1,id2,id3",
        }

    raise NotImplementedError(
        "Schema not implemented for filter type: {}".format(
            type(field)))


def _choices_type(choices):
    """Derive the choices type from the enum value"""
    value = choices[0][0]
    if isinstance(value, enum.Enum):
        value = value.value

    if isinstance(value, str):
        return "string"

    return "integer"


def _choices_enum(choices):
    """Get choices values"""
    return [_unpack_choice_value(c) for c in choices]


def _unpack_choice_value(value):
    """Unpack value"""
    if isinstance(value, tuple):
        value = value[0]
    if isinstance(value, enum.Enum):
        return value.value

    return value
