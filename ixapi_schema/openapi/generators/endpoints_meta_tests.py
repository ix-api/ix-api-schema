"""
Test endpoints schema generator
"""

import pytest

from ixapi_schema.openapi import components
from ixapi_schema.openapi.generators import endpoints_meta
from ixapi_schema.v1.entities import problems


def test_format_path():
    """Test openapi path formatting"""
    path = "/some/<param:int>/path/<foo:str>"
    expected = "/some/{param}/path/{foo}"
    assert endpoints_meta.format_path(path) == expected

def test_encode_path_params():
    """Test getting the path param schema"""
    path = "/some/path/<foo:int>/<bar:str>"
    params = endpoints_meta.encode_path_params(path)
    assert len(params) == 2
    assert params[0]["name"] == "foo"


def test_encode_path_type():
    """Test encoding of the path type"""
    assert endpoints_meta.encode_path_type("int") == "integer"
    assert endpoints_meta.encode_path_type("str") == "string"

    with pytest.raises(ValueError):
        endpoints_meta.encode_path_type("foo")

# Endpoints: Paths
def test_get_operation():
    """Test getting an operation by name"""
    endpoint = {
        "Get": True,
        "POST": True,
        "put": True,
    }

    assert endpoints_meta.get_operation(endpoint, "get")
    assert endpoints_meta.get_operation(endpoint, "pOsT")
    assert endpoints_meta.get_operation(endpoint, "PUT")
    assert not endpoints_meta.get_operation(endpoint, "delete")


#
# Endpoints: Tags
#
def test_encode_tags():
    """Test getting tags"""
    endpoints = [
        {
            "paths": {
                "/foo": {
                    "tag": "foo",
                    "description": "desc1",
                },
            },
        },
        {
            "paths": {
                "/foobar": {
                    "tag": "bar",
                    "description": "desc2",
                }
            }
        }
    ]

    # Tags are sorted by name, so bar is before foo
    tags = [
        {"name": "bar", "description": "desc2"},
        {"name": "foo", "description": "desc1"},
    ]

    assert endpoints_meta.encode_tags(endpoints) == tags


def test_get_operation_id():
    """Test getting the op id"""
    endpoint = {
        "tag": "facilities",
    }

    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities", "get") \
        == "facilities_list"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities/<pk:str>", "get") \
        == "facilities_read"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities", "post") \
        == "facilities_create"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities/<pk:str>", "delete") \
        == "facilities_destroy"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities/<pk:str>", "put") \
        == "facilities_update"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities/<pk:str>", "patch") \
        == "facilities_partial_update"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities/<pk:str>", "options") \
        == "facilities_meta"
    assert endpoints_meta.get_operation_id(
        endpoint, "/facilities", "options") \
        == "facilities_collection_meta"


def test_merge_content_types():
    """Test merging content type dicts"""
    responses = [
        {
            "description": "test",
            "foo": "42",
            "content": {
                "application/json": "JSON",
            },
        },
        {
            "content": {
                "application/pdf": "PDF",
            },
            "bar": 23,
        },
    ]

    res = endpoints_meta.merge_content_types(responses)
    assert res["description"] == "test"
    assert res["foo"] == "42"
    assert res["content"]["application/json"] == "JSON"
    assert res["content"]["application/pdf"] == "PDF"


def test_encode_component():
    """Test encoding a response component"""
    class Entity(components.Component):
        """Entity"""
        field = components.CharField()

    fragment = endpoints_meta.encode_component(Entity())
    assert fragment, "should generate a fragment"
    assert fragment["content"]["application/json"]["schema"]


def test_encode_component_content_type_override():
    """Test encoding a response component"""
    class Entity(components.Component):
        """Entity"""
        field = components.CharField()

    ctype = "x-application/json+custom"
    fragment = endpoints_meta.encode_component(
        Entity(), content_type=ctype)
    assert fragment["content"][ctype]["schema"]


def test_encode_problem_response():
    """Test problem response encoding"""
    error_base = "http://localhost:8000/"
    group = [
        problems.AuthenticationProblem(),
        problems.SignatureExpiredProblem(),
    ]

    fragment = endpoints_meta.encode_problem_response_group(
        error_base, group)
    assert fragment


def test_encode_operation_responses():
    """Test operation encoding"""
    class Entity(components.Component):
        """Entity"""
        field = components.CharField()
    api = {
        "error_documents_base_url": "http://localhorst:4242",
    }
    op = {
        "responses": {
            200: Entity(),
        },
        "problems": [
            problems.ServerErrorProblem(),
        ],
    }
    fragment = endpoints_meta.encode_operation_responses(api, op)
    assert fragment


def test_encode_filter_params():
    """Test filter param encoding"""
    op = {
        "filters": {
            "id": components.BulkIdFilter(),
            "name": components.CharFilter(),
        }
    }
    fragment = endpoints_meta.encode_filter_params(op)
    assert fragment
    print(fragment)


