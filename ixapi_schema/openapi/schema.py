
"""
OpenAPI 3 schema generator from serializers and
filters metadata.
"""

from copy import deepcopy
from ixapi_schema.openapi import reflection
from ixapi_schema.openapi.generators import (
    endpoints_meta,
)


def generate(api_base):
    """
    Generate the schema by providing the source module.

    The base structure is read from <module>.schema.SPEC,
    the paths are populated from modules in <module>.enpoints.*
    and the entity schemas are genrated from the serializer
    classes in <module>.entities.*

    :param api_base: A valid python module containing a schema.
        Example: ixapi_schema.v1
    """
    # Get API information and base schema
    info = reflection.get_api_info(api_base)
    endpoints = reflection.find_endpoints(api_base)
    schemas = reflection.get_schemas(api_base)

    info = deepcopy(info)

    # Get API endpoints
    tags = endpoints_meta.encode_tags(endpoints)
    paths = endpoints_meta.encode_paths(info, endpoints)

    components_overrides = info.get("components", {})

    # Delete non standard fields and overrides
    del info["error_documents_base_url"]

    if "components" in info.keys():
        del info["components"]

    # Build OpenAPI 3 document
    doc = {
        "openapi": "3.0.0",
        "info": {
            "version": "unknown",
            "title": "",
            "description": "",
            "contact": {
                "url": "https://ix-api.net",
                "email": "team@ix-api.net",
            },
        },
        "paths": paths,
        "components": {
            "schemas": schemas,
        },
        "tags": tags,
    }
    doc.update(info) # Add authorship and server info
    doc["components"].update(components_overrides)

    return doc
