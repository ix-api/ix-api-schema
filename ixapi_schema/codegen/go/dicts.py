
from collections.abc import Mapping


def merge(*dicts):
    """Merge a list of dicts"""
    res = {}
    for dic in dicts:
        res = merge_dicts(res, dic)
    return res


def merge_lists(l1, l2):
    """Merge lists"""
    return l1 + l2 # We don't care about duplicates for now


def merge_dicts(d1, d2):
    """Merge dicts"""
    for k, v in d2.items():
        if isinstance(v, Mapping):
            d1[k] = merge_dicts(d1.get(k, {}),  v)
        elif isinstance(v, list):
            d1[k] = merge_lists(d1.get(k, []), v)
        else:
            d1[k] = v
    return d1

