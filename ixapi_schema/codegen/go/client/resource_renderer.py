
import subprocess

from copy import copy

from ixapi_schema.codegen.go.client.struct_renderer import (
    render_struct_attr,
)
from ixapi_schema.codegen.go.text import (
    idiomize,
    camelize,
    comment_newlines,
)
from ixapi_schema.codegen.go.tmpl import (
    render,
)

# Override success type for operation:
# Return: ( success_type, success_serializer, is_array )
SUCCESS_TYPE_OVERRIDES = {
    "product_offerings_list": ("[]ProductOffering", "ProductOffering", True),
}

EXCLUDE_OPERATIONS = [
    "ports_statistics_read",
    "ports_statistics_timeseries_read",
    "connections_statistics_read",
    "connections_statistics_timeseries_read",
    "network_service_configs_statistics_read",
    "network_service_configs_statistics_timeseries_read",
    "network_services_statistics_read",
    "network_services_statistics_timeseries_read",
]

POLYMORPHIC_TYPES = [
    "MemberJoiningRuleRequest",
    "MemberJoiningRulePatch",
    "MemberJoiningRuleUpdate",
    "NetworkServiceConfigRequest",
    "NetworkServiceConfigPatch",
    "NetworkServiceConfigUpdate",
    "NetworkServiceRequest",
    "NetworkServicePatch",
    "NetworkServiceUpdate",
    "NetworkFeatureConfigRequest",
    "NetworkFeatureConfigUpdate",
    "NetworkFeatureConfigPatch",
]


DECODE_EXTRA_VLAN_CONFIG = """
    vlanConfig, err := decodeVLANConfig(res.VLANConfigRaw)
    if err != nil {
        return nil, err
    }
    res.VLANConfig = vlanConfig
"""

DECODE_EXTRA = {
    "NetworkServiceConfig": DECODE_EXTRA_VLAN_CONFIG,
}

DECODE_EXTRA_VLAN_CONFIG_ARRAY = """
    vlanConfig, err := decodeVLANConfig(rval.VLANConfigRaw)
    if err != nil {
        return nil, err
    }
    rval.VLANConfig = vlanConfig
"""

DECODE_EXTRA_ARRAY = {
    "NetworkServiceConfig": DECODE_EXTRA_VLAN_CONFIG_ARRAY,
}

TMPL_RESOURCE = """
// {{OPERATION}} {{DESCRIPTION}}
func (c *Client) {{OPERATION}}(
    ctx context.Context,
    {{REQUEST_PARAM}}
    {{QUERY_PARAM}}
) ({{SUCCESS_TYPE}}, error) {
    {{BLOCK_MAKE_REQUEST}}

	// Set request headers
	for k, v := range c.header {
		hreq.Header.Set(k, v[0])
	}
	ret, err := c.Do(hreq)
	if err != nil {
		return nil, err
	}
	defer ret.Body.Close()
	body, err := io.ReadAll(ret.Body)
	if err != nil {
		return nil, err
	}

    // Success
    if ret.StatusCode <= http.StatusAccepted {
      {{BLOCK_DECODE_SUCCESS}}
    }

    // Decode error 404
    if ret.StatusCode == http.StatusNotFound {
        res := &NotFoundError{}
        if err := json.Unmarshal(body, res); err != nil {
            return nil, err
        }
        res.Status = ret.StatusCode // implementations are not reliable
        return nil, res
    }

	// Decode error 403
	if ret.StatusCode == http.StatusForbidden {
		res := &AuthenticationError{}
		if err := json.Unmarshal(body, res); err != nil {
			return nil, err
		}
        res.Status = ret.StatusCode
		return nil, res
	}
	// Decode error 401
	if ret.StatusCode == http.StatusUnauthorized {
		res := &AuthenticationError{}
		if err := json.Unmarshal(body, res); err != nil {
			return nil, err
		}
        res.Status = ret.StatusCode
		return nil, res
	}
	// Decode error 400
	if ret.StatusCode == http.StatusBadRequest {
		res := &ValidationError{}
		if err := json.Unmarshal(body, res); err != nil {
			return nil, err
		}
        res.Status = ret.StatusCode
		return nil, res
	}

	// Decode as generic error
	res := &APIError{}
	if err := json.Unmarshal(body, res); err != nil {
		return nil, err
	}
    res.Status = ret.StatusCode
	return nil, res
}
"""

TMPL_REQUEST_WITH_JSON_BODY = """
    {{URL}}

	data, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	hreq, err := http.NewRequestWithContext(
		ctx, {{METHOD}}, url, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
    hreq.Header.Set("Content-Type", "application/json")
"""

TMPL_REQUEST_WITH_RAW_BODY = """
    {{URL}}
	hreq, err := http.NewRequestWithContext(
		ctx, {{METHOD}}, url, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
    hreq.Header.Set("Content-Type", "application/octet-stream")
"""


TMPL_REQUEST = """
    {{URL}}

	hreq, err := http.NewRequestWithContext(
		ctx, {{METHOD}}, url, nil)
	if err != nil {
		return nil, err
	}
"""

TMPL_DECODE = """
    res := {{SUCCESS_TYPE}}{}
    if err := json.Unmarshal(body, res); err != nil {
        return nil, err
    }
    return res, nil
"""

TMPL_DECODE_ARRAY = """
    res := {{SUCCESS_TYPE}}{}
    if err := json.Unmarshal(body, &res); err != nil {
        return nil, err
    }
    return res, nil
"""

TMPL_DECODE_POLYMORPHIC = """
    tmp := &Polymorphic{{SUCCESS_TYPE}}{}
    if err := json.Unmarshal(body, tmp); err != nil {
        return nil, err
    }
    ptype := tmp.PolymorphicType()
    switch(ptype) {
        {{POLYMORPHIC_SWITCH}}
    }

    return nil, ErrInvalidPolymorphicType
"""

TMPL_POLYMORPHIC_CASE = """
    case {{POLYMORPHIC_TYPE}}Type:
        res := &{{POLYMORPHIC_TYPE}}{}
        if err := json.Unmarshal(body, res); err != nil {
            return nil, err
        }
        {{DECODE_EXTRA}}
        return res, nil
"""

TMPL_DECODE_POLYMORPHIC_ARRAY = """
    msgs := []json.RawMessage{}
    if err := json.Unmarshal(body, &msgs); err != nil {
        return nil, err
    }
    res := make([]{{SUCCESS_TYPE}}, 0, len(msgs))

    for _, msg := range msgs {
        tmp := &Polymorphic{{SUCCESS_TYPE}}{}
        if err := json.Unmarshal(msg, tmp); err != nil {
            return nil, err
        }
        ptype := tmp.PolymorphicType()
        switch(ptype) {
            {{POLYMORPHIC_SWITCH}}
        }
    }

    return res, nil
"""

TMPL_POLYMORPHIC_ARRAY_CASE = """
    case {{POLYMORPHIC_TYPE}}Type:
        rval := &{{POLYMORPHIC_TYPE}}{}
        if err := json.Unmarshal(msg, rval); err != nil {
            return nil, err
        }
        {{DECODE_EXTRA}}
        res = append(res, rval)
"""

RESOURCES_PRELUDE = """package ixapi

// This file is generated from the IX-API schema.
// DO NOT EDIT.

// Errors
var (
    // ErrInvalidPolymorphicType is raised when a polymorphic type could
    // not be resolved.
    ErrInvalidPolymorphicType = errors.New("unknown polymorphic type")
)

"""


def render_resources_from_paths(components, paths):
    """Render resources functions from path"""
    resources = [
        render_resource_op(components, resource, op, method)
        for resource, ops in paths.items()
        for method, op in ops.items()]
    return resources


def write_resources_to_file(components, paths, dst):
    """Write components"""
    resources = render_resources_from_paths(components, paths)
    with open(dst, "w+") as f:
        f.write(RESOURCES_PRELUDE)
        for res in resources:
            f.writelines(res)

    # Invoke goimports on file
    subprocess.run(["goimports", "-w", dst])


def render_resource_op(components, resource, op, method):
    """
    Render a single resource operation
    """
    op_id = op["operationId"]

    if op_id in EXCLUDE_OPERATIONS:
        return []

    func = idiomize(camelize(op_id))
    filters_header = _filters_header_from_func(func, op)

    buf = []

    # Render filter type if present
    params = _query_params_from_op(op)
    if len(params) > 0:
        buf += _render_query_params_struct(func, params)


    request_type, _ = _operation_request_type(op)
    success_type, _, _ = _operation_success_type(components, op)

    # st = _operation_success_type(components, op)
    # print("OP:", op["operationId"], "ST:", st)

    make_request = _operation_request(resource, method, op)
    decode_response = _operation_decode(components, op)

    request_param = ""
    if "{id}" in resource:
        request_param = "id string,"
    request_param += request_type

    buf += [render(
        TMPL_RESOURCE,
        operation=func,
        description=comment_newlines(op["description"]),
        query_param=filters_header,
        request_param=request_param,
        success_type=success_type,
        block_make_request=make_request,
        block_decode_success=decode_response,
    )]

    return buf


def _operation_decode(components, op):
    """Decode success"""
    _, success_serializer, array = _operation_success_type(components, op)

    component = components.get(success_serializer)
    if not component:
        return "return body, nil"

    success_type = idiomize(success_serializer)
    if array:
        success_var = "[]*" + success_type
    else:
        success_var = "&" + success_type

    # Is polymorphic?
    disc = component.get("discriminator")
    if disc and array:
        return _operation_decode_polymorphic_array(component, success_type)
    if disc:
        return _operation_decode_polymorphic(component, success_type)

    if array:
        return render(
            TMPL_DECODE_ARRAY,
            success_type=success_var)

    return render(
        TMPL_DECODE,
        success_type=success_var)


def _operation_decode_polymorphic(component, success_type):
    """decode polymorphic"""
    pswitch = ""
    decode_extra=DECODE_EXTRA.get(success_type, "")

    for c in component.get("oneOf", []):
        ref = c["$ref"]
        ptype = idiomize(_ref_to_type(ref))
        pswitch += render(
            TMPL_POLYMORPHIC_CASE,
            decode_extra=decode_extra,
            polymorphic_type=ptype)

    return render(
        TMPL_DECODE_POLYMORPHIC,
        decode_extra=decode_extra,
        polymorphic_switch=pswitch,
        success_type=success_type)


def _operation_decode_polymorphic_array(component, success_type):
    pswitch = ""
    decode_extra=DECODE_EXTRA_ARRAY.get(success_type, "")
    for c in component.get("oneOf", []):
        ref = c["$ref"]
        ptype = idiomize(_ref_to_type(ref))
        pswitch += render(
            TMPL_POLYMORPHIC_ARRAY_CASE,
            decode_extra=decode_extra,
            success_type=success_type,
            polymorphic_type=ptype)

    return render(
        TMPL_DECODE_POLYMORPHIC_ARRAY,
        polymorphic_switch=pswitch,
        success_type=success_type)


def _operation_request(resource, method, op):
    """Make request in op"""
    url = _operation_url(resource, op)
    has_body = op.get("requestBody")
    body_content = op.get("requestBody", {}).get("content", {})
    body_type = "json"
    if body_content.get("application/octet-stream"):
        body_type = "raw"

    http_method = _http_method(method)

    if has_body and body_type == "raw":
        return render(
            TMPL_REQUEST_WITH_RAW_BODY,
            url=url,
            method=http_method)

    if has_body:
        return render(
            TMPL_REQUEST_WITH_JSON_BODY,
            url=url,
            method=http_method)

    return render(
        TMPL_REQUEST,
        url=url,
        method=http_method)


def _http_method(method):
    """Get http.Method..."""
    return "http.Method" + method.title()


def _operation_url(resource, op):
    """get url part"""
    has_body = op.get("requestBody")
    has_query = len(_query_params_from_op(op)) > 0

    url = ""
    url += "params := \"\"\n"


    if has_query:
        url += "if len(qry) > 0 && qry[0] != nil {\n params = qry[0].RawQuery()\n}\n\n"

    url += "if params != \"\" {\n"
    url += "  params = \"?\" + params\n"
    url += "}\n\n"

    if "{id}" in resource:
        url += f"url := c.resourceURL(\"{resource}\"+params, id)"
    else:
        url += f"url := c.resourceURL(\"{resource}\"+params)"

    return url



def _ref_to_type(ref):
    r = ref.split("/")
    return r[len(r)-1]


def _operation_success_type(components, op):
    """Get the success type: (type, serializer, is_array)"""
    override = SUCCESS_TYPE_OVERRIDES.get(op["operationId"])
    if override:
        return override

    schema = op.get("responses", {}).get(200)
    if not schema:
        schema = op.get("responses", {}).get(201)
    if not schema:
        schema = op.get("responses", {}).get(202)
    if not schema:
        return "", None, False
    schema = schema.get("content", {}).get("application/json", {}).get("schema", {})

    stype = schema.get("type")
    if stype and stype == "array":
        s = _ref_to_type(schema["items"]["$ref"])
        t = idiomize(s)
        c = components.get(s)
        if c and c.get("discriminator"):
            return f"[]{t}", s, True

        return f"[]*{t}", s, True

    ref = schema.get("$ref")
    if not ref:
        return "Response", None, False

    s = _ref_to_type(ref)
    t = idiomize(s)
    c = components.get(s)
    if c and c.get("discriminator"):
        return t, s, False

    return f"*{t}", s, False


def _operation_request_type(op):
    """Get the success type"""
    schema_raw = op.get("requestBody",{}) \
        .get("content", {}).get("application/octet-stream", {}) \
        .get("schema")

    if schema_raw:
        return f"data []byte,\n", "[]byte"

    schema = op.get("requestBody",{}) \
        .get("content", {}).get("application/json", {}) \
        .get("schema")

    patch_schema = op.get("requestBody",{}) \
        .get("content", {}).get("application/merge-patch+json", {}) \
        .get("schema")

    if not schema and not patch_schema:
        return "", None

    if not schema:
        schema = patch_schema

    stype = schema.get("type")
    if stype and stype == "array":
        s = _ref_to_type(schema["items"]["$ref"])
        t = idiomize(s)
        return f"req []*{t},\n", s

    ref = schema.get("$ref")
    if not ref:
        return "", None

    s = _ref_to_type(ref)
    t = idiomize(s)

    if s in POLYMORPHIC_TYPES:
        return f"req {t},\n", s

    return f"req *{t},\n", s



def _render_query_params_struct(func, params):
    """Param struct"""
    buf = []
    buf += [f"// {func}Query has all query parameters for {func}\n"]
    buf += ["type %sQuery struct {\n" % func]

    for p in params:
        buf += render_struct_attr("", p["name"], p["schema"], True)

    buf += ["}\n\n"]

    short = func[0].lower()

    buf += [f"// RawQuery creates a query string for {func}Query\n"]
    buf += [f"func ({short}* {func}Query) RawQuery() string ", "{\n"]
    buf += ["qry := url.Values{}\n"]
    buf += ["val := \"\"\n"]

    for p in params:
       buf += _render_query_param_to_raw(short, p)

    buf += ["return qry.Encode()"]
    buf += ["}\n\n"]

    return buf


def _render_query_param_to_raw(short, param):
    """Add param to RawQuery"""
    buf = []
    name = param["name"]
    stype = param["schema"].get("type")
    mname = idiomize(camelize(name))

    if stype == "array":
        buf += [f"val = strings.Join({short}.{mname}, \",\")\n"]
        buf += ["if val != \"\" {\n"]
        buf += [f"qry.Add(\"{name}\", val)\n"]
        buf += ["}\n"]
    elif stype == "string":
        buf += [f"val = {short}.{mname}\n"]
        buf += ["if val != \"\" {\n"]
        buf += [f"qry.Add(\"{name}\", val)\n"]
        buf += ["}\n"]
    else:
        buf += [f"val = fmt.Sprintf(\"%v\", {short}.{mname})\n"]
        buf += ["if val != \"0\" {\n"]
        buf += [f"qry.Add(\"{name}\", val)\n"]
        buf += ["}\n"]

    return buf


def _query_params_from_op(op):
    """Get Query Params"""
    return [p for p in op.get("parameters", [])
        if p.get("in", "") == "query"]


def _filters_header_from_func(func, op):
    """filters header"""
    params = _query_params_from_op(op)
    if len(params) > 0:
        return f"qry ...*{func}Query,"
    return ""


