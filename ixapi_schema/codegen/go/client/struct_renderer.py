
import subprocess

from ixapi_schema.codegen.go.text import (
    idiomize,
    camelize,
    comment_newlines,
)
from ixapi_schema.codegen.go.components import (
    is_readonly_model,
)
from ixapi_schema.v2 import __version__ as schema_version

EXCLUDE_STRUCTS = [
    "Aggregate",
    "AggregateStatistics",
    "AggregateTimeseries",
]

POLYMORPHIC_REF_TYPES = [
    "VLANConfig",
]

POLYMORPHIC_REF_STRUCTS = {
    "ExchangeLanNetworkServiceConfig": "vlan_config",
    "P2PNetworkServiceConfig": "vlan_config",
    "P2MPNetworkServiceConfig": "vlan_config",
    "MP2MPNetworkServiceConfig": "vlan_config",
    "CloudNetworkServiceConfig": "vlan_config",
}

STRUCTS_PRELUDE = """package ixapi

//
// CAUTION:
//   This file is generated from the IX-API
//   openapi specs. DO NOT EDIT.
//

const dateLayout = "2006-01-02"

// Date is a date only time type representing
// a date without time.
type Date time.Time

// String implements the stringer interface for Date
func (d Date) String() string {
    t := time.Time(d)
    return t.Format(dateLayout)
}

// ParseDate decodes a date from a string
func ParseDate(value string) (Date, error) {
    t, err := time.Parse(dateLayout, value)
    if err != nil {
        return Date(t), err
    }
    return Date(t), nil
}

// MustParseDate decodes a Date using ParseDate, but
// will panic in case of an error.
func MustParseDate(value string) Date {
    d, err := ParseDate(value)
    if err != nil {
        panic(err)
    }
    return d
}

// UnmarshalJSON parses the json value of a date
func (d *Date) UnmarshalJSON(b []byte) error {
    s := strings.Trim(string(b), `"`)
    t, err := time.Parse(dateLayout, s)
    if err != nil {
        return err
    }
    *d = Date(t)
    return nil
}

// MarshalJSON returns the time in date format
func (d Date) MarshalJSON() ([]byte, error) {
    val := `"` + d.String() + `"`
    return []byte(val), nil
}

// Polymorphic indicates that the type is polymorphic.
type Polymorphic interface {
    PolymorphicType() string
}

// Response is an IX-API general response
type Response interface{}

"""

_ConstRef = {}

def render_structs_from_components(components):
    """
    Returns a dict with component name and struct
    struct representation
    """
    return {
        name: _render_struct(components, name, component)
        for name, component in components.items()
    }


def write_structs_to_file(components, dst):
    """Write components"""
    structs = render_structs_from_components(components)
    with open(dst, "w+") as f:
        f.write(STRUCTS_PRELUDE)
        # Add version info
        f.write("// SchemaVersion is the version of the ix-api schema\n")
        f.write(f"const SchemaVersion = \"{schema_version}\"\n\n")

        for _, s in structs.items():
            f.writelines(s)

    # Invoke goimports on file
    subprocess.run(["goimports", "-w", dst])


def _render_struct(components, name, component):
    """Render a component struct"""
    global _ConstRef
    if name in EXCLUDE_STRUCTS:
        return []

    buf = []

    description = comment_newlines(
        component.get("description", f"is {name}"))

    name = idiomize(name)
    short = name[0].lower()

    # Declare polymorphic base
    ptype, pfield = _polymorphic_type(component)
    if ptype:
        buf += ["// %s %s\n" % (name, description)]
        buf += ["type %s interface {\n" % name]
        buf += ["Polymorphic"]
        buf += ["}\n\n"]

        buf += ["// Polymorphic%s is a polymorphic base\n" % name]
        buf += ["type Polymorphic%s struct {\n" % name]
        buf += [f"{ptype} string `json:\"{pfield}\"`\n"]
        buf += ["}\n\n"]

        buf += ["// PolymorphicType implements the polymorphic interface\n"]
        buf += ["func (%s Polymorphic%s) PolymorphicType() string {\n" % (short, name)]
        buf += ["return %s.%s\n" % (short, ptype)]
        buf += ["}\n\n"]


    mapping = component.get("discriminator", {}).get("mapping", {})

    for val, ref in mapping.items():
        tname = _type_from_ref(ref)
        if _ConstRef.get(tname):
            continue
        buf += [f"// {tname}Type is a polymorphic type value for {tname}\n"]
        buf += [f"const {tname}Type = \"{val}\"\n\n"]
        _ConstRef[tname] = True

    # Handle property components
    if not component.get("properties"):
        return buf

    buf = []
    buf += ["// %s %s\n" % (name, description)]
    buf += ["type %s struct {\n" % name]

    readonly = is_readonly_model(name)

    for pname, prop in component["properties"].items():
        required = pname in component.get("required", [])
        if readonly:
            required = True

        buf += render_struct_attr(name, pname, prop, required)
    buf += ["}\n\n"]

    # Implement interfaces if required
    buf += _maybe_implement_polymorphic(components, name, component)
    buf += _maybe_implement_error(name, component)

    return buf


def _maybe_implement_error(name, component):
    """Implement error interface"""
    if name != "ProblemResponse": # Yeah.... ...
        return []

    tshort = name[0].lower()
    buf = []
    buf += ["// Error implements the error interface for %s\n" % name]
    buf += ["func (%s %s) Error() string {\n" % (tshort, name)]
    buf += ["return fmt.Sprintf(\"%s (%d), %s\",\n"]
    buf += [f"{tshort}.Title, {tshort}.Status, {tshort}.Detail)\n"]
    buf += ["}\n\n"]

    return buf


def _maybe_implement_polymorphic(components, name, component):
    """
    Add implementation for Polymorphic type
    """
    all_of = component.get("allOf")
    if not all_of:
        return []

    buf = []
    short = name[0].lower()
    buf += ["// PolymorphicType implements the polymorphic interface\n"]
    buf += ["func (%s %s) PolymorphicType() string {\n" % (short, name)]
    buf += ["return %sType\n" % (name)]
    buf += ["}\n\n"]

    return buf


def _polymorphic_type(component):
    """Checks if the base has a discriminator"""
    disc = component.get("discriminator")
    if not disc:
        return None, None
    pname = disc["propertyName"]
    return idiomize(camelize(pname)), pname


def render_struct_attr(struct, name, prop, required):
    """
    Create struct member
    """
    description = comment_newlines(
        prop.get("description", f"is a {name}"))

    mname = idiomize(camelize(name))
    mtype = _type_from_property(prop, required)

    if struct in POLYMORPHIC_REF_STRUCTS.keys():
        pref_attr = POLYMORPHIC_REF_STRUCTS[struct]
        if pref_attr == "vlan_config" and name == "vlan_config":
            return _render_vlan_config_struct_attr()

    buf = []

    # Properties
    buf += ["// %s %s\n" % (mname, description)]
    buf += ["%s %s `json:\"%s,omitempty\"`\n\n" % (mname, mtype, name)]

    return buf

def _render_vlan_config_struct_attr():
    buf = []
    buf += ["// VLANConfig is a polymorphic vlan configuration", "\n"]
    buf += ['VLANConfig VLANConfig `tf:"vlan_config" json:"-"`', "\n"]
    buf += ["// VLANConfigRaw contains the vlan config response data", "\n" ]
    buf += ['VLANConfigRaw json.RawMessage `tf:"-" json:"vlan_config,omitempty"`', "\n\n"]
    return buf


def _pointer_if_nullable(prop):
    """return * if nullable"""
    if prop.get("nullable"):
        return "*"
    return ""


def  _type_from_property(prop, required):
    """Return property type"""
    ref = prop.get("$ref")
    if ref:
        ref_type = _type_from_ref(ref)
        if ref_type in POLYMORPHIC_REF_TYPES:
            return ref_type
        return "*" + _type_from_ref(ref)

    stype = None
    ptype = prop["type"]
    if ptype == "array":
        return _array_ptype(prop)
    if ptype == "object":
        return _object_type(prop)

    if ptype == "string":
        stype = _string_type(prop)
    if ptype == "integer":
        stype = "int" # maybe needs refinement
    if ptype == "boolean":
        stype = "bool"
    if ptype == "number":
        stype = "float64"

    if not stype:
        raise NotImplementedError("missing type", ptype, prop)

    if required:
        return stype

    return "*" + stype


def _type_from_ref(ref):
    """ref2type"""
    t = ref.split("/")
    return idiomize(t[len(t)-1])


def _object_type(prop):
    """object type"""
    return "map[string]interface{}"


def _array_ptype(prop):
    """array property type"""
    tname = _type_from_property(prop["items"], True)
    return f"[]{tname}"


def _string_type(prop):
    """check the format of the string"""
    fmt = prop.get("format")
    if fmt and fmt == "date":
        return "Date"
    if fmt and fmt == "date-time":
        return "time.Time"

    return "string"

