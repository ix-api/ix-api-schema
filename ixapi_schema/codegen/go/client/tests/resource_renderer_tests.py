
from ixapi_schema.codegen.go.client import resource_renderer as rr


def test_filter_header_from_func():
    """Test Filter Header"""
    h = rr._filters_header_from_func("Test", {
        "parameters": [ { "in": "query" } ],
    })

    assert not h == ""


def test_render_query_params_struct():
    """Test query params struct"""
    buf = rr._render_query_params_struct("Test", [
        {
            "name": "id",
            "description": "foo",
            "in": "query",
            "schema": {
                "type": "array",
                "items": { "type": "string" },
            },
        },
    ])
    assert len(buf) > 0


