

from ixapi_schema.codegen.go.client import struct_renderer

components = {
    "Device": {
        "description": "an IXP platform device",
        "properties": {
            "name": {
                "type": "string",
                "description": "Name of the device",
                "example": "dev1.foo.bar",
                "maxLength": 180,
            },
            "capabilities": {
                "type": "array",
                "items": {
                    "$ref": "#/components/schemas/DeviceCapability",
                },
            },
            "speed": {
                "type": "integer",
            },
            "address": {
                "$ref": "#/components/schemas/Address",
            },
            "foo": {
                "type": "string",
                "nullable": True,
            },
        }
    }
}


def test_render_structs_from_components():
    """Test rendering structs from components"""
    # Render without crashing
    struct_renderer.render_structs_from_components(components)

