
def camelize(text):
    """Return camelized from snake"""
    return "".join([t.title() for t in text.split("_")])


def idiomize(name):
    """make an idiomatic go name"""
    fixes = {
        "Id": "ID",
        "Ip": "IP",
        "Url": "URL",
        "Bgp": "BGP",
        "Asn": "ASN",
        "Fqdn": "FQDN",
        "Api": "API",
        "IpV6": "IPv6",
        "IpV4": "IPv4",
        "VLan": "VLAN",
        "Vlan": "VLAN",
        "PartialUpdate": "Patch",
    }
    for k, v in fixes.items():
        name = name.replace(k, v)

    return name

def comment_newlines(text):
    return "\n// ".join(text.split("\n"))
