"""
Minimal templating
"""

from copy import copy

def render(tmpl, **opts):
    """render a text template"""
    res = copy(tmpl)
    for k, v in opts.items():
        var = "{{" + k.upper() + "}}"
        res = res.replace(var, v)
    return res

