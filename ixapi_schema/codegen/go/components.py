
"""
Component inspection functions
"""

READONLY_MODELS = [
    "Status",
    "Device",
    "DeviceCapability",
    "Event",
    "APIHealth",
    "APIImplementation",
    "ProblemResponse",
]


def is_readonly_model(name):
    """
    Read only models assume all primitive types
    to be required but ommitted when empty.
    This makes interacting with a Status or ProblemResponse
    easier
    """
    return name in READONLY_MODELS


READONLY_PROP_NAMES = [
    "id",
    "status",
    "state",
    "Device.device_capabilities",
    "Port.connecting_party",
]

def is_readonly_attr(component, prop_name):
    """Some props like status are readonly"""
    component_name = component.get("name")
    for prop in READONLY_PROP_NAMES:
        match prop.split():
            case [pname]:
                if pname == prop_name:
                    return True
            case [cname, pname]:
                if component_name == cname and pname == prop_name:
                    return True

    return False


WRITEONLY_PROP_NAMES = [
    "connecting_party",
]

def is_writeonly_attr(prop_name):
    """Some pops are only present in the request"""
    return prop_name in WRITEONLY_PROP_NAMES


OVERRIDE_PROP_NAMES = [
    "vlan_config",
]

def is_override_attr(prop_name):
    """Some things need to be inserted manually"""
    return prop_name in OVERRIDE_PROP_NAMES

