"""
IX-API Documentation Server
---------------------------

Minimal flask application to serve an embedded redoc
and the generated spec. The RFC7808 error documents
will also be served through this application.
"""

import os
import json
from glob import glob

import yaml
import flask
from werkzeug import serving, routing, exceptions

from ixapi_schema import v1, v2
from ixapi_schema.openapi import schema
from ixapi_schema.problem_details import (
    generator as pd_generator,
    reflection as pd_reflection,
)

app = flask.Flask(__name__)

# Default configs
LISTEN_DEFAULT = os.getenv(
    "IX_API_SCHEMA_LISTEN_DEFAULT", "127.0.0.1:9080")
LISTEN_HOST_DEFAULT, LISTEN_PORT_DEFAULT = LISTEN_DEFAULT.split(":")
LISTEN_PORT_DEFAULT = int(LISTEN_PORT_DEFAULT)

SCHEMA_ENV = os.getenv(
    "IX_API_SCHEMA_ENV", "develop")


class IxApiBaseConverter(routing.BaseConverter):
    """Resolve the ix api version parameter"""
    API_VERSIONS = {
        "v1": v1,
        "v2": v2,
    }

    def to_python(self, value):
        api_base = self.API_VERSIONS.get(value.lower())
        if not api_base:
            raise exceptions.NotFound(
                "The api version {} could not be found.".format(
                    value))
        return api_base


class IxApiExtensionConverter(IxApiBaseConverter):
    API_VERSIONS = {
        # "v1-connection-creation-v1": v1_connection_creation_v1,
    }


app.url_map.converters["ixapi_base"] = IxApiBaseConverter
app.url_map.converters["ixapi_extension"] = IxApiExtensionConverter


@app.route('/static/<path:path>')
def send_static(path):
    return flask.send_from_directory(
        "ixapi_schema/server/static", path)


@app.route("/")
def docs_index():
    """Render the documentation entry page"""
    index = "ixapi_schema/server/static/index.html"
    if SCHEMA_ENV == "production":
        index = "ixapi_schema/server/static/index_production.html"
    with open(index) as f:
        body = f.read()
        body = body \
            .replace("__V1_VERSION__", v1.__version__) \
            .replace("__V2_VERSION__", v2.__version__)

        return body


@app.route("/<basepath>/redoc")
def redoc_index(basepath):
    with open("ixapi_schema/server/static/redoc/redoc.html") as f:
        content = f.read()
        content = content.replace("{{basepath}}", basepath)
        return content

@app.route("/extensions/<basepath>/redoc")
def redoc_index_extensions(basepath):
    return redoc_index("extensions/" + basepath)


@app.route("/<ixapi_base:version>/ix-api-latest.json")
@app.route("/extensions/<ixapi_extension:version>/ix-api-latest.json")
def spec_render_latest_json(version):
    """Render current version in json format"""
    spec = schema.generate(version)
    return json.dumps(spec), {
        "Content-Type": "application/json",
    }


@app.route("/<ixapi_base:version>/ix-api-latest.y<aml>")
def spec_render_latest_yaml(version, aml):
    """Render latest yaml version"""
    spec = schema.generate(version)
    return yaml.dump(spec), {
        "Content-Type": "application/yaml",
    }


@app.route("/<version>/problems")
def pd_redirect_index(version):
    """Redirect to index page"""
    return flask.redirect("/{}/problems/".format(version))


@app.route("/<ixapi_base:version>/problems/")
def pd_render_index(version):
    """Render problem details index page"""
    print(version)
    print(type(version))
    if version == v1:
        from ixapi_schema.v1.entities import problems
    else:
        from ixapi_schema.v2.entities import problems

    return pd_generator.render_index(problems)


@app.route("/<ixapi_base:version>/problems/<path:problem_ptype>")
def pd_render_details(version, problem_ptype):
    """Render problem details"""
    # strip ext
    problem_ptype = problem_ptype.replace(".html", "")
    if version == v1:
        from ixapi_schema.v1.entities import problems
    else:
        from ixapi_schema.v2.entities import problems

    try:
        problem = pd_reflection.find_problem_by_ptype(problems, problem_ptype)
    except pd_reflection.ProblemNotFound:
        return "404 - Not Found", 404

    return pd_generator.render_details(problem)


def serve(listen):
    """
    Start the HTTP server.

    :param listen: The listen address to bind to <host>:<port>
    :param api_base: A python module containing the api specs.
    """
    host, port = listen.split(":")
    if not host:
        host = LISTEN_HOST_DEFAULT
    try:
        port = int(port)
    except:
        port = None
    if not port:
        port = LISTEN_PORT_DEFAULT

    # Find all api sepc files
    api_spec_files = glob("ixapi_schema/v1/*.py") + \
                     glob("ixapi_schema/v1/*/*.py")

    # Create app instance and start werkzeug
    serving.run_simple(
        host, port, app,
        use_reloader=True,
        extra_files=api_spec_files)
