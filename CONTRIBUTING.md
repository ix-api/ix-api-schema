# How to contribute

1. Fork the IX-API schema from https://gitlab.com/ix-api/ix-api-schema into your own gitlab project
1. Please follow the instructions in the README file to make changes and work with the schema
1. Create a merge request from your own project back to https://gitlab.com/ix-api/ix-api-schema
1. We are going to discuss the change on the merge request page and eventually accept or reject

For non-trivial changes please raise an Gitlab issue first, so an approach can be agreed before investing time and work.
