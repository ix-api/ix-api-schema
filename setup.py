#!/usr/bin/env python3

"""
IX-API: Schema
==============

This package contains the ix-api schema.
"""

from setuptools import setup, find_packages

from ixapi_schema import (
    __title__,
    __license__,
    __author__,
    __version__,
)

# Load requirements
with open("requirements.txt") as f:
    requirements = f.read()

# Load description
with open("README.md") as f:
    long_description = f.read()


setup(name=__title__,
      version=__version__,
      packages=find_packages(),
      license=__license__,
      long_description=long_description)

